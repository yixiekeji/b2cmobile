"use strict";
const store_address = require("../../store/address.js");
const common_vendor = require("../../common/vendor.js");
let CHINA_REGIONS = store_address.address().addresses;
const _sfc_main = {
  props: {
    defaultRegions: {
      type: Array,
      default() {
        return [];
      }
    },
    defaultRegionCode: {
      type: String
    },
    defaultRegion: [String, Array]
  },
  data() {
    return {
      cityArr: CHINA_REGIONS[0].children,
      districtArr: CHINA_REGIONS[0].children[0].children,
      multiIndex: [0, 0, 0],
      isInitMultiArray: true
    };
  },
  watch: {
    defaultRegion: {
      handler(region, oldRegion) {
        if (Array.isArray(region)) {
          oldRegion = oldRegion || [];
          if (region.join("") !== oldRegion.join("")) {
            this.handleDefaultRegion(region);
          }
        } else if (region && region.length == 6) {
          this.handleDefaultRegion(region);
        } else {
          console.warn("defaultRegion非有效格式");
        }
      },
      immediate: true
    }
  },
  computed: {
    multiArray() {
      return this.pickedArr.map((arr) => arr.map((item) => item.regionName));
    },
    pickedArr() {
      console.log(CHINA_REGIONS, this.cityArr, this.districtArr);
      if (this.isInitMultiArray) {
        return [
          CHINA_REGIONS,
          CHINA_REGIONS[0].children,
          CHINA_REGIONS[0].children[0].children
        ];
      }
      return [CHINA_REGIONS, this.cityArr, this.districtArr];
    }
  },
  methods: {
    handleColumnChange(e) {
      this.isInitMultiArray = false;
      const that = this;
      let col = e.detail.column;
      let row = e.detail.value;
      that.multiIndex[col] = row;
      try {
        switch (col) {
          case 0:
            if (CHINA_REGIONS[that.multiIndex[0]].children.length == 0) {
              that.cityArr = that.districtArr = [CHINA_REGIONS[that.multiIndex[0]]];
              break;
            }
            that.cityArr = CHINA_REGIONS[that.multiIndex[0]].children;
            that.districtArr = CHINA_REGIONS[that.multiIndex[0]].children[that.multiIndex[1]].children;
            break;
          case 1:
            that.districtArr = CHINA_REGIONS[that.multiIndex[0]].children[that.multiIndex[1]].children;
            break;
          case 2:
            break;
        }
      } catch (e2) {
        that.districtArr = CHINA_REGIONS[that.multiIndex[0]].children[0].children;
      }
    },
    handleValueChange(e) {
      let [index0, index1, index2] = e.detail.value;
      let [arr0, arr1, arr2] = this.pickedArr;
      let address = [arr0[index0], arr1[index1], arr2[index2]];
      this.$emit("getRegion", address);
    },
    handleDefaultRegion(region) {
      const isCode = !Array.isArray(region);
      this.isInitMultiArray = false;
      let childrend = CHINA_REGIONS;
      for (let i = 0; i < 3; i++) {
        for (let j = 0; j < childrend.length; j++) {
          let condition = isCode ? childrend[j].id == region.slice(0, (i + 1) * 2) : childrend[j].regionName.includes(region[i]);
          if (condition) {
            childrend = childrend[j].children;
            if (i == 0) {
              this.cityArr = childrend;
            } else if (i == 1) {
              this.districtArr = childrend;
            }
            this.$set(this.multiIndex, i, j);
            break;
          } else {
            if (i == 0 && j == childrend.length - 1) {
              this.isInitMultiArray = true;
            }
          }
        }
      }
    }
  }
};
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: $data.multiIndex,
    b: $options.multiArray,
    c: common_vendor.o((...args) => $options.handleValueChange && $options.handleValueChange(...args)),
    d: common_vendor.o((...args) => $options.handleColumnChange && $options.handleColumnChange(...args))
  };
}
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render]]);
wx.createComponent(Component);
