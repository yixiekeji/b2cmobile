"use strict";
const common_vendor = require("../../common/vendor.js");
const components_navbar_comon = require("./comon.js");
const store_navHeight = require("../../store/navHeight.js");
getApp();
const _sfc_main = {
  name: "HeadView",
  props: {
    // 文本区域位置 left：左  center：中  
    textAlign: {
      type: String,
      default: "center"
    },
    // 文本区内容
    textContent: {
      type: String,
      default: ""
    },
    // 文本区离左边的距离
    textPaddingLeft: {
      type: Number,
      default: 16
    },
    // 是否需要返回按钮
    isBackShow: {
      type: Boolean,
      default: true
    },
    // 文本区字体大小
    fontSize: {
      type: Number,
      default: 20
      //px
    },
    // 文本区字体粗细
    fontWeight: {
      type: Number,
      default: 700
    },
    // 文本区返回按钮图片宽
    backImageWidth: {
      type: Number,
      default: 12
      //px
    },
    // 文本区返回按钮图片高
    backImageHeight: {
      type: Number,
      default: 24
      //px
    },
    // 返回按钮图标路径
    backImageUrl: {
      type: String,
      default: "1"
    },
    // 导航栏整体背景颜色
    navBackgroundColor: {
      type: String,
      default: "#2476F9"
    },
    // 标题字体颜色
    titleColor: {
      type: String,
      default: "#000"
    },
    /******** h5端，app端需要传入自定义导航栏高度 *******/
    navHeightValue: {
      type: Number,
      default: 44
      //px
    },
    searchValues: {
      type: String,
      default: ""
    }
  },
  computed: {
    // 文本区宽度
    navTextWidth() {
      if (this.textAlign === "center") {
        return this.windowWidth - (this.windowWidth - this.menubarLeft) * 2 + "rpx";
      } else {
        return this.menubarLeft + "rpx";
      }
    },
    // 文本区paddingLeft
    textPaddingleft() {
      if (this.textAlign === "center") {
        return "0";
      } else {
        return this.textPaddingLeft + "rpx";
      }
    }
  },
  data() {
    return {
      statusBarHeight: getApp().globalData.statusBarHeight,
      //状态栏高度
      navHeight: getApp().globalData.navHeight,
      //头部导航栏总体高度
      navigationBarHeight: getApp().globalData.navigationBarHeight,
      //导航栏高度
      customHeight: getApp().globalData.customHeight,
      //胶囊高度
      scaleFactor: getApp().globalData.scaleFactor,
      //比例系数
      menubarLeft: getApp().globalData.menubarLeft,
      //胶囊定位的左边left
      windowWidth: getApp().globalData.windowWidth * getApp().globalData.scaleFactor,
      searchValue: ""
    };
  },
  methods: {
    backEvent(val) {
      console.log(this.textContent);
      if (val == 2) {
        common_vendor.index.switchTab({
          url: "/pages/homePage/homepage"
        });
      } else {
        if (this.textContent == "在线支付") {
          common_vendor.index.navigateTo({
            url: "/pages/memberCenter/myOrder/myOrder?stateid=1"
          });
        } else if (this.textContent == "定金支付") {
          common_vendor.index.navigateTo({
            url: "/pages/memberCenter/myOrder/myOrder?stateid=1"
          });
        } else if (this.textContent == "我的订单") {
          common_vendor.index.switchTab({
            url: "/pages/memberCenter/memberCenter"
          });
        } else if (this.textContent == "支付成功") {
          common_vendor.index.navigateTo({
            url: "/pages/memberCenter/myOrder/myOrder?stateid=0"
          });
        } else {
          common_vendor.index.navigateBack({
            delta: 1
          });
        }
      }
    },
    doSearch() {
      console.log(this.searchValue);
      this.$emit("searched", this.searchValue);
    },
    focused() {
      console.log(111);
      common_vendor.index.navigateTo({
        url: "/pages/homePage/searchPage"
      });
    }
  },
  mounted() {
    if (this.searchValues != "") {
      console.log(this.searchValues);
      this.searchValue = this.searchValues;
    }
  },
  created() {
    const SystemInfomations = components_navbar_comon.systemInfo();
    this.statusBarHeight = SystemInfomations.statusBarHeight;
    this.scaleFactor = SystemInfomations.scaleFactor;
    this.windowWidth = SystemInfomations.windowWidth;
    this.navHeight = SystemInfomations.navHeight + SystemInfomations.statusBarHeight;
    this.navigationBarHeight = SystemInfomations.navHeight;
    this.customHeight = SystemInfomations.menuButtonHeight;
    this.menubarLeft = SystemInfomations.menuButtonLeft;
    store_navHeight.navHeight().setHeight(this.navHeight);
  }
};
if (!Array) {
  const _easycom_uni_icons2 = common_vendor.resolveComponent("uni-icons");
  _easycom_uni_icons2();
}
const _easycom_uni_icons = () => "../../uni_modules/uni-icons/components/uni-icons/uni-icons.js";
if (!Math) {
  _easycom_uni_icons();
}
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return common_vendor.e({
    a: $props.backImageUrl == "1" || $props.backImageUrl == "2" || $props.backImageUrl == "0"
  }, $props.backImageUrl == "1" || $props.backImageUrl == "2" || $props.backImageUrl == "0" ? common_vendor.e({
    b: common_vendor.t($props.textContent),
    c: $options.navTextWidth,
    d: $data.customHeight + "rpx",
    e: $props.textPaddingLeft * $data.scaleFactor + "rpx",
    f: $props.fontSize * $data.scaleFactor + "rpx",
    g: $props.fontWeight,
    h: $props.titleColor,
    i: $props.backImageUrl == "1"
  }, $props.backImageUrl == "1" ? {
    j: common_vendor.o(($event) => $options.backEvent(1)),
    k: common_vendor.p({
      type: "left",
      size: "17"
    })
  } : {}, {
    l: $props.backImageUrl == "2"
  }, $props.backImageUrl == "2" ? {
    m: common_vendor.o(($event) => $options.backEvent(2)),
    n: common_vendor.p({
      type: "closeempty",
      size: "17"
    })
  } : {}, {
    o: $props.backImageWidth * $data.scaleFactor + "rpx",
    p: $props.backImageHeight * $data.scaleFactor + "rpx",
    q: $props.isBackShow ? "flex" : "none",
    r: $data.customHeight + "rpx",
    s: $props.textAlign === "center" ? "center" : "left"
  }) : {}, {
    t: $props.backImageUrl == "3" || $props.backImageUrl == "4"
  }, $props.backImageUrl == "3" || $props.backImageUrl == "4" ? common_vendor.e({
    v: common_vendor.o((...args) => $options.doSearch && $options.doSearch(...args)),
    w: $data.searchValue,
    x: common_vendor.o(($event) => $data.searchValue = $event.detail.value),
    y: common_vendor.p({
      type: "search",
      size: "18"
    }),
    z: common_vendor.s({
      "margin-left": $props.textAlign === "center" ? "" : "20rpx"
    }),
    A: common_vendor.s({
      width: $props.textAlign === "center" ? " calc(100% - 320rpx)" : " calc(100% - 280rpx)"
    }),
    B: $props.backImageUrl == "3"
  }, $props.backImageUrl == "3" ? {
    C: common_vendor.p({
      type: "left",
      size: "17"
    })
  } : {}, {
    D: $props.backImageWidth * $data.scaleFactor + "rpx",
    E: $props.backImageHeight * $data.scaleFactor + "rpx",
    F: common_vendor.s({
      display: $props.isBackShow ? "flex" : "none"
    }),
    G: common_vendor.s({
      width: $props.backImageUrl == "4" ? "0" : "60rpx"
    }),
    H: common_vendor.o((...args) => $options.backEvent && $options.backEvent(...args)),
    I: $data.customHeight + "rpx",
    J: $props.textAlign === "center" ? "center" : "left"
  }) : {}, {
    K: $props.backImageUrl == "7" || $props.backImageUrl == "8"
  }, $props.backImageUrl == "7" || $props.backImageUrl == "8" ? common_vendor.e({
    L: common_vendor.p({
      type: "search",
      size: "18"
    }),
    M: common_vendor.o((...args) => $options.focused && $options.focused(...args)),
    N: common_vendor.s({
      "margin-left": $props.textAlign === "center" ? "" : "20rpx"
    }),
    O: common_vendor.s({
      width: $props.textAlign === "center" ? " calc(100% - 320rpx)" : " calc(100% - 280rpx)"
    }),
    P: $props.backImageUrl == "7"
  }, $props.backImageUrl == "7" ? {
    Q: common_vendor.p({
      type: "left",
      size: "17"
    })
  } : {}, {
    R: $props.backImageWidth * $data.scaleFactor + "rpx",
    S: $props.backImageHeight * $data.scaleFactor + "rpx",
    T: common_vendor.s({
      display: $props.isBackShow ? "flex" : "none"
    }),
    U: common_vendor.s({
      width: $props.backImageUrl == "4" ? "0" : "60rpx"
    }),
    V: common_vendor.o((...args) => $options.backEvent && $options.backEvent(...args)),
    W: $data.customHeight + "rpx",
    X: $props.textAlign === "center" ? "center" : "left"
  }) : {}, {
    Y: $data.navigationBarHeight + "rpx",
    Z: $data.statusBarHeight + "rpx",
    aa: $data.navHeight + "rpx",
    ab: $props.navBackgroundColor,
    ac: $data.navHeight + "rpx"
  });
}
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render]]);
wx.createComponent(Component);
