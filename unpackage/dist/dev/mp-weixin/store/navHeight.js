"use strict";
const common_vendor = require("../common/vendor.js");
const navHeight = common_vendor.defineStore("navHeight", {
  state: () => {
  },
  actions: {
    setHeight(val) {
      this.navHeight = val;
    }
  }
});
exports.navHeight = navHeight;
