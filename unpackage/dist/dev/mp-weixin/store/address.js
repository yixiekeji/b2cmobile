"use strict";
const common_vendor = require("../common/vendor.js");
const api_member = require("../api/member.js");
const address = common_vendor.defineStore("address", {
  state: () => {
  },
  actions: {
    setAddress() {
      api_member.getAllPCA().then((res) => {
        if (res) {
          console.log(res);
          this.addresses = res.data.data;
        }
      });
    }
  }
});
exports.address = address;
