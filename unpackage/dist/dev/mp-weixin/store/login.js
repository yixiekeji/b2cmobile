"use strict";
const common_vendor = require("../common/vendor.js");
const api_login = require("../api/login.js");
const login = common_vendor.defineStore("login", {
  state: () => ({
    token: common_vendor.index.getStorageSync("token")
  }),
  actions: {
    doLogin(val) {
      return new Promise((resolve, reject) => {
        api_login.dologin(val).then((res) => {
          if (res) {
            this.token = res.data.data.token;
            common_vendor.index.setStorageSync("token", res.data.data.token);
            resolve();
          } else {
            reject();
          }
        }).catch((error) => {
          reject();
        });
      });
    },
    doLoginPhone(val) {
      return new Promise((resolve, reject) => {
        api_login.dologinPhone(val).then((res) => {
          if (res) {
            this.token = res.data.data.token;
            common_vendor.index.setStorageSync("token", res.data.data.token);
            resolve();
          } else {
            reject();
          }
        }).catch((error) => {
          reject();
        });
      });
    },
    loginOut() {
      return new Promise((resolve, reject) => {
        api_login.logout().then((res) => {
          if (res) {
            this.token = "";
            common_vendor.index.removeStorageSync("token");
            resolve();
          } else {
            reject();
          }
        }).catch((error) => {
          reject();
        });
      });
    },
    setToken(val) {
      this.token = val;
    }
  }
});
exports.login = login;
