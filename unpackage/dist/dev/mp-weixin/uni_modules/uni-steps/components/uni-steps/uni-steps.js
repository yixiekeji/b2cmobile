"use strict";
const common_vendor = require("../../../../common/vendor.js");
const common_assets = require("../../../../common/assets.js");
const _sfc_main = {
  name: "UniSteps",
  props: {
    direction: {
      // 排列方向 row column
      type: String,
      default: "row"
    },
    activeColor: {
      // 激活状态颜色
      type: String,
      default: "#634310"
    },
    deactiveColor: {
      // 未激活状态颜色
      type: String,
      default: "#634310"
    },
    active: {
      // 当前步骤
      type: Number,
      default: 0
    },
    activeIcon: {
      // 当前步骤
      type: String,
      default: "checkbox-filled"
    },
    options: {
      type: Array,
      default() {
        return [];
      }
    }
    // 数据
  },
  data() {
    return {};
  }
};
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: common_vendor.f($props.options, (item, index, i0) => {
      return common_vendor.e({
        a: index !== $props.active
      }, index !== $props.active ? {
        b: common_vendor.t(item.title),
        c: common_vendor.s({
          color: index === $props.active ? $props.activeColor : $props.deactiveColor
        }),
        d: common_vendor.s({
          "font-weight": index === $props.active ? "700" : "400"
        }),
        e: common_vendor.n($props.direction === "column" ? "uni-steps__column-title" : "uni-steps__row-title")
      } : {}, {
        f: index === $props.active && $props.active == 0
      }, index === $props.active && $props.active == 0 ? {
        g: common_vendor.s({
          color: index === $props.active ? $props.activeColor : $props.deactiveColor
        }),
        h: common_vendor.s({
          "font-weight": index === $props.active ? "700" : "400"
        }),
        i: common_vendor.s({
          width: "122rpx"
        }),
        j: common_vendor.s({
          height: "58rpx"
        }),
        k: common_vendor.s({
          "margin-left": "16rpx"
        }),
        l: common_vendor.s({
          transform: "translateY(20px)"
        }),
        m: common_vendor.n($props.direction === "column" ? "uni-steps__column-title" : "uni-steps__row-title"),
        n: common_assets._imports_0$13
      } : {}, {
        o: index === $props.active && $props.active == 1
      }, index === $props.active && $props.active == 1 ? {
        p: common_vendor.s({
          color: index === $props.active ? $props.activeColor : $props.deactiveColor
        }),
        q: common_vendor.s({
          "font-weight": index === $props.active ? "700" : "400"
        }),
        r: common_vendor.s({
          width: "122rpx"
        }),
        s: common_vendor.s({
          height: "58rpx"
        }),
        t: common_vendor.s({
          "margin-left": "16rpx"
        }),
        v: common_vendor.s({
          transform: "translateY(20px)"
        }),
        w: common_vendor.n($props.direction === "column" ? "uni-steps__column-title" : "uni-steps__row-title"),
        x: common_assets._imports_1$6
      } : {}, {
        y: index === $props.active && $props.active == 2
      }, index === $props.active && $props.active == 2 ? {
        z: common_vendor.s({
          color: index === $props.active ? $props.activeColor : $props.deactiveColor
        }),
        A: common_vendor.s({
          "font-weight": index === $props.active ? "700" : "400"
        }),
        B: common_vendor.s({
          width: "122rpx"
        }),
        C: common_vendor.s({
          height: "58rpx"
        }),
        D: common_vendor.s({
          "margin-left": "16rpx"
        }),
        E: common_vendor.s({
          transform: "translateY(20px)"
        }),
        F: common_vendor.n($props.direction === "column" ? "uni-steps__column-title" : "uni-steps__row-title"),
        G: common_assets._imports_2$4
      } : {}, {
        H: index === $props.active && $props.active == 3
      }, index === $props.active && $props.active == 3 ? {
        I: common_vendor.s({
          color: index === $props.active ? $props.activeColor : $props.deactiveColor
        }),
        J: common_vendor.s({
          "font-weight": index === $props.active ? "700" : "400"
        }),
        K: common_vendor.s({
          width: "122rpx"
        }),
        L: common_vendor.s({
          height: "58rpx"
        }),
        M: common_vendor.s({
          "margin-left": "16rpx"
        }),
        N: common_vendor.s({
          transform: "translateY(20px)"
        }),
        O: common_vendor.n($props.direction === "column" ? "uni-steps__column-title" : "uni-steps__row-title"),
        P: common_assets._imports_3$2
      } : {}, {
        Q: index === $props.active && $props.active == 4
      }, index === $props.active && $props.active == 4 ? {
        R: common_vendor.s({
          color: index === $props.active ? $props.activeColor : $props.deactiveColor
        }),
        S: common_vendor.s({
          "font-weight": index === $props.active ? "700" : "400"
        }),
        T: common_vendor.s({
          width: "122rpx"
        }),
        U: common_vendor.s({
          height: "58rpx"
        }),
        V: common_vendor.s({
          "margin-left": "16rpx"
        }),
        W: common_vendor.s({
          transform: "translateY(20px)"
        }),
        X: common_vendor.n($props.direction === "column" ? "uni-steps__column-title" : "uni-steps__row-title"),
        Y: common_assets._imports_4$2
      } : {}, {
        Z: common_vendor.t(item.desc),
        aa: index
      });
    }),
    b: $props.deactiveColor,
    c: common_vendor.n($props.direction === "column" ? "uni-steps__column-desc" : "uni-steps__row-desc"),
    d: common_vendor.n($props.direction === "column" ? "uni-steps__column-text" : "uni-steps__row-text"),
    e: common_vendor.n($props.direction === "column" ? "uni-steps__column-text-container" : "uni-steps__row-text-container"),
    f: common_vendor.f($props.options, (item, index, i0) => {
      return {
        a: index <= $props.active && index !== 0 ? "#E5A749" : index === 0 ? "transparent" : "#E5A749",
        b: index == $props.active ? "#fff" : "#E5A749",
        c: index < $props.active && index !== $props.options.length - 1 ? "#E5A749" : index === $props.options.length - 1 ? "transparent" : "#E5A749",
        d: index
      };
    }),
    g: common_vendor.n($props.direction === "column" ? "uni-steps__column-line" : "uni-steps__row-line"),
    h: common_vendor.n($props.direction === "column" ? "uni-steps__column-line--before" : "uni-steps__row-line--before"),
    i: common_vendor.n($props.direction === "column" ? "uni-steps__column-circle" : "uni-steps__row-circle"),
    j: common_vendor.n($props.direction === "column" ? "uni-steps__column-line" : "uni-steps__row-line"),
    k: common_vendor.n($props.direction === "column" ? "uni-steps__column-line--after" : "uni-steps__row-line--after"),
    l: common_vendor.n($props.direction === "column" ? "uni-steps__column-line-item" : "uni-steps__row-line-item"),
    m: common_vendor.n($props.direction === "column" ? "uni-steps__column-container" : "uni-steps__row-container"),
    n: common_vendor.n($props.direction === "column" ? "uni-steps__column" : "uni-steps__row")
  };
}
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render]]);
wx.createComponent(Component);
