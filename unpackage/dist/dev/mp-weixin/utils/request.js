"use strict";
const common_vendor = require("../common/vendor.js");
const store_login = require("../store/login.js");
function service(options = {}) {
  let optionsApi = "";
  optionsApi = options.url;
  options.url = `https://b2c.yixiekeji.cn/prod-api/api${options.url}`;
  console.log(store_login.login());
  options.header = {
    "content-type": "application/x-www-form-urlencoded;charset=UTF-8",
    token: store_login.login().token
    //Bearer
    // 'tokenId': uni.getStorageSync("s"),
    // 'tokenId':'1',
    //获取存储的cookie,仅在mp小程序环境运行
    // 'cookie': uni.getStorageSync("shiro")
  };
  options.withCredentials = true;
  return new Promise((resolved, rejected) => {
    options.success = (res) => {
      console.log(res.data.success);
      if (Number(res.statusCode) == 200) {
        if (res.data.success != false) {
          resolved(res);
          if (cookie == "" && res.cookies[0].indexOf("JSESSIONID") != -1) {
            common_vendor.index.setStorageSync("cookieKey", res.cookies[0]);
            cookie = res.cookies[0];
          }
        } else {
          resolved(res);
          if (res.data.message == "亲爱的用户，请先登录后再操作") {
            if (optionsApi == "/member/info.html" || optionsApi == "/cart/list.html" || optionsApi == "/member/index.html")
              ;
            else {
              common_vendor.index.redirectTo({
                url: "/pages/loginInterface/passwordLogin"
              });
            }
          } else if (optionsApi == "/orders/submit.html") {
            common_vendor.index.showModal({
              title: "提示",
              content: res.data.message,
              cancelText: "取消",
              // 取消按钮的文字
              confirmText: "确认",
              // 确认按钮的文字
              showCancel: false,
              // 是否显示取消按钮，默认为 true
              confirmColor: "#000",
              success: (res2) => {
                if (res2.confirm) {
                  console.log("comfirm");
                } else {
                  console.log("cancel");
                }
              }
            });
          } else {
            common_vendor.index.showToast({
              icon: "none",
              duration: 3e3,
              title: `${res.data.message}`
            });
          }
        }
      } else {
        if (res.data.msg) {
          common_vendor.index.showToast({
            icon: "none",
            duration: 3e3,
            title: `${res.data.msg}`
          });
        }
        rejected(res.data.msg);
      }
    };
    options.fail = (err) => {
      rejected(err);
    };
    common_vendor.index.request(options);
  });
}
exports.service = service;
