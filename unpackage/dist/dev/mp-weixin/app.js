"use strict";
Object.defineProperty(exports, Symbol.toStringTag, { value: "Module" });
const common_vendor = require("./common/vendor.js");
if (!Math) {
  "./pages/homePage/homepage.js";
  "./pages/homePage/noDataPage.js";
  "./pages/homePage/searchResult.js";
  "./pages/memberCenter/myOrder/myOrder.js";
  "./pages/shoppingCart/ShoppingCart.js";
  "./pages/memberCenter/memberCenter.js";
  "./pages/memberCenter/collectPage.js";
  "./pages/memberCenter/personalInformation/personalInformation.js";
  "./pages/memberCenter/personalInformation/alterNikename.js";
  "./pages/memberCenter/resetPassword.js";
  "./pages/memberCenter/alertPhoneNumber.js";
  "./pages/homePage/goodsDetail.js";
  "./pages/classification/classification.js";
  "./pages/shoppingCart/paySuccess.js";
  "./pages/shoppingCart/commitSuccess.js";
  "./pages/memberCenter/address/address.js";
  "./pages/memberCenter/address/addAddress.js";
  "./pages/shoppingCart/confirmOrder.js";
  "./pages/shoppingCart/paymentMethod.js";
  "./pages/loginInterface/passwordLogin.js";
  "./pages/loginInterface/resetPassword.js";
  "./pages/loginInterface/registerPage.js";
  "./pages/loginInterface/loginInterface.js";
  "./pages/homePage/searchPage.js";
  "./pages/memberCenter/invoice/alertInvoice.js";
  "./pages/memberCenter/findMe.js";
  "./pages/loginInterface/registerProtocol/registerProtocol.js";
  "./pages/loginInterface/privacyAgreement/privacyAgreement.js";
  "./pages/homePage/reviewsPage.js";
  "./pages/memberCenter/integralPage/integralPage.js";
  "./pages/memberCenter/integralPage/integralDetails.js";
  "./pages/memberCenter/integralPage/integralEXP.js";
  "./pages/memberCenter/integralPage/confirmOrder.js";
  "./pages/memberCenter/commentPage/commentPage.js";
  "./pages/memberCenter/commentPage/addComment.js";
  "./pages/homePage/newsPage.js";
  "./pages/homePage/newsDetails.js";
  "./pages/homePage/couponList.js";
  "./pages/memberCenter/myCoupon.js";
  "./pages/memberCenter/couponsPage/couponsPage.js";
  "./pages/memberCenter/returnsExchanges/returnsPage.js";
  "./pages/memberCenter/returnsExchanges/exchangesPage.js";
  "./pages/memberCenter/myOrder/returnsExchanges.js";
  "./pages/memberCenter/myOrder/commentList.js";
  "./pages/memberCenter/returnsExchanges/logisticsInformation.js";
  "./pages/memberCenter/returnsExchanges/deliveryPage.js";
  "./pages/memberCenter/myOrder/orderDetail.js";
}
const _sfc_main = {
  onLaunch: function() {
    console.log("App Launch");
  },
  onShow: function() {
    console.log("App Show");
  },
  onHide: function() {
    console.log("App Hide");
  }
};
const HeadNav = () => "./components/navbar/index.js";
function createApp() {
  const app = common_vendor.createSSRApp(_sfc_main);
  const pinia = common_vendor.createPinia();
  app.use(pinia);
  app.config.globalProperties.imageUrl = "http://img.yixiekeji.com:8080/b2cimage";
  app.component("HeadNav", HeadNav);
  return {
    app,
    Pinia: common_vendor.Pinia
  };
}
createApp().app.mount("#app");
exports.createApp = createApp;
