"use strict";
const utils_request = require("../utils/request.js");
function shopInvoiceCreateOrUpdate(data) {
  return utils_request.service({
    url: "/orders/invoice/createOrUpdate.html",
    method: "POST",
    data
  });
}
function shopInvoiceList(data) {
  return utils_request.service({
    url: "/orders/invoice/list.html",
    method: "GET",
    data
  });
}
exports.shopInvoiceCreateOrUpdate = shopInvoiceCreateOrUpdate;
exports.shopInvoiceList = shopInvoiceList;
