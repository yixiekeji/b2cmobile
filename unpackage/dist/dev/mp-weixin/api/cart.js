"use strict";
const utils_request = require("../utils/request.js");
function cartList(data) {
  return utils_request.service({
    url: "/cart/list.html",
    method: "GET",
    data
  });
}
function cartcheckedAll(data) {
  return utils_request.service({
    url: "/cart/cartcheckedAll.html",
    method: "GET",
    data
  });
}
function cartchecked(data) {
  return utils_request.service({
    url: "/cart/cartchecked.html",
    method: "GET",
    data
  });
}
function addOrdersCart(data) {
  return utils_request.service({
    url: "/cart/addOrdersCart.html",
    method: "GET",
    data
  });
}
function reduceOrdersCart(data) {
  return utils_request.service({
    url: "/cart/reduceOrdersCart.html",
    method: "GET",
    data
  });
}
exports.addOrdersCart = addOrdersCart;
exports.cartList = cartList;
exports.cartchecked = cartchecked;
exports.cartcheckedAll = cartcheckedAll;
exports.reduceOrdersCart = reduceOrdersCart;
