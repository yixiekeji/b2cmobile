"use strict";
const utils_request = require("../utils/request.js");
function sendVerifySMS(data) {
  return utils_request.service({
    url: "/sendVerifySMS.html",
    method: "GET",
    data
  });
}
function verify(data) {
  return utils_request.service({
    url: "/verifyBase64.html",
    method: "GET"
  });
}
function doforgetpassword(data) {
  return utils_request.service({
    url: "/member/resetMemberPwd.html",
    method: "POST",
    data
  });
}
function resetPwd(data) {
  return utils_request.service({
    url: "/resetPwd.html",
    method: "POST",
    data
  });
}
function updatephone(data) {
  return utils_request.service({
    url: "/member/updatephone.html",
    method: "POST",
    data
  });
}
function dologin(data) {
  return utils_request.service({
    url: "/dologin.html",
    method: "POST",
    data
  });
}
function dologinPhone(data) {
  return utils_request.service({
    url: "/dologinPhone.html",
    method: "POST",
    data
  });
}
function getOpenId(data) {
  return utils_request.service({
    url: "/getOpenId.html",
    method: "POST",
    data
  });
}
function getPhoneNumber(data) {
  return utils_request.service({
    url: "/getPhoneNumber.html",
    method: "GET",
    data
  });
}
function dologinWx(data) {
  return utils_request.service({
    url: "/dologinWx.html",
    method: "POST",
    data
  });
}
function register(data) {
  return utils_request.service({
    url: "/register.html",
    method: "POST",
    data
  });
}
function logout(data) {
  return utils_request.service({
    url: "/logout.html",
    method: "GET",
    data
  });
}
exports.doforgetpassword = doforgetpassword;
exports.dologin = dologin;
exports.dologinPhone = dologinPhone;
exports.dologinWx = dologinWx;
exports.getOpenId = getOpenId;
exports.getPhoneNumber = getPhoneNumber;
exports.logout = logout;
exports.register = register;
exports.resetPwd = resetPwd;
exports.sendVerifySMS = sendVerifySMS;
exports.updatephone = updatephone;
exports.verify = verify;
