"use strict";
const utils_request = require("../utils/request.js");
function ordersInfo(data) {
  return utils_request.service({
    url: "/orders/info.html",
    method: "GET",
    data
  });
}
function ordersSubmit(data) {
  return utils_request.service({
    url: "/orders/submit.html",
    method: "POST",
    data
  });
}
function memberOrdersList(data) {
  return utils_request.service({
    url: "/member/orders/list.html",
    method: "GET",
    data
  });
}
function memberOrdersCancelOrder(data) {
  return utils_request.service({
    url: "/member/orders/cancelOrder.html",
    method: "POST",
    data
  });
}
function memberOrdersConfirmOrder(data) {
  return utils_request.service({
    url: "/member/orders/confirmOrder.html",
    method: "POST",
    data
  });
}
function memberOrdersGetOrders(data) {
  return utils_request.service({
    url: "/member/orders/getOrders.html",
    method: "GET",
    data
  });
}
function getCoupon(data) {
  return utils_request.service({
    url: "/orders/getCoupon.html",
    method: "GET",
    data
  });
}
function backapply(data) {
  return utils_request.service({
    url: "/member/backapply.html",
    method: "GET",
    data
  });
}
function canbackorexchange(data) {
  return utils_request.service({
    url: "/member/canbackorexchange.html",
    method: "GET",
    data
  });
}
function doproductback(data) {
  return utils_request.service({
    url: "/member/doproductback.html",
    method: "POST",
    data
  });
}
function doproductexchange(data) {
  return utils_request.service({
    url: "/member/doproductexchange.html",
    method: "POST",
    data
  });
}
function back(data) {
  return utils_request.service({
    url: "/member/back.html",
    method: "GET",
    data
  });
}
function exchange(data) {
  return utils_request.service({
    url: "/member/exchange.html",
    method: "GET",
    data
  });
}
function backDeliverGoods(data) {
  return utils_request.service({
    url: "/member/backDeliverGoods.html",
    method: "GET",
    data
  });
}
function doBackDeliverGoods(data) {
  return utils_request.service({
    url: "/member/doBackDeliverGoods.html",
    method: "POST",
    data
  });
}
function lookBackLogistics(data) {
  return utils_request.service({
    url: "/member/lookBackLogistics.html",
    method: "GET",
    data
  });
}
function exchangeDeliverGoods(data) {
  return utils_request.service({
    url: "/member/exchangeDeliverGoods.html",
    method: "GET",
    data
  });
}
function doExchangeDeliverGoods(data) {
  return utils_request.service({
    url: "/member/doExchangeDeliverGoods.html",
    method: "POST",
    data
  });
}
function doReceiveExchangeGoods(data) {
  return utils_request.service({
    url: "/member/doReceiveExchangeGoods.html",
    method: "POST",
    data
  });
}
function lookExchangeLogistics(data) {
  return utils_request.service({
    url: "/member/lookExchangeLogistics.html",
    method: "GET",
    data
  });
}
exports.back = back;
exports.backDeliverGoods = backDeliverGoods;
exports.backapply = backapply;
exports.canbackorexchange = canbackorexchange;
exports.doBackDeliverGoods = doBackDeliverGoods;
exports.doExchangeDeliverGoods = doExchangeDeliverGoods;
exports.doReceiveExchangeGoods = doReceiveExchangeGoods;
exports.doproductback = doproductback;
exports.doproductexchange = doproductexchange;
exports.exchange = exchange;
exports.exchangeDeliverGoods = exchangeDeliverGoods;
exports.getCoupon = getCoupon;
exports.lookBackLogistics = lookBackLogistics;
exports.lookExchangeLogistics = lookExchangeLogistics;
exports.memberOrdersCancelOrder = memberOrdersCancelOrder;
exports.memberOrdersConfirmOrder = memberOrdersConfirmOrder;
exports.memberOrdersGetOrders = memberOrdersGetOrders;
exports.memberOrdersList = memberOrdersList;
exports.ordersInfo = ordersInfo;
exports.ordersSubmit = ordersSubmit;
