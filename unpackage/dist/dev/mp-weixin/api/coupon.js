"use strict";
const utils_request = require("../utils/request.js");
function couponProduct(data) {
  return utils_request.service({
    url: "/couponProduct.html",
    method: "GET",
    data
  });
}
exports.couponProduct = couponProduct;
