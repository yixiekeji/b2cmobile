"use strict";
const utils_request = require("../utils/request.js");
function integralBanner(data) {
  return utils_request.service({
    url: "/integral-banner.html",
    method: "GET",
    data
  });
}
function integralType(data) {
  return utils_request.service({
    url: "/integral-type.html",
    method: "GET",
    data
  });
}
function jifen(data) {
  return utils_request.service({
    url: "/jifen.html",
    method: "GET",
    data
  });
}
function issign(data) {
  return utils_request.service({
    url: "/is-sign.html",
    method: "GET",
    data
  });
}
function sign(data) {
  return utils_request.service({
    url: "/member/sign.html",
    method: "POST",
    data
  });
}
function jifenDetail(data) {
  return utils_request.service({
    url: "/jifen/detail.html",
    method: "GET",
    data
  });
}
function detailgoods(data) {
  return utils_request.service({
    url: "/detailgoods.html",
    method: "GET",
    data
  });
}
function integral(data) {
  return utils_request.service({
    url: "/member/integral.html",
    method: "GET",
    data
  });
}
function grade(data) {
  return utils_request.service({
    url: "/member/grade.html",
    method: "GET",
    data
  });
}
function ordersjifen(data) {
  return utils_request.service({
    url: "/orders/jifen.html",
    method: "GET",
    data
  });
}
function submitintegral(data) {
  return utils_request.service({
    url: "/orders/submitintegral.html",
    method: "POST",
    data
  });
}
exports.detailgoods = detailgoods;
exports.grade = grade;
exports.integral = integral;
exports.integralBanner = integralBanner;
exports.integralType = integralType;
exports.issign = issign;
exports.jifen = jifen;
exports.jifenDetail = jifenDetail;
exports.ordersjifen = ordersjifen;
exports.sign = sign;
exports.submitintegral = submitintegral;
