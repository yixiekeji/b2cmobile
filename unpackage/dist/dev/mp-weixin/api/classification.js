"use strict";
const utils_request = require("../utils/request.js");
function getIndexCate(data) {
  return utils_request.service({
    url: "/index/getIndexCate.html",
    method: "GET"
  });
}
function productlist(data) {
  return utils_request.service({
    url: "/product/productlist.html",
    method: "GET",
    data
  });
}
function productDetail(data) {
  return utils_request.service({
    url: "/product/detail.html",
    method: "GET",
    data
  });
}
function cartSave(data) {
  return utils_request.service({
    url: "/cart/save.html",
    method: "post",
    data
  });
}
function detailgoods(data) {
  return utils_request.service({
    url: "/product/detailgoods.html",
    method: "GET",
    data
  });
}
exports.cartSave = cartSave;
exports.detailgoods = detailgoods;
exports.getIndexCate = getIndexCate;
exports.productDetail = productDetail;
exports.productlist = productlist;
