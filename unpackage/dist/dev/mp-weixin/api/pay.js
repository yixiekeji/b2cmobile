"use strict";
const utils_request = require("../utils/request.js");
function xcxpayindex(data) {
  return utils_request.service({
    url: "/pay/xcx/payindex.html",
    method: "POST",
    data
  });
}
exports.xcxpayindex = xcxpayindex;
