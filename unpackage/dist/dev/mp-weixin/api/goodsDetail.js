"use strict";
const utils_request = require("../utils/request.js");
function productDetail(data) {
  return utils_request.service({
    url: "/product/detail.html",
    method: "GET",
    data
  });
}
function cartSave(data) {
  return utils_request.service({
    url: "/cart/save.html",
    method: "post",
    data
  });
}
function detailgoods(data) {
  return utils_request.service({
    url: "/product/detailgoods.html",
    method: "GET",
    data
  });
}
function getEffectiveCoupon(data) {
  return utils_request.service({
    url: "/product/getEffectiveCoupon.html",
    method: "GET",
    data
  });
}
function reveivecoupon(data) {
  return utils_request.service({
    url: "/member/coupon/reveivecoupon.html",
    method: "POST",
    data
  });
}
function getTransportPrice(data) {
  return utils_request.service({
    url: "/product/getTransportPrice.html",
    method: "GET",
    data
  });
}
exports.cartSave = cartSave;
exports.detailgoods = detailgoods;
exports.getEffectiveCoupon = getEffectiveCoupon;
exports.getTransportPrice = getTransportPrice;
exports.productDetail = productDetail;
exports.reveivecoupon = reveivecoupon;
