"use strict";
const utils_request = require("../utils/request.js");
function commentlist(data) {
  return utils_request.service({
    url: "/product/commentlist.html",
    method: "GET",
    data
  });
}
function comment(data) {
  return utils_request.service({
    url: "/product/comment.html",
    method: "GET",
    data
  });
}
exports.comment = comment;
exports.commentlist = commentlist;
