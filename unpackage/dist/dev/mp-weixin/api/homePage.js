"use strict";
const utils_request = require("../utils/request.js");
function floor(data) {
  return utils_request.service({
    url: "/index/floor.html",
    method: "GET",
    data
  });
}
function banner(data) {
  return utils_request.service({
    url: "/index/banner.html",
    method: "GET",
    data
  });
}
function indexLike(data) {
  return utils_request.service({
    url: "/index/indexLike.html",
    method: "GET",
    data
  });
}
function newslist(data) {
  return utils_request.service({
    url: "/newslist.html",
    method: "GET",
    data
  });
}
function newstype(data) {
  return utils_request.service({
    url: "/news/type.html",
    method: "GET",
    data
  });
}
function newsDetail(data) {
  return utils_request.service({
    url: "/newsDetail.html",
    method: "GET",
    data
  });
}
exports.banner = banner;
exports.floor = floor;
exports.indexLike = indexLike;
exports.newsDetail = newsDetail;
exports.newslist = newslist;
exports.newstype = newstype;
