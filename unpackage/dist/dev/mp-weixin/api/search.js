"use strict";
const utils_request = require("../utils/request.js");
function searchList(data) {
  return utils_request.service({
    url: "/search.html",
    method: "GET",
    data
  });
}
function searchIndex(data) {
  return utils_request.service({
    url: "/search-index.html",
    method: "GET",
    data
  });
}
function delSearch(data) {
  return utils_request.service({
    url: "/delSearch.html",
    method: "GET",
    data
  });
}
exports.delSearch = delSearch;
exports.searchIndex = searchIndex;
exports.searchList = searchList;
