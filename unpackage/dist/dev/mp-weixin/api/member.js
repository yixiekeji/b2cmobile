"use strict";
const utils_request = require("../utils/request.js");
function memberInfo(data) {
  return utils_request.service({
    url: "/member/info.html",
    method: "GET",
    data
  });
}
function memberindex(data) {
  return utils_request.service({
    url: "/member/index.html",
    method: "GET",
    data
  });
}
function memberUpdateInfo(data) {
  return utils_request.service({
    url: "/member/updateInfo.html",
    method: "POST",
    data
  });
}
function shopCollectionCreate(data) {
  return utils_request.service({
    url: "/member/collection/create.html",
    method: "POST",
    data
  });
}
function shopCollectionCancel(data) {
  return utils_request.service({
    url: "/member/collection/delete.html",
    method: "POST",
    data
  });
}
function collectionList(data) {
  return utils_request.service({
    url: "/member/collection/list",
    method: "GET",
    data
  });
}
function address(data) {
  return utils_request.service({
    url: "/member/listAll.html",
    method: "GET",
    data
  });
}
function deleteaddress(data) {
  return utils_request.service({
    url: "/member/delete.html",
    method: "POST",
    data
  });
}
function updateaddress(data) {
  return utils_request.service({
    url: "/member/update.html",
    method: "POST",
    data
  });
}
function createaddress(data) {
  return utils_request.service({
    url: "/member/create.html",
    method: "POST",
    data
  });
}
function getMemberAddress(data) {
  return utils_request.service({
    url: "/member/getMemberAddress.html",
    method: "GET",
    data
  });
}
function auditaddress(data) {
  return utils_request.service({
    url: "/member/audit.html",
    method: "POST",
    data
  });
}
function couponList(data) {
  return utils_request.service({
    url: "/member/coupon/list.html",
    method: "GET",
    data
  });
}
function wdlcoupon(data) {
  return utils_request.service({
    url: "/coupon.html",
    method: "GET",
    data
  });
}
function convertCoupon(data) {
  return utils_request.service({
    url: "/member/coupon/convertCoupon.html",
    method: "POST",
    data
  });
}
function savecomment(data) {
  return utils_request.service({
    url: "/member/savecomment.html",
    method: "POST",
    data
  });
}
function orderscomment(data) {
  return utils_request.service({
    url: "/member/orderscomment.html",
    method: "GET",
    data
  });
}
function getProductComment(data) {
  return utils_request.service({
    url: "/member/getProductComment.html",
    method: "GET",
    data
  });
}
function commentlist(data) {
  return utils_request.service({
    url: "/member/commentlist.html",
    method: "GET",
    data
  });
}
function deleted(data) {
  return utils_request.service({
    url: "/member/delete.html",
    method: "GET",
    data
  });
}
function getAllPCA(data) {
  return utils_request.service({
    url: "/system/getAllRegions.html",
    method: "GET",
    data
  });
}
exports.address = address;
exports.auditaddress = auditaddress;
exports.collectionList = collectionList;
exports.commentlist = commentlist;
exports.convertCoupon = convertCoupon;
exports.couponList = couponList;
exports.createaddress = createaddress;
exports.deleteaddress = deleteaddress;
exports.deleted = deleted;
exports.getAllPCA = getAllPCA;
exports.getMemberAddress = getMemberAddress;
exports.getProductComment = getProductComment;
exports.memberInfo = memberInfo;
exports.memberUpdateInfo = memberUpdateInfo;
exports.memberindex = memberindex;
exports.orderscomment = orderscomment;
exports.savecomment = savecomment;
exports.shopCollectionCancel = shopCollectionCancel;
exports.shopCollectionCreate = shopCollectionCreate;
exports.updateaddress = updateaddress;
exports.wdlcoupon = wdlcoupon;
