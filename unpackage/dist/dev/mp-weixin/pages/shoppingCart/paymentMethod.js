"use strict";
const common_vendor = require("../../common/vendor.js");
const api_order = require("../../api/order.js");
const api_pay = require("../../api/pay.js");
require("../../store/order.js");
if (!Array) {
  const _component_HeadNav = common_vendor.resolveComponent("HeadNav");
  _component_HeadNav();
}
const _sfc_main = {
  __name: "paymentMethod",
  setup(__props) {
    const orderSn = common_vendor.ref("");
    const id = common_vendor.ref("");
    let payAmount = common_vendor.ref(0);
    common_vendor.ref("wxh5");
    let codes = common_vendor.ref("");
    common_vendor.ref(false);
    let orderList = common_vendor.ref([]);
    const orders = common_vendor.ref({});
    function getDetails() {
      console.log(1111);
      api_order.memberOrdersGetOrders({
        id: id.value
      }).then((res) => {
        orderList.value = res.data.data.ordersProductList;
        res.data.data.ordersProductList.forEach((item) => {
          payAmount.value += Number(item.moneyAmount);
        });
        orders.value = res.data.data.orders;
        orderSn.value = res.data.data.orders.ordersSn;
      });
    }
    let formContent = common_vendor.ref("");
    common_vendor.ref(0);
    function payment(flagPay) {
      {
        common_vendor.index.login({
          provider: "weixin",
          success(res) {
            if (res.code) {
              codes.value = res.code;
              api_pay.xcxpayindex({
                code: res.code,
                orderPaySn: orderSn.value
              }).then((result) => {
                if (!result.data.success && result.data.message.indexOf(
                  "out_trade_no已经在其他合单中使用过"
                ) != -1) {
                  console.log("11111", result);
                  payment();
                  return;
                }
                console.log(result.data.data);
                common_vendor.wx$1.requestPayment({
                  "timeStamp": result.data.data.timeStamp,
                  //后端返回的时间戳
                  "nonceStr": result.data.data.nonceStr,
                  //后端返回的随机字符串
                  "package": result.data.data.package,
                  //后端返回的prepay_id
                  "signType": "MD5",
                  //后端签名算法,根据后端来,后端MD5这里即为MD5
                  "paySign": result.data.data.paySign,
                  //后端返回的签名
                  success(res2) {
                    console.log("用户支付扣款成功", res2);
                    if (orders.value.ordersType == "2") {
                      common_vendor.index.navigateTo({
                        url: "/pages/shoppingCart/paySuccess?price=" + orders.value.moneyOrder + "&jifen=" + orders.value.integral
                      });
                    } else {
                      common_vendor.index.navigateTo({
                        url: "/pages/shoppingCart/paySuccess?price=" + orders.value.moneyOrder
                      });
                    }
                  },
                  fail(res2) {
                    console.log("用户支付扣款失败", res2);
                    common_vendor.index.navigateTo({
                      url: "/pages/memberCenter/myOrder/myOrder?stateid=1"
                    });
                  }
                });
              });
            } else {
              console.log("登录失败！" + res.errMsg);
            }
          }
        });
      }
    }
    common_vendor.onLoad((options) => {
      id.value = options.id;
      getDetails();
    });
    return (_ctx, _cache) => {
      return {
        a: common_vendor.p({
          ["nav-background-color"]: "#F5F5F7",
          backImageUrl: "1",
          textContent: "在线支付",
          fontWeight: "600",
          fontSize: "15"
        }),
        b: common_vendor.t(orders.value.ordersType == "2" ? orders.value.integral + "积分+" : ""),
        c: common_vendor.t(orders.value.moneyOrder),
        d: common_vendor.t(orders.value.ordersSn),
        e: common_vendor.t(orders.value.createTime),
        f: common_vendor.t(orders.value.ordersType == "2" ? orders.value.integral + "积分+" : ""),
        g: common_vendor.t(orders.value.moneyOrder),
        h: common_vendor.o(($event) => payment()),
        i: common_vendor.unref(formContent)
      };
    };
  }
};
wx.createPage(_sfc_main);
