"use strict";
const common_vendor = require("../../common/vendor.js");
const common_assets = require("../../common/assets.js");
require("../../store/order.js");
if (!Array) {
  const _component_HeadNav = common_vendor.resolveComponent("HeadNav");
  _component_HeadNav();
}
const _sfc_main = {
  __name: "paySuccess",
  setup(__props) {
    const price = common_vendor.ref("");
    const jifen = common_vendor.ref("");
    function jumpAddress(url) {
      common_vendor.index.switchTab({
        url
      });
    }
    function onPageJump(url) {
      common_vendor.index.redirectTo({
        url
      });
    }
    function isWeixin() {
      return true;
    }
    common_vendor.onLoad((options) => {
      jifen.value = options.jifen ? options.jifen : "";
      if (options.money == "11") {
        price.value = common_vendor.index.getStorageSync("money");
      } else {
        price.value = options.price;
      }
    });
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: common_vendor.p({
          ["nav-background-color"]: "#F5F5F7",
          backImageUrl: "1",
          textContent: "支付成功",
          fontWeight: "600",
          fontSize: "15"
        }),
        b: common_assets._imports_0$9,
        c: common_vendor.t(jifen.value ? jifen.value + "积分" : ""),
        d: common_vendor.t(jifen.value && price.value && price.value != 0 ? "+" : ""),
        e: common_vendor.t(price.value && price.value != 0 ? "￥" + price.value : ""),
        f: common_vendor.o(($event) => jumpAddress("/pages/homePage/homepage")),
        g: common_vendor.o(($event) => onPageJump("/pages/memberCenter/myOrder/myOrder?stateid=0")),
        h: !isWeixin()
      }, {}, {
        i: common_vendor.o(($event) => jumpAddress("/pages/homePage/homepage"))
      });
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-fddc6db5"]]);
wx.createPage(MiniProgramPage);
