"use strict";
const common_vendor = require("../../common/vendor.js");
const common_assets = require("../../common/assets.js");
const api_order = require("../../api/order.js");
const api_member = require("../../api/member.js");
const api_pay = require("../../api/pay.js");
const store_order = require("../../store/order.js");
if (!Array) {
  const _component_HeadNav = common_vendor.resolveComponent("HeadNav");
  const _easycom_uni_popup_dialog2 = common_vendor.resolveComponent("uni-popup-dialog");
  const _easycom_uni_popup2 = common_vendor.resolveComponent("uni-popup");
  const _easycom_uni_icons2 = common_vendor.resolveComponent("uni-icons");
  (_component_HeadNav + _easycom_uni_popup_dialog2 + _easycom_uni_popup2 + _easycom_uni_icons2)();
}
const _easycom_uni_popup_dialog = () => "../../uni_modules/uni-popup/components/uni-popup-dialog/uni-popup-dialog.js";
const _easycom_uni_popup = () => "../../uni_modules/uni-popup/components/uni-popup/uni-popup.js";
const _easycom_uni_icons = () => "../../uni_modules/uni-icons/components/uni-icons/uni-icons.js";
if (!Math) {
  (_easycom_uni_popup_dialog + _easycom_uni_popup + _easycom_uni_icons)();
}
const _sfc_main = {
  __name: "confirmOrder",
  setup(__props) {
    let details = common_vendor.ref({});
    const addressFlag = common_vendor.ref(false);
    let addressed = common_vendor.ref([]);
    const addressId = common_vendor.ref("");
    const invoice = common_vendor.ref("0");
    const notes = common_vendor.ref("");
    const invoiceId = common_vendor.ref("");
    const invoiceInfo = common_vendor.ref("");
    common_vendor.ref("");
    const state = common_vendor.ref("1");
    const couponSn = common_vendor.ref("");
    const couponMoney = common_vendor.ref("");
    const orderSn = common_vendor.ref("");
    common_vendor.ref(0);
    common_vendor.ref("wxh5");
    let codes = common_vendor.ref("");
    common_vendor.ref(false);
    function getDetails() {
      if (store_order.order().order.productId && store_order.order().order.productId != "") {
        api_order.ordersInfo({
          buy: 1,
          productId: store_order.order().order.productId,
          productGoodId: store_order.order().order.productGoodId,
          number: store_order.order().order.number,
          addressId: addressId.value
        }).then((res) => {
          details.value = res.data.data;
        });
      } else {
        api_order.ordersInfo({
          buy: 0,
          addressId: addressId.value
        }).then((res) => {
          details.value = res.data.data;
        });
      }
    }
    function getAddress(id) {
      if (id == "00") {
        api_member.address().then((res) => {
          console.log(res);
          if (res.data.data.length == 0) {
            addressFlag.value = false;
          } else {
            addressed.value = res.data.data;
            addressId.value = res.data.data[0].id;
            store_order.order().setAddressId(addressId.value);
            addressed.value.length == 0 ? addressFlag.value = false : addressFlag.value = true;
          }
          getDetails();
        });
      } else {
        api_member.getMemberAddress({
          id
        }).then((res) => {
          addressed.value = [];
          addressed.value.push(res.data.data);
          addressId.value = res.data.data.id;
          store_order.order().setAddressId(addressId.value);
          addressed.value.length == 0 ? addressFlag.value = false : addressFlag.value = true;
          getDetails();
        });
      }
    }
    const couponDialog = common_vendor.ref();
    function choosecoupon() {
      getList();
      couponDialog.value.open();
    }
    function closecoupon() {
      couponSn.value = "";
      couponMoney.value = "";
      store_order.order().setCouponSn("");
      couponDialog.value.close();
    }
    const tabs = common_vendor.reactive([
      {
        id: 2,
        name: "可用优惠券"
      },
      {
        id: 1,
        name: "不可用优惠券"
      }
    ]);
    const currentTab = common_vendor.ref(1);
    const tabCurrent = common_vendor.ref("tabNum1");
    common_vendor.ref(0);
    const top = common_vendor.ref(0);
    common_vendor.ref(1);
    let list = common_vendor.ref([]);
    const couponSns = common_vendor.ref("");
    common_vendor.ref(false);
    function swichMenu(val) {
      list.value = [];
      currentTab.value = val;
      tabCurrent.value = "tabNum" + val;
      getList();
    }
    function getList() {
      if (store_order.order().order.productId && store_order.order().order.productId != "") {
        api_order.getCoupon({
          buy: 1,
          productId: store_order.order().order.productId,
          canUse: currentTab.value,
          productGoodId: store_order.order().order.productGoodId,
          num: store_order.order().order.number
        }).then((res) => {
          list.value = [...res.data.data];
        });
      } else {
        api_order.getCoupon({
          buy: 0,
          productId: 0,
          canUse: currentTab.value
        }).then((res) => {
          list.value = [...res.data.data];
        });
      }
    }
    function exchange() {
      if (!couponSns.value) {
        common_vendor.index.showToast({
          title: "请填写兑换码",
          duration: 2e3,
          icon: "none"
        });
        return;
      }
      api_member.convertCoupon({
        couponSn: couponSns.value
      }).then((res) => {
        console.log(res);
        if (res.data.success) {
          common_vendor.index.showToast({
            title: "兑换成功",
            duration: 2e3,
            icon: "none"
          });
          couponSns.value = "";
          search();
        }
      });
    }
    function choosecoupons(e, val, money) {
      console.log(e);
      if (e.target.id == "watch") {
        return;
      }
      if (currentTab.value == 1) {
        couponSn.value = val;
        couponMoney.value = money;
        store_order.order().setCouponSn(val);
        store_order.order().setCouponMoney(money);
        couponDialog.value.close();
      }
    }
    function jumpPage(val, couponName, useEndTime, couponValue, minAmount, linkProduct) {
      common_vendor.index.navigateTo({
        url: "/pages/memberCenter/couponsPage/couponsPage?id=" + val + "&couponName=" + couponName + "&useEndTime=" + useEndTime + "&couponValue=" + couponValue + "&minAmount=" + minAmount + "&linkProduct=" + linkProduct + "&flag=1"
      });
    }
    function search() {
      list.value = [];
      getList();
    }
    const popup = common_vendor.ref();
    function addNotes() {
      popup.value.open();
    }
    function close() {
      popup.value.close();
    }
    function confirm(value) {
      if (value.length > 50) {
        common_vendor.index.showToast({
          icon: "none",
          duration: 3e3,
          title: "请将订单备注控制在50字以内"
        });
      } else {
        notes.value = value;
        store_order.order().setRemarks(value);
        popup.value.close();
      }
    }
    const popups = common_vendor.ref();
    function confirmOrders() {
      if (addressId.value == "") {
        common_vendor.index.showToast({
          icon: "none",
          duration: 3e3,
          title: "请选择收货地址"
        });
      } else {
        popups.value.open("center");
        if (store_order.order().order.productId && store_order.order().order.productId != "") {
          api_order.ordersSubmit({
            addressId: addressId.value,
            invoiceId: invoiceId.value,
            invoiceInfo: invoiceInfo.value,
            invoiceStatus: invoice.value,
            remark: notes.value,
            payState: state.value,
            couponSn: couponSn.value,
            buy: 1,
            productId: store_order.order().order.productId,
            productGoodId: store_order.order().order.productGoodId,
            number: store_order.order().order.number
          }).then((res) => {
            if (res.data.success) {
              orderSn.value = res.data.data.ordersSn;
              payment();
            } else {
              popups.value.close("center");
            }
          });
        } else {
          api_order.ordersSubmit({
            addressId: addressId.value,
            buy: 0,
            invoiceStatus: invoice.value,
            payState: state.value,
            invoiceId: invoiceId.value,
            invoiceInfo: invoiceInfo.value,
            couponSn: couponSn.value,
            remark: notes.value
          }).then((res) => {
            if (res.data.success) {
              orderSn.value = res.data.data.ordersSn;
              payment();
            } else {
              popups.value.close("center");
            }
          });
        }
      }
    }
    let formContent = common_vendor.ref("");
    function payment() {
      {
        common_vendor.index.login({
          provider: "weixin",
          success(res) {
            console.log(res);
            if (res.code) {
              codes.value = res.code;
              api_pay.xcxpayindex({
                code: res.code,
                orderPaySn: orderSn.value
              }).then((result) => {
                if (!result.data.success && result.data.message.indexOf(
                  "out_trade_no已经在其他合单中使用过"
                ) != -1) {
                  console.log("11111", result);
                  payment();
                  return;
                }
                console.log(result.data.data);
                common_vendor.wx$1.requestPayment({
                  "timeStamp": result.data.data.timeStamp,
                  //后端返回的时间戳
                  "nonceStr": result.data.data.nonceStr,
                  //后端返回的随机字符串
                  "package": result.data.data.package,
                  //后端返回的prepay_id
                  "signType": "MD5",
                  //后端签名算法,根据后端来,后端MD5这里即为MD5
                  "paySign": result.data.data.paySign,
                  //后端返回的签名
                  success(res2) {
                    console.log("用户支付扣款成功", res2);
                    common_vendor.index.navigateTo({
                      url: "/pages/shoppingCart/paySuccess?price=" + details.value.cartAmount
                    });
                  },
                  fail(res2) {
                    console.log("用户支付扣款失败", res2);
                    common_vendor.index.navigateTo({
                      url: "/pages/memberCenter/myOrder/myOrder?orderState=1"
                    });
                  }
                });
              });
            } else {
              console.log("登录失败！" + res.errMsg);
            }
          }
        });
      }
    }
    function jumpAddress() {
      common_vendor.index.redirectTo({
        url: "/pages/memberCenter/address/address?order=order"
      });
    }
    function jumpInvoice() {
      common_vendor.index.redirectTo({
        url: "/pages/memberCenter/invoice/alertInvoice?invoice=invoice"
      });
    }
    function getUrlValue(value) {
      var query = location.search.substring(1);
      var hash = location.hash.split("?");
      if (!query && hash.length < 2) {
        return "";
      }
      var vars = query.split("&");
      for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        if (pair[0] == value) {
          return pair[1];
        }
      }
      if (!hash[1]) {
        return "";
      }
      vars = hash[1].split("&");
      for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        if (pair[0] == value) {
          return pair[1];
        }
      }
      return "";
    }
    common_vendor.onLoad((options) => {
      if (store_order.order().order.addressId != "") {
        getAddress(store_order.order().order.addressId);
      } else {
        getAddress("00");
      }
      couponSn.value = store_order.order().order.couponSn ? store_order.order().order.couponSn : "";
      couponMoney.value = store_order.order().order.couponMoney ? store_order.order().order.couponMoney : "";
      invoice.value = store_order.order().order.invoice.invoiceStatus ? store_order.order().order.invoice.invoiceStatus : "";
      invoiceId.value = store_order.order().order.invoice.invoiceId ? store_order.order().order.invoice.invoiceId : "";
      invoiceInfo.value = store_order.order().order.invoice.invoiceTitle ? store_order.order().order.invoice.invoiceTitle : "";
      notes.value = store_order.order().order.remarks ? store_order.order().order.remarks : "";
      orderSn.value = store_order.order().order.orderSn;
      if (getUrlValue("code")) {
        payment();
      }
    });
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: common_vendor.p({
          ["nav-background-color"]: "#f7f7f5",
          backImageUrl: "1",
          textContent: "确认订单",
          fontSize: "15",
          fontWeight: "600"
        }),
        b: addressFlag.value
      }, addressFlag.value ? {
        c: common_assets._imports_0$4,
        d: common_vendor.t(common_vendor.unref(addressed)[0].name),
        e: common_vendor.t(common_vendor.unref(addressed)[0].mobile),
        f: common_vendor.t(common_vendor.unref(addressed)[0].addAll + common_vendor.unref(addressed)[0].addressInfo),
        g: common_assets._imports_1$4,
        h: common_vendor.o(jumpAddress)
      } : {
        i: common_vendor.o(jumpAddress)
      }, {
        j: common_vendor.f(common_vendor.unref(details).ordersCartList, (item, k0, i0) => {
          return common_vendor.e({
            a: _ctx.imageUrl + item.masterImg,
            b: common_vendor.t(item.name),
            c: item.specInfo
          }, item.specInfo ? {
            d: common_vendor.t(item.specInfo)
          } : {}, {
            e: common_vendor.t(item.mallPriceAll),
            f: common_vendor.t(item.count)
          });
        }),
        k: common_vendor.t(common_vendor.unref(details).cartAmount),
        l: common_vendor.t(common_vendor.unref(details).logisticsFeeAmount),
        m: common_vendor.t(couponSn.value ? "￥" + couponMoney.value : ""),
        n: common_assets._imports_1$4,
        o: common_vendor.o(choosecoupon),
        p: common_vendor.t(invoiceInfo.value),
        q: common_assets._imports_1$4,
        r: common_vendor.o(jumpInvoice),
        s: common_vendor.t(notes.value == "" ? "无备注" : notes.value),
        t: common_assets._imports_1$4,
        v: common_vendor.o(addNotes),
        w: common_vendor.t(common_vendor.unref(details).cartAmount + common_vendor.unref(details).logisticsFeeAmount - (couponMoney.value ? couponMoney.value : 0)),
        x: common_vendor.o(confirmOrders),
        y: common_vendor.sr("inputClose", "f71379f0-2,f71379f0-1"),
        z: common_vendor.o(confirm),
        A: common_vendor.o(close),
        B: common_vendor.p({
          mode: "input",
          title: "备注",
          value: notes.value,
          placeholder: "请输入内容"
        }),
        C: common_vendor.sr(popup, "f71379f0-1", {
          "k": "popup"
        }),
        D: common_vendor.p({
          type: "dialog"
        }),
        E: _ctx.type === "left" || _ctx.type === "right" ? 1 : "",
        F: common_vendor.sr(popups, "f71379f0-3", {
          "k": "popups"
        }),
        G: common_vendor.p({
          ["is-mask-click"]: false
        }),
        H: common_vendor.o(($event) => closecoupon()),
        I: common_vendor.p({
          type: "closeempty",
          size: "18",
          color: "#1E1E1F"
        }),
        J: common_vendor.f(tabs, (item, k0, i0) => {
          return {
            a: common_vendor.t(item.name),
            b: common_vendor.n(currentTab.value == item.id - 1 ? "menu-topic-act" : "menu-topic"),
            c: "tabNum" + item.id,
            d: item.id - 1,
            e: common_vendor.o(($event) => swichMenu(item.id - 1), item.id - 1)
          };
        }),
        K: top.value,
        L: currentTab.value == 1
      }, currentTab.value == 1 ? {
        M: couponSns.value,
        N: common_vendor.o(($event) => couponSns.value = $event.detail.value),
        O: common_vendor.o(exchange)
      } : {}, {
        P: common_vendor.f(common_vendor.unref(list), (item, k0, i0) => {
          return common_vendor.e({
            a: common_vendor.t(item.couponName),
            b: common_vendor.t(item.useEndTime),
            c: common_vendor.t(item.couponValue),
            d: common_vendor.t(item.minAmount),
            e: common_vendor.t(item.linkProduct == 1 ? "全部商品" : "部分商品"),
            f: item.linkProduct != 1
          }, item.linkProduct != 1 ? {
            g: common_vendor.o(($event) => jumpPage(item.couponId, item.couponName, item.useEndTime, item.couponValue, item.minAmount, item.linkProduct))
          } : {}, {
            h: currentTab.value == 0 || item.couponSn == couponSn.value
          }, currentTab.value == 0 || item.couponSn == couponSn.value ? {} : {}, {
            i: item.timeout
          }, item.timeout ? {
            j: common_assets._imports_0$10
          } : {}, {
            k: item.use
          }, item.use ? {
            l: common_assets._imports_1$5
          } : {}, {
            m: common_vendor.o(($event) => choosecoupons($event, item.couponSn, item.couponValue))
          });
        }),
        Q: common_vendor.unref(list).length == 0
      }, common_vendor.unref(list).length == 0 ? {
        R: common_assets._imports_2$3
      } : {}, {
        S: common_vendor.sr(couponDialog, "f71379f0-4", {
          "k": "couponDialog"
        }),
        T: common_vendor.p({
          type: "bottom",
          ["background-color"]: "#F5F5F7",
          ["is-mask-click"]: false
        }),
        U: common_vendor.unref(formContent)
      });
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-f71379f0"]]);
wx.createPage(MiniProgramPage);
