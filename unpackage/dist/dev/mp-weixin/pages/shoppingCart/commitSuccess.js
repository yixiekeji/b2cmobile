"use strict";
const common_vendor = require("../../common/vendor.js");
const common_assets = require("../../common/assets.js");
const _sfc_main = {
  data() {
    return {
      money: ""
    };
  },
  methods: {
    //跳转到指定页面
    jumpAddress(url) {
      common_vendor.index.switchTab({
        url
      });
    },
    onPageJump(url) {
      window.location.href = window.location.href.split("?")[0] + "#" + url;
    }
  },
  onLoad(e) {
    this.money = e.money;
    common_vendor.index.setNavigationBarTitle({
      title: common_vendor.index.getStorageSync("shopName") ? common_vendor.index.getStorageSync("shopName") : "禾鲜谷"
    });
  }
};
if (!Array) {
  const _component_HeadNav = common_vendor.resolveComponent("HeadNav");
  _component_HeadNav();
}
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: common_vendor.p({
      ["nav-background-color"]: "#f7f7f5",
      backImageUrl: "1",
      textContent: "支付成功",
      fontSize: "15",
      fontWeight: "600"
    }),
    b: common_assets._imports_0$9,
    c: common_vendor.t($data.money),
    d: common_vendor.o(($event) => $options.jumpAddress("/pages/homePage/homepage")),
    e: common_vendor.o(($event) => $options.onPageJump("/pages/memberCenter/myOrder/myOrder?stateid=0")),
    f: common_vendor.o(($event) => $options.jumpAddress("/pages/homePage/homepage"))
  };
}
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__scopeId", "data-v-90227208"]]);
wx.createPage(MiniProgramPage);
