"use strict";
const common_vendor = require("../../common/vendor.js");
const common_assets = require("../../common/assets.js");
const api_cart = require("../../api/cart.js");
const store_order = require("../../store/order.js");
if (!Array) {
  const _component_HeadNav = common_vendor.resolveComponent("HeadNav");
  const _easycom_uni_number_box2 = common_vendor.resolveComponent("uni-number-box");
  const _easycom_uni_popup_dialog2 = common_vendor.resolveComponent("uni-popup-dialog");
  const _easycom_uni_popup2 = common_vendor.resolveComponent("uni-popup");
  (_component_HeadNav + _easycom_uni_number_box2 + _easycom_uni_popup_dialog2 + _easycom_uni_popup2)();
}
const _easycom_uni_number_box = () => "../../uni_modules/uni-number-box/components/uni-number-box/uni-number-box.js";
const _easycom_uni_popup_dialog = () => "../../uni_modules/uni-popup/components/uni-popup-dialog/uni-popup-dialog.js";
const _easycom_uni_popup = () => "../../uni_modules/uni-popup/components/uni-popup/uni-popup.js";
if (!Math) {
  (_easycom_uni_number_box + _easycom_uni_popup_dialog + _easycom_uni_popup)();
}
const _sfc_main = {
  __name: "ShoppingCart",
  setup(__props) {
    let goodList = common_vendor.ref([]);
    common_vendor.ref([]);
    const allChecked = common_vendor.ref(false);
    const totalPrice = common_vendor.ref(0);
    const id = common_vendor.ref("");
    const alertDialog = common_vendor.ref();
    const flag = common_vendor.ref(false);
    const dataFlag = common_vendor.ref(false);
    function getCartList() {
      dataFlag.value = false;
      api_cart.cartList().then((res) => {
        console.log(res);
        if (res.data.success) {
          totalPrice.value = res.data.data.checkedCartAmount;
          goodList.value = res.data.data.ordersCartList;
          const cartList = goodList.value.every((item) => {
            return item.checked == 1;
          });
          if (cartList) {
            allChecked.value = 1;
          } else {
            allChecked.value = 0;
          }
          flag.value = true;
        } else {
          goodList.value = [];
          totalPrice.value = 0;
          flag.value = false;
        }
        dataFlag.value = true;
      });
    }
    function changeitem(item) {
      api_cart.cartchecked({
        checked: item.checked == 0 ? 1 : 0,
        id: item.id
      }).then((res) => {
        getCartList();
      });
    }
    function selectAll() {
      api_cart.cartcheckedAll({
        checked: allChecked.value == 0 ? 1 : 0
      }).then((res) => {
        getCartList();
      });
    }
    function addShopCarts(event, count, val) {
      console.log(event, count, val);
      if (count == 0) {
        alertDialog.value.open();
        id.value = val;
      } else {
        if (event.type == "plus") {
          api_cart.addOrdersCart({
            id: val
          }).then((res) => {
            getCartList();
          });
        } else {
          api_cart.reduceOrdersCart({
            id: val
          }).then((res) => {
            getCartList();
          });
        }
      }
    }
    function dialogConfirm() {
      api_cart.reduceOrdersCart({
        id: id.value
      }).then((res) => {
        getCartList();
      });
    }
    function dialogClose() {
      getCartList();
      alertDialog.value.close();
    }
    function jumpDetails(val) {
      common_vendor.index.navigateTo({
        url: "/pages/homePage/goodsDetail?id=" + val
      });
    }
    function jumpLogin(val) {
      common_vendor.index.navigateTo({
        url: val
      });
    }
    function jumpHome() {
      common_vendor.index.switchTab({
        url: "/pages/homePage/homepage"
      });
    }
    function settlement() {
      let result = goodList.value.some((item) => {
        return item.checked == 1;
      });
      if (goodList.value.length == 0) {
        common_vendor.index.showToast({
          icon: "none",
          duration: 3e3,
          title: "购物车没有商品哦~"
        });
      } else if (!result) {
        common_vendor.index.showToast({
          icon: "none",
          duration: 3e3,
          title: "请选择要购买的商品~"
        });
      } else if (result) {
        store_order.order().resize();
        common_vendor.index.navigateTo({
          url: "/pages/shoppingCart/confirmOrder"
        });
      }
    }
    common_vendor.onTabItemTap(() => {
      getCartList();
    });
    common_vendor.onShow(() => {
      getCartList();
    });
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: common_vendor.o(_ctx.test),
        b: common_vendor.p({
          ["nav-background-color"]: "#f7f7f5",
          backImageUrl: "0",
          textContent: "购物车",
          fontSize: "15",
          fontWeight: "600"
        }),
        c: common_vendor.unref(goodList).length != 0
      }, common_vendor.unref(goodList).length != 0 ? {
        d: common_vendor.f(common_vendor.unref(goodList), (item, index, i0) => {
          return common_vendor.e({
            a: item.checked == 1
          }, item.checked == 1 ? {
            b: common_assets._imports_0$3
          } : {
            c: common_assets._imports_1$1
          }, {
            d: common_vendor.o(($event) => changeitem(item)),
            e: common_vendor.o(($event) => jumpDetails(item.productId)),
            f: _ctx.imageUrl + item.masterImg,
            g: common_vendor.t(item.name),
            h: common_vendor.t(item.specInfo),
            i: common_vendor.n(item.specInfo ? "font2" : "font22"),
            j: common_vendor.t(item.mallPrice),
            k: common_vendor.o(($event) => addShopCarts($event, item.count, item.id)),
            l: "a8c6014b-1-" + i0,
            m: common_vendor.o(($event) => item.count = $event),
            n: common_vendor.p({
              modelValue: item.count
            })
          });
        })
      } : {}, {
        e: flag.value
      }, flag.value ? common_vendor.e({
        f: allChecked.value == 1
      }, allChecked.value == 1 ? {
        g: common_assets._imports_0$3
      } : {
        h: common_assets._imports_1$1
      }, {
        i: common_vendor.o(selectAll),
        j: common_vendor.t(totalPrice.value),
        k: common_vendor.o(settlement)
      }) : {}, {
        l: flag.value && common_vendor.unref(goodList).length == 0 && dataFlag.value
      }, flag.value && common_vendor.unref(goodList).length == 0 && dataFlag.value ? {
        m: common_assets._imports_2$1,
        n: common_vendor.o(($event) => jumpHome())
      } : {}, {
        o: !flag.value
      }, !flag.value ? {
        p: common_assets._imports_2$1,
        q: common_vendor.o(($event) => jumpLogin("/pages/loginInterface/passwordLogin"))
      } : {}, {
        r: common_vendor.o(dialogConfirm),
        s: common_vendor.o(dialogClose),
        t: common_vendor.p({
          type: _ctx.msgType,
          cancelText: "取消",
          confirmText: "确认",
          content: "确认删除此条商品吗?"
        }),
        v: common_vendor.sr(alertDialog, "a8c6014b-2", {
          "k": "alertDialog"
        }),
        w: common_vendor.p({
          type: "dialog"
        })
      });
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-a8c6014b"]]);
wx.createPage(MiniProgramPage);
