"use strict";
const common_vendor = require("../../common/vendor.js");
const common_assets = require("../../common/assets.js");
const api_classification = require("../../api/classification.js");
if (!Array) {
  const _component_HeadNav = common_vendor.resolveComponent("HeadNav");
  const _easycom_uni_icons2 = common_vendor.resolveComponent("uni-icons");
  const _easycom_uni_data_checkbox2 = common_vendor.resolveComponent("uni-data-checkbox");
  const _easycom_uni_number_box2 = common_vendor.resolveComponent("uni-number-box");
  const _easycom_uni_popup2 = common_vendor.resolveComponent("uni-popup");
  (_component_HeadNav + _easycom_uni_icons2 + _easycom_uni_data_checkbox2 + _easycom_uni_number_box2 + _easycom_uni_popup2)();
}
const _easycom_uni_icons = () => "../../uni_modules/uni-icons/components/uni-icons/uni-icons.js";
const _easycom_uni_data_checkbox = () => "../../uni_modules/uni-data-checkbox/components/uni-data-checkbox/uni-data-checkbox.js";
const _easycom_uni_number_box = () => "../../uni_modules/uni-number-box/components/uni-number-box/uni-number-box.js";
const _easycom_uni_popup = () => "../../uni_modules/uni-popup/components/uni-popup/uni-popup.js";
if (!Math) {
  (_easycom_uni_icons + _easycom_uni_data_checkbox + _easycom_uni_number_box + _easycom_uni_popup)();
}
const _sfc_main = {
  __name: "classification",
  setup(__props) {
    let catesData = common_vendor.ref([]);
    let productData = common_vendor.ref([]);
    const catePid = common_vendor.ref(0);
    const sort = common_vendor.ref(1);
    const page = common_vendor.ref(1);
    let details = common_vendor.ref({});
    common_vendor.ref(0);
    common_vendor.ref(0);
    common_vendor.ref(0);
    common_vendor.ref("");
    common_vendor.ref([]);
    common_vendor.ref(0);
    common_vendor.ref("dot");
    common_vendor.ref({
      backgroundColor: "rgba(0, 0, 0,0.3)",
      border: "0px rgba(83, 200, 249,0.3) solid",
      color: "#fff",
      selectedBackgroundColor: "#1C9B64",
      selectedBorder: "2rpx solid #FFFFFF"
    });
    common_vendor.ref(0);
    common_vendor.ref(0);
    let norm1s = common_vendor.ref([]);
    const norm1 = common_vendor.ref("");
    const norm1Value = common_vendor.ref("");
    let norm2s = common_vendor.ref([]);
    const norm2 = common_vendor.ref("");
    const norm2Value = common_vendor.ref("");
    common_vendor.ref("");
    const masterImg = common_vendor.ref("");
    const mallPrice = common_vendor.ref("");
    const name = common_vendor.ref("");
    const stock = common_vendor.ref("");
    const productId = common_vendor.ref("");
    const max = common_vendor.ref(1);
    const min = common_vendor.ref(0);
    const numValue = common_vendor.ref(1);
    const productGoodId = common_vendor.ref("");
    const chooseValue = common_vendor.ref("");
    const loading = common_vendor.ref(false);
    const loadingText = common_vendor.ref("加载中...");
    const popup = common_vendor.ref();
    const openFlag = common_vendor.ref(true);
    const classificationFlag = common_vendor.ref(true);
    const cateId = common_vendor.ref("0");
    const shopCates = common_vendor.ref([]);
    const dataFlag = common_vendor.ref(false);
    function open() {
      openFlag.value = !openFlag.value;
      classificationFlag.value = !classificationFlag.value;
    }
    function classification(e) {
      if (e.target.id) {
        cateId.value = e.target.id;
        sort.value = 1;
        page.value = 1;
        productData.value = [];
        loading.value = false;
        getProductList();
      }
    }
    function norm2Change(val) {
      getinfor();
      choose();
    }
    function norm1Change(val) {
      getinfor();
      choose();
    }
    function getinfor() {
      if (norm1.value == 99 && norm2.value == 99) {
        return;
      } else if (norm2.value == 99) {
        setTimeout(() => {
          api_classification.detailgoods({
            normName: chooseValue.value,
            productId: productId.value
          }).then((res) => {
            mallPrice.value = res.data.data.mallPrice;
            if (res.data.data.stock == 0) {
              min.value = 0;
              max.value = 0;
              numValue.value = 0;
            } else {
              min.value = 1;
              max.value = res.data.data.stock;
              numValue.value = 1;
            }
            productGoodId.value = res.data.data.id;
            stock.value = res.data.data.stock == 0 ? "暂无库存" : "剩余" + res.data.data.stock + "件";
          });
        }, 100);
      } else if (norm1.value != 99 && norm2.value != 99) {
        if (norm1Value.value != "" && norm2Value.value != "") {
          setTimeout(() => {
            api_classification.detailgoods({
              normName: chooseValue.value,
              productId: productId.value
            }).then((res) => {
              console.log(res);
              mallPrice.value = res.data.data.mallPrice;
              if (res.data.data.stock == 0) {
                min.value = 0;
                max.value = 0;
                numValue.value = 0;
              } else {
                min.value = 1;
                max.value = res.data.data.stock;
                numValue.value = 1;
              }
              productGoodId.value = res.data.data.id;
              stock.value = res.data.data.stock == 0 ? "暂无库存" : "剩余" + res.data.data.stock + "件";
            });
          }, 100);
        }
      }
    }
    function getProductCates() {
      api_classification.getIndexCate().then((res) => {
        catesData.value = res.data.data;
        shopCates.value = [];
        catesData.value.forEach((item) => {
          shopCates.value = [...shopCates.value, ...item.children];
        });
      });
    }
    function getProductList() {
      dataFlag.value = false;
      api_classification.productlist({
        catePid: catePid.value,
        cateId: cateId.value,
        page: page.value,
        sort: sort.value
      }).then((res) => {
        if (res.data.data.products.length == 0 && page.value > 1 && productData.value.length > 20) {
          loadingText.value = "到底啦~";
        } else {
          productData.value = [...productData.value, ...res.data.data.products];
          loading.value = false;
        }
        dataFlag.value = true;
        console.log(productData.value.length);
      });
    }
    function goodssort(e) {
      if (e.target.id && e.target.id != sort.value) {
        if (e.target.id == 1) {
          sort.value = 1;
        } else if (e.target.id == 2) {
          sort.value = 2;
        } else if (e.target.id == 4) {
          sort.value = 4;
        }
        page.value = 1;
        productData.value = [];
        loading.value = false;
        getProductList();
      } else if (e.target.id == 4) {
        sort.value = 5;
        page.value = 1;
        loading.value = false;
        productData.value = [];
        getProductList();
      }
    }
    function addCart(id, val) {
      if (val != 2) {
        common_vendor.index.showToast({
          icon: "none",
          duration: 3e3,
          title: `该商品已下架`
        });
        return;
      }
      productId.value = id;
      norm1s.value = [];
      norm2s.value = [];
      norm1.value = "";
      norm2.value = "";
      norm1Value.value = "";
      norm2Value.value = "";
      api_classification.productDetail({
        productId: id
      }).then((res) => {
        if (res.data.data.product.isNorm == 0) {
          norm1.value = 99;
          norm2.value = 99;
          productGoodId.value = res.data.data.product.productGoodsList[0].id;
        } else if (res.data.data.product.isNorm == 1) {
          norm1.value = res.data.data.norm1;
          res.data.data.norm1s.forEach((item) => {
            norm1s.value.push({
              text: item,
              value: item
            });
            norm1Value.value = norm1s.value[0].value;
          });
          if (res.data.data.norm2) {
            norm2.value = res.data.data.norm2;
            res.data.data.norm2s.forEach((item) => {
              norm2s.value.push({
                text: item,
                value: item
              });
              norm2Value.value = norm2s.value[0].value;
            });
          } else {
            norm2.value = 99;
          }
          choose();
          getinfor();
        }
        stock.value = res.data.data.product.productGoodsList[0].stock == 0 ? "暂无库存" : "剩余" + res.data.data.product.productGoodsList[0].stock + "件";
        name.value = res.data.data.product.name;
        masterImg.value = res.data.data.product.masterImg;
        mallPrice.value = res.data.data.product.mallPrice;
        details.value = res.data.data.product;
        if (res.data.data.product.productGoodsList[0].stock == 0) {
          min.value = 0;
          max.value = 0;
          numValue.value = 0;
        } else {
          min.value = 1;
          max.value = res.data.data.product.productGoodsList[0].stock;
          numValue.value = 1;
        }
        popup.value.open();
      });
    }
    function close() {
      popup.value.close();
    }
    function confirmAddCart() {
      api_classification.cartSave({
        productId: productId.value,
        number: numValue.value,
        productGoodId: productGoodId.value
      }).then((res) => {
        if (res.data.success) {
          common_vendor.index.showToast({
            icon: "none",
            duration: 3e3,
            title: `加购成功`
          });
        }
        console.log(res);
      });
      popup.value.close();
    }
    const switchImg = common_vendor.ref("");
    function swichMenu(index) {
      if (index == catePid.value)
        return;
      shopCates.value = [];
      console.log(catesData.value);
      catesData.value.forEach((item) => {
        if (item.id == index) {
          switchImg.value = item.image;
          shopCates.value = [...shopCates.value, ...item.children];
        } else if (index == 0) {
          shopCates.value = [...shopCates.value, ...item.children];
        }
      });
      sort.value = 1;
      page.value = 1;
      cateId.value = 0;
      catePid.value = index;
      productData.value = [];
      getProductList();
    }
    function choose() {
      let aaa = "";
      if (norm1.value != 99 && norm1Value.value != "") {
        aaa = norm1.value + ",";
      } else {
        aaa = "";
      }
      let bbb = "";
      if (norm1Value.value != "" && norm2Value.value != "") {
        bbb = ";";
      } else {
        bbb = "";
      }
      let ccc = "";
      if (norm2.value != 99 && norm2Value.value != "") {
        ccc = norm2.value + ",";
      } else {
        ccc = "";
      }
      chooseValue.value = aaa + norm1Value.value + bbb + ccc + norm2Value.value;
    }
    function jumpDetail(e, id) {
      console.log(e.target);
      if (e.target.id != "addCart") {
        common_vendor.index.navigateTo({
          url: "/pages/homePage/goodsDetail?id=" + id
        });
      }
    }
    common_vendor.onShow(() => {
      sort.value = 1;
      page.value = 1;
      cateId.value = 0;
      productData.value = [];
      getProductList();
      getProductCates();
    });
    common_vendor.onReachBottom(() => {
      console.log(productData.value.length);
      if (productData.value.length >= 20 * page.value) {
        page.value += 1;
        loading.value = true;
        loadingText.value = "加载中...";
        setTimeout(() => {
          getProductList();
        }, 800);
      } else if (productData.value.length != 0) {
        console.log(1111222);
        loading.value = true;
        loadingText.value = "到底啦~";
      }
    });
    common_vendor.onPullDownRefresh(() => {
      setTimeout(() => {
        productData.value = [];
        page.value = 1;
        getProductList();
        common_vendor.index.stopPullDownRefresh();
      }, 800);
    });
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: common_vendor.o(_ctx.test),
        b: common_vendor.p({
          ["nav-background-color"]: "#f5f5f7",
          backImageUrl: "8",
          textAlign: "left"
        }),
        c: common_vendor.n(catePid.value == 0 ? "u-tab-item-active" : ""),
        d: common_vendor.o(($event) => swichMenu(0)),
        e: common_vendor.f(common_vendor.unref(catesData), (item, k0, i0) => {
          return {
            a: common_vendor.t(item.name),
            b: common_vendor.n(catePid.value == item.id ? "u-tab-item-active" : ""),
            c: common_vendor.o(($event) => swichMenu(item.id))
          };
        }),
        f: catePid.value != 0 && switchImg.value
      }, catePid.value != 0 && switchImg.value ? {
        g: _ctx.imageUrl + switchImg.value
      } : {}, {
        h: catePid.value != 0
      }, catePid.value != 0 ? common_vendor.e({
        i: common_vendor.n(cateId.value == 0 ? "active" : ""),
        j: common_vendor.f(shopCates.value, (item, index, i0) => {
          return {
            a: common_vendor.t(item.name),
            b: common_vendor.n(cateId.value == item.id ? "active" : ""),
            c: item.id
          };
        }),
        k: common_vendor.n(classificationFlag.value ? "classificationEvery" : "classificationAllEvery"),
        l: common_vendor.o(($event) => classification($event)),
        m: !classificationFlag.value
      }, !classificationFlag.value ? {} : {}, {
        n: common_vendor.o(open),
        o: common_vendor.p({
          type: openFlag.value ? "bottom" : "top",
          size: "22"
        })
      }) : {}, {
        p: common_vendor.n(sort.value == 1 ? "active" : ""),
        q: common_vendor.n(sort.value == 2 ? "active" : ""),
        r: common_vendor.n(sort.value == 4 ? "topActive" : "top"),
        s: common_vendor.n(sort.value == 5 ? "bottomActive" : "bottom"),
        t: common_vendor.n(sort.value == 4 || sort.value == 5 ? "active" : ""),
        v: common_vendor.o(($event) => goodssort($event)),
        w: common_vendor.unref(productData).length != 0
      }, common_vendor.unref(productData).length != 0 ? {
        x: common_vendor.f(common_vendor.unref(productData), (item, k0, i0) => {
          return {
            a: _ctx.imageUrl + item.masterImg,
            b: common_vendor.t(item.name),
            c: common_vendor.t(item.mallPrice),
            d: common_vendor.o(($event) => addCart(item.id, item.state)),
            e: common_vendor.o(($event) => jumpDetail($event, item.id))
          };
        }),
        y: common_assets._imports_0$8
      } : {}, {
        z: loading.value
      }, loading.value ? {
        A: common_vendor.t(loadingText.value)
      } : {}, {
        B: common_vendor.unref(productData).length == 0 && dataFlag.value
      }, common_vendor.unref(productData).length == 0 && dataFlag.value ? {
        C: common_assets._imports_0$2
      } : {}, {
        D: _ctx.imageUrl + masterImg.value,
        E: common_vendor.t(name.value),
        F: common_vendor.t(mallPrice.value),
        G: common_vendor.o(close),
        H: common_vendor.p({
          type: "closeempty",
          color: "#999",
          size: "18"
        }),
        I: norm1Value.value != "" || norm2Value.value != ""
      }, norm1Value.value != "" || norm2Value.value != "" ? {
        J: common_vendor.t(chooseValue.value == "" ? "" : chooseValue.value)
      } : {}, {
        K: norm2.value != 99
      }, norm2.value != 99 ? {
        L: common_vendor.t(norm2.value),
        M: common_vendor.o(norm2Change),
        N: common_vendor.o(($event) => norm2Value.value = $event),
        O: common_vendor.p({
          selectedColor: "rgba(28, 155, 100, 0.4)",
          selectedTextColor: "#1C9B64",
          mode: "tag",
          localdata: common_vendor.unref(norm2s),
          modelValue: norm2Value.value
        })
      } : {}, {
        P: norm1.value != 99
      }, norm1.value != 99 ? {
        Q: common_vendor.t(norm1.value),
        R: common_vendor.o(norm1Change),
        S: common_vendor.o(($event) => norm1Value.value = $event),
        T: common_vendor.p({
          selectedColor: "rgba(28, 155, 100, 0.4)",
          selectedTextColor: "#1C9B64",
          mode: "tag",
          localdata: common_vendor.unref(norm1s),
          modelValue: norm1Value.value
        })
      } : {}, {
        U: common_vendor.t(stock.value),
        V: common_vendor.o(($event) => numValue.value = $event),
        W: common_vendor.p({
          max: max.value,
          min: min.value,
          modelValue: numValue.value
        }),
        X: common_vendor.t(stock.value == "暂无库存" ? stock.value : "确定"),
        Y: common_vendor.o(($event) => confirmAddCart()),
        Z: stock.value == "暂无库存",
        aa: common_vendor.sr(popup, "72a4f3e6-2", {
          "k": "popup"
        }),
        ab: common_vendor.p({
          type: "bottom"
        })
      });
    };
  }
};
_sfc_main.__runtimeHooks = 1;
wx.createPage(_sfc_main);
