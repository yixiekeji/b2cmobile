"use strict";
const common_vendor = require("../../common/vendor.js");
const store_login = require("../../store/login.js");
const api_login = require("../../api/login.js");
if (!Array) {
  const _component_HeadNav = common_vendor.resolveComponent("HeadNav");
  const _easycom_uni_easyinput2 = common_vendor.resolveComponent("uni-easyinput");
  const _easycom_uni_forms_item2 = common_vendor.resolveComponent("uni-forms-item");
  const _easycom_uni_forms2 = common_vendor.resolveComponent("uni-forms");
  const _easycom_uni_popup2 = common_vendor.resolveComponent("uni-popup");
  const _easycom_uni_popup_dialog2 = common_vendor.resolveComponent("uni-popup-dialog");
  (_component_HeadNav + _easycom_uni_easyinput2 + _easycom_uni_forms_item2 + _easycom_uni_forms2 + _easycom_uni_popup2 + _easycom_uni_popup_dialog2)();
}
const _easycom_uni_easyinput = () => "../../uni_modules/uni-easyinput/components/uni-easyinput/uni-easyinput.js";
const _easycom_uni_forms_item = () => "../../uni_modules/uni-forms/components/uni-forms-item/uni-forms-item.js";
const _easycom_uni_forms = () => "../../uni_modules/uni-forms/components/uni-forms/uni-forms.js";
const _easycom_uni_popup = () => "../../uni_modules/uni-popup/components/uni-popup/uni-popup.js";
const _easycom_uni_popup_dialog = () => "../../uni_modules/uni-popup/components/uni-popup-dialog/uni-popup-dialog.js";
if (!Math) {
  (_easycom_uni_easyinput + _easycom_uni_forms_item + _easycom_uni_forms + _easycom_uni_popup + _easycom_uni_popup_dialog)();
}
const _sfc_main = {
  __name: "loginInterface",
  setup(__props) {
    const form = common_vendor.ref();
    const flag = common_vendor.ref(false);
    const codeImg = common_vendor.ref("");
    let FormData = common_vendor.ref({
      phone: "",
      //手机号
      verifyCode: "",
      //验证码
      smsCode: "",
      //手机验证码
      verifyUUID: "",
      promoterPhone: ""
    });
    const alertDialog = common_vendor.ref();
    const resendFlag = common_vendor.ref(true);
    const timer = common_vendor.ref(null);
    const count = common_vendor.ref(60);
    const appPhone = common_vendor.ref("");
    let rules = common_vendor.ref({
      // 手机号
      phone: {
        rules: [
          {
            required: true,
            errorMessage: "请输入手机号"
          },
          {
            pattern: "^[1][3,4,5,6,7,8,9][0-9]{9}$",
            errorMessage: "手机号格式不正确"
          }
        ]
      },
      // 验证码
      verifyCode: {
        rules: [{
          required: true,
          errorMessage: "请输入验证码"
        }]
      },
      // 手机验证码
      smsCode: {
        rules: [{
          required: true,
          errorMessage: "请输入手机验证码"
        }]
      }
    });
    common_vendor.ref("");
    const shopLogo = common_vendor.ref("../../static/loginInterface/logo.png");
    function verifyCode() {
      api_login.verify().then((res) => {
        codeImg.value = "data:image/jpg;base64," + res.data.data.base64Image;
        FormData.value.verifyUUID = res.data.data.verifyUUID;
      });
    }
    function toggle() {
      verifyCode();
    }
    function resendCode() {
      console.log(1111);
      form.value.validateField(["phone", "verifyCode"]).then((res) => {
        getSendVerifySMS();
        console.log("表单数据信息：", res);
      }).catch((err) => {
        console.log("表单错误信息：", err);
      });
    }
    function getSendVerifySMS() {
      api_login.sendVerifySMS({
        phone: FormData.value.phone,
        verifycode: FormData.value.verifyCode,
        // verifycode:'1234'
        verifyUUID: FormData.value.verifyUUID
        // type: 'reg'
      }).then((res) => {
        console.log(res);
        if (res.data.success) {
          resendFlag.value = false;
          count.value = 60;
          getTimer();
          common_vendor.index.showToast({
            icon: "none",
            duration: 3e3,
            title: "验证码已发送"
          });
        } else {
          common_vendor.index.showToast({
            icon: "none",
            duration: 3e3,
            title: `${res.data.message}`
          });
        }
      });
    }
    function resendCodes() {
      if (count.value != -1) {
        return;
      }
      form.value.validateField(["phone", "verifyCode"]).then((res) => {
        getSendVerifySMS();
        console.log("表单数据信息：", res);
      }).catch((err) => {
        console.log("表单错误信息：", err);
      });
    }
    function getTimer() {
      timer.value = setInterval(() => {
        if (count.value == 0) {
          clearInterval(timer.value);
          count.value = -1;
          return;
        }
        count.value--;
      }, 1e3);
    }
    function changed(val) {
      flag.value = !flag.value;
    }
    function onPageJump(url) {
      common_vendor.index.navigateTo({
        url
      });
    }
    function openTost() {
      common_vendor.index.showToast({
        icon: "none",
        duration: 3e3,
        title: "请先阅读并同意《注册协议》和《隐私条款》"
      });
    }
    function submit() {
      form.value.validate().then((res) => {
        if (flag.value) {
          logins();
        } else {
          common_vendor.index.showToast({
            icon: "none",
            duration: 3e3,
            title: "请先阅读并同意《用户手册》和《隐私条款》"
          });
        }
        console.log("表单数据信息：", res);
      }).catch((err) => {
        console.log("表单错误信息：", err);
      });
    }
    function logins() {
      store_login.login().doLoginPhone(FormData.value).then(() => {
        common_vendor.index.showToast({
          icon: "none",
          duration: 3e3,
          title: "登录成功",
          success: () => {
            setTimeout(function() {
              common_vendor.index.switchTab({
                url: "/pages/homePage/homepage"
              });
            }, 1e3);
          }
        });
      });
    }
    const avatarUrl = common_vendor.ref("");
    common_vendor.ref("");
    common_vendor.ref("");
    common_vendor.ref("");
    const codes = common_vendor.ref("");
    function wxLogin(e) {
      console.log(e);
      var detail = e.detail;
      if (detail.errMsg == "getPhoneNumber:ok") {
        console.log("用户同意授权");
        var code = detail.code;
        codes.value = detail.code;
        console.log(code);
        common_vendor.wx$1.login({
          success: function(result) {
            if (result.code) {
              api_login.getOpenId({
                jsCode: result.code
              }).then((res) => {
                console.log(res);
                if (res.data.success) {
                  openId.value = res.data.data.unionid;
                  xcxphone.value = res.data.data.phone;
                  console.log(res.data.data.phone);
                  if (res.data.data.phone) {
                    api_login.dologinWx({
                      openId: openId.value,
                      phone: res.data.data.phone,
                      headImage: ""
                    }).then((result2) => {
                      common_vendor.index.setStorageSync("token", result2.data.data.token);
                      store_login.login().setToken(result2.data.data.token);
                      common_vendor.index.showToast({
                        icon: "none",
                        duration: 3e3,
                        title: "登录成功",
                        success: () => {
                          setTimeout(
                            function() {
                              common_vendor.index.switchTab({
                                url: "/pages/homePage/homepage"
                              });
                            },
                            1e3
                          );
                        }
                      });
                      console.log(result2);
                    });
                  } else {
                    alertDialog.value.open();
                  }
                }
              });
            } else {
              console.log("登录失败！" + result.errMsg);
            }
          }
        });
      }
    }
    function closeParameter() {
      alertDialog.value.close();
      getxcxphone(codes.value);
    }
    function chooseavatar(e) {
      console.log(e.detail.avatarUrl);
      common_vendor.index.uploadFile({
        url: `https://b2c.yixiekeji.cn/prod-api/api/uploadFileImage`,
        filePath: e.detail.avatarUrl,
        name: "imageFile",
        header: {},
        success: (res) => {
          avatarUrl.value = JSON.parse(res.data).data;
          alertDialog.value.close();
          getxcxphone(codes.value);
        },
        fail: (err) => {
          common_vendor.index.hideLoading();
          common_vendor.index.showToast({
            title: "上传附件失败，请稍候再试！",
            duration: 1e3,
            icon: "none"
          });
          return;
        }
      });
    }
    function close() {
      alertDialog.value.close();
    }
    const openId = common_vendor.ref("");
    const sessionKey = common_vendor.ref("");
    function getOpenid(val) {
      api_login.getOpenId({
        jsCode: val
      }).then((res) => {
        console.log(res);
        if (res.data.success) {
          openId.value = res.data.data.unionid;
          sessionKey.value = res.data.data.sessionkey;
          api_login.dologinWx({
            openId: openId.value,
            phone: xcxphone.value,
            headImage: avatarUrl.value
          }).then((result) => {
            common_vendor.index.setStorageSync("token", result.data.data.token);
            store_login.login().setToken(result.data.data.token);
            common_vendor.index.showToast({
              icon: "none",
              duration: 3e3,
              title: "登录成功",
              success: () => {
                setTimeout(function() {
                  common_vendor.index.switchTab({
                    url: "/pages/homePage/homepage"
                  });
                }, 1e3);
              }
            });
            console.log(result);
          });
        }
      });
    }
    const xcxphone = common_vendor.ref("");
    function getxcxphone(val) {
      api_login.getPhoneNumber({
        jsCode: val
      }).then((res) => {
        console.log(res);
        xcxphone.value = res.data.data;
        common_vendor.wx$1.login({
          success: function(result) {
            if (result.code) {
              var codes2 = result.code;
              getOpenid(codes2);
            } else {
              console.log("登录失败！" + result.errMsg);
            }
          }
        });
      });
    }
    const phoneDialog = common_vendor.ref();
    const unionids = common_vendor.ref("");
    const headImgUrl = common_vendor.ref("");
    function closePhone() {
      phoneDialog.value.close();
    }
    function confimPhone(value) {
      const tests = /^(?:(?:\+|00)86)?1[3-9]\d{9}$/;
      if (tests.test(value)) {
        console.log(value);
        loginApp(unionids.value, value, headImgUrl.value);
        phoneDialog.value.close();
      } else {
        common_vendor.index.showToast({
          icon: "none",
          duration: 3e3,
          title: "手机号码不正确"
        });
      }
    }
    function loginApp(unionid, phone, headImgUrl2) {
      api_login.dologinWx({
        openId: unionid,
        phone,
        headImage: headImgUrl2
      }).then((result) => {
        common_vendor.index.setStorageSync("token", result.data.data.token);
        store_login.login().setToken(result.data.data.token);
        common_vendor.index.showToast({
          icon: "none",
          duration: 3e3,
          title: "登录成功",
          success: () => {
            setTimeout(function() {
              common_vendor.index.switchTab({
                url: "/pages/homePage/homepage"
              });
            }, 1e3);
          }
        });
        console.log(result);
      });
    }
    common_vendor.onShow(() => {
      verifyCode();
    });
    common_vendor.onLoad(() => {
      verifyCode();
      common_vendor.index.setNavigationBarTitle({
        title: common_vendor.index.getStorageSync("shopName") ? common_vendor.index.getStorageSync("shopName") : "禾鲜谷"
      });
    });
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: common_vendor.p({
          ["nav-background-color"]: "#fff",
          backImageUrl: "2",
          textContent: "登录",
          fontSize: "15",
          fontWeight: "600"
        }),
        b: shopLogo.value,
        c: common_vendor.o(($event) => common_vendor.unref(FormData).phone = $event),
        d: common_vendor.p({
          type: "number",
          placeholder: "请输入手机号码",
          modelValue: common_vendor.unref(FormData).phone
        }),
        e: common_vendor.p({
          name: "phone"
        }),
        f: common_vendor.o(($event) => common_vendor.unref(FormData).verifyCode = $event),
        g: common_vendor.p({
          placeholder: "图像验证码",
          modelValue: common_vendor.unref(FormData).verifyCode
        }),
        h: codeImg.value,
        i: common_vendor.o(toggle),
        j: common_vendor.p({
          name: "verifyCode"
        }),
        k: common_vendor.o(($event) => common_vendor.unref(FormData).smsCode = $event),
        l: common_vendor.p({
          type: "number",
          placeholder: "测试环境默认:888888",
          modelValue: common_vendor.unref(FormData).smsCode
        }),
        m: resendFlag.value
      }, resendFlag.value ? {
        n: common_vendor.o(resendCode)
      } : common_vendor.e({
        o: !resendFlag.value && count.value != -1
      }, !resendFlag.value && count.value != -1 ? {
        p: common_vendor.t(count.value)
      } : {}, {
        q: common_vendor.o(resendCodes),
        r: count.value != -1 ? "#ccc" : "#1C9B64"
      }), {
        s: common_vendor.p({
          name: "smsCode"
        }),
        t: common_vendor.sr(form, "90cf8bb8-1", {
          "k": "form"
        }),
        v: common_vendor.p({
          rules: common_vendor.unref(rules),
          modelValue: common_vendor.unref(FormData)
        }),
        w: flag.value,
        x: common_vendor.o(changed),
        y: common_vendor.s({
          "background-color": flag.value ? "#1C9B64" : "#1C9B6488"
        }),
        z: common_vendor.o(($event) => submit()),
        A: common_vendor.o(($event) => onPageJump("/pages/loginInterface/registerPage")),
        B: common_vendor.o(($event) => onPageJump("/pages/loginInterface/resetPassword")),
        C: common_vendor.s({
          "background-color": flag.value ? "#1C9B64" : "#1C9B6488"
        }),
        D: common_vendor.o(($event) => onPageJump("/pages/loginInterface/passwordLogin")),
        E: !flag.value
      }, !flag.value ? {
        F: common_vendor.s({
          "background-color": flag.value ? "#1C9B64" : "#1C9B6488"
        }),
        G: common_vendor.o(openTost)
      } : {
        H: common_vendor.s({
          "background-color": flag.value ? "#1C9B64" : "#1C9B6488"
        }),
        I: common_vendor.o(wxLogin),
        J: !_ctx.logged
      }, {
        K: common_vendor.o(($event) => close()),
        L: common_vendor.o(($event) => closeParameter()),
        M: common_vendor.o(chooseavatar),
        N: !_ctx.logged,
        O: common_vendor.sr(alertDialog, "90cf8bb8-8", {
          "k": "alertDialog"
        }),
        P: common_vendor.p({
          ["mask-click"]: false
        }),
        Q: common_vendor.sr("phoneDialogs", "90cf8bb8-10,90cf8bb8-9"),
        R: common_vendor.o(confimPhone),
        S: common_vendor.o(closePhone),
        T: common_vendor.p({
          mode: "input",
          title: "绑定手机号",
          value: appPhone.value,
          placeholder: "请录入手机号",
          ["before-close"]: true
        }),
        U: common_vendor.sr(phoneDialog, "90cf8bb8-9", {
          "k": "phoneDialog"
        }),
        V: common_vendor.p({
          type: "dialog"
        })
      });
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-90cf8bb8"]]);
wx.createPage(MiniProgramPage);
