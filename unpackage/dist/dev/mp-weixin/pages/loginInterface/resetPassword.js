"use strict";
const common_vendor = require("../../common/vendor.js");
const api_login = require("../../api/login.js");
if (!Array) {
  const _component_HeadNav = common_vendor.resolveComponent("HeadNav");
  const _easycom_uni_easyinput2 = common_vendor.resolveComponent("uni-easyinput");
  const _easycom_uni_forms_item2 = common_vendor.resolveComponent("uni-forms-item");
  const _easycom_uni_forms2 = common_vendor.resolveComponent("uni-forms");
  (_component_HeadNav + _easycom_uni_easyinput2 + _easycom_uni_forms_item2 + _easycom_uni_forms2)();
}
const _easycom_uni_easyinput = () => "../../uni_modules/uni-easyinput/components/uni-easyinput/uni-easyinput.js";
const _easycom_uni_forms_item = () => "../../uni_modules/uni-forms/components/uni-forms-item/uni-forms-item.js";
const _easycom_uni_forms = () => "../../uni_modules/uni-forms/components/uni-forms/uni-forms.js";
if (!Math) {
  (_easycom_uni_easyinput + _easycom_uni_forms_item + _easycom_uni_forms)();
}
const _sfc_main = {
  __name: "resetPassword",
  setup(__props) {
    const form = common_vendor.ref();
    const codeImg = common_vendor.ref("");
    let FormData = common_vendor.ref({
      smsCode: "",
      //手机验证码
      password: "",
      //密码
      repassword: "",
      //确认密码
      verifyCode: "",
      //验证码
      phone: "",
      //手机号
      verifyUUID: ""
    });
    const resendFlag = common_vendor.ref(true);
    const timer = common_vendor.ref(null);
    const count = common_vendor.ref(60);
    const disabledFlag = common_vendor.ref(false);
    const rules = common_vendor.ref({
      // 手机号
      phone: {
        rules: [
          {
            required: true,
            errorMessage: "请输入手机号"
          },
          {
            pattern: "^[1][3,4,5,6,7,8,9][0-9]{9}$",
            errorMessage: "手机号格式不正确"
          }
        ]
      },
      // 验证新密码
      password: {
        rules: [
          {
            required: true,
            errorMessage: "请输入密码"
          },
          {
            minLength: 6,
            errorMessage: "至少需要六个字符"
          },
          {
            maxLength: 20,
            errorMessage: "最多需要二十个字符"
          }
        ]
      },
      // 验证确认密码
      repassword: {
        rules: [
          {
            required: true,
            errorMessage: "请输入密码"
          },
          {
            validateFunction: (rule, value, data, callback) => {
              if (value != FormData.value.password) {
                callback("两次密码输入不一致");
              }
              return true;
            }
          }
        ]
      },
      // 验证验证码
      verifyCode: {
        rules: [{
          required: true,
          errorMessage: "请输入验证码"
        }]
      },
      // 验证手机验证码
      smsCode: {
        rules: [{
          required: true,
          errorMessage: "请输入手机验证码"
        }]
      }
    });
    common_vendor.ref({
      password: {
        rules: [{
          required: true,
          errorMessage: "请输入密码"
        }]
      },
      repassword: {
        rules: [
          {
            required: true,
            errorMessage: "请输入密码"
          },
          {
            validateFunction: (rule, value, data, callback) => {
              if (value != FormData.value.password) {
                callback("两次密码输入不一致");
              }
              return true;
            }
          }
        ]
      },
      verifyCode: {
        rules: [{
          required: true,
          errorMessage: "请输入验证码"
        }]
      }
    });
    const shopLogo = common_vendor.ref("../../static/loginInterface/logo.png");
    function verifyCode() {
      api_login.verify().then((res2) => {
        codeImg.value = "data:image/jpg;base64," + res2.data.data.base64Image;
        FormData.value.verifyUUID = res2.data.data.verifyUUID;
      });
    }
    function toggle() {
      verifyCode();
    }
    function resendCode() {
      form.value.validateField(["phone", "password", "repassword", "verifyCode"]).then((res2) => {
        getSendVerifySMS();
        console.log("表单数据信息：", res2);
      }).catch((err) => {
        console.log("表单错误信息：", err);
      });
    }
    function getSendVerifySMS() {
      api_login.sendVerifySMS({
        phone: FormData.value.phone,
        verifycode: FormData.value.verifyCode,
        verifyUUID: FormData.value.verifyUUID,
        // verifycode:'1234'
        type: ""
      }).then((res2) => {
        console.log(res2);
        if (res2.data.success) {
          resendFlag.value = false;
          count.value = 60;
          getTimer();
          common_vendor.index.showToast({
            icon: "none",
            duration: 3e3,
            title: "验证码已发送"
          });
        } else {
          common_vendor.index.showToast({
            icon: "none",
            duration: 3e3,
            title: `${res2.data.message}`
          });
        }
      });
    }
    function resendCodes() {
      if (count.value != -1) {
        return;
      }
      form.value.clearValidate();
      form.value.validateField(["phone", "password", "repassword", "verifyCode"]).then((res2) => {
        getSendVerifySMS();
        console.log("表单数据信息：", res2);
      }).catch((err) => {
        console.log("表单错误信息：", err);
      });
    }
    function getTimer() {
      timer.value = setInterval(() => {
        if (count.value == 0) {
          clearInterval(timer.value);
          count.value = -1;
          return;
        }
        count.value--;
      }, 1e3);
    }
    function submit() {
      form.value.validate().then((res2) => {
        console.log("表单数据信息：", res2);
        edit();
      }).catch((err) => {
        common_vendor.index.showToast({
          icon: "none",
          duration: 3e3,
          title: `${res.data.message}`
        });
        console.log("表单错误信息：", err);
      });
    }
    function edit() {
      api_login.resetPwd(FormData.value).then((res2) => {
        common_vendor.index.showToast({
          icon: "none",
          duration: 3e3,
          title: "修改成功"
        });
        common_vendor.index.navigateBack({
          delta: 1
        });
      }).catch((err) => {
        common_vendor.index.showToast({
          icon: "none",
          duration: 3e3,
          title: `${res.data.message}`
        });
        console.log("表单错误信息：", err);
      });
    }
    common_vendor.onLoad((options) => {
      if (options.phone) {
        FormData.value.phone = options.phone;
        disabledFlag.value = true;
      } else {
        FormData.value.phone = "";
        disabledFlag.value = false;
      }
      verifyCode();
    });
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: common_vendor.p({
          ["nav-background-color"]: "#fff",
          backImageUrl: "2",
          textContent: "忘记密码",
          fontSize: "15",
          fontWeight: "600"
        }),
        b: shopLogo.value,
        c: common_vendor.o(($event) => common_vendor.unref(FormData).phone = $event),
        d: common_vendor.p({
          disabled: disabledFlag.value,
          placeholder: "请输入手机号",
          modelValue: common_vendor.unref(FormData).phone
        }),
        e: common_vendor.p({
          name: "phone"
        }),
        f: common_vendor.o(($event) => common_vendor.unref(FormData).password = $event),
        g: common_vendor.p({
          type: "password",
          placeholder: "请输入新密码",
          modelValue: common_vendor.unref(FormData).password
        }),
        h: common_vendor.p({
          name: "password"
        }),
        i: common_vendor.o(($event) => common_vendor.unref(FormData).repassword = $event),
        j: common_vendor.p({
          type: "password",
          placeholder: "请确认密码",
          modelValue: common_vendor.unref(FormData).repassword
        }),
        k: common_vendor.p({
          name: "repassword"
        }),
        l: common_vendor.o(($event) => common_vendor.unref(FormData).verifyCode = $event),
        m: common_vendor.p({
          placeholder: "图像验证码",
          modelValue: common_vendor.unref(FormData).verifyCode
        }),
        n: common_vendor.p({
          name: "verifyCode"
        }),
        o: codeImg.value,
        p: common_vendor.o(toggle),
        q: common_vendor.o(($event) => common_vendor.unref(FormData).smsCode = $event),
        r: common_vendor.p({
          type: "number",
          placeholder: "测试环境默认:888888",
          modelValue: common_vendor.unref(FormData).smsCode
        }),
        s: resendFlag.value
      }, resendFlag.value ? {
        t: common_vendor.o(resendCode)
      } : common_vendor.e({
        v: !resendFlag.value && count.value != -1
      }, !resendFlag.value && count.value != -1 ? {
        w: common_vendor.t(count.value)
      } : {}, {
        x: common_vendor.o(resendCodes),
        y: count.value != -1 ? "#ccc" : "#1C9B64"
      }), {
        z: common_vendor.p({
          name: "smsCode"
        }),
        A: common_vendor.sr(form, "63c35614-1", {
          "k": "form"
        }),
        B: common_vendor.p({
          rules: rules.value,
          modelValue: common_vendor.unref(FormData)
        }),
        C: common_vendor.o(submit)
      });
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-63c35614"]]);
wx.createPage(MiniProgramPage);
