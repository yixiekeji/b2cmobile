"use strict";
const common_vendor = require("../../common/vendor.js");
const api_login = require("../../api/login.js");
const store_login = require("../../store/login.js");
if (!Array) {
  const _component_HeadNav = common_vendor.resolveComponent("HeadNav");
  const _easycom_uni_easyinput2 = common_vendor.resolveComponent("uni-easyinput");
  const _easycom_uni_forms_item2 = common_vendor.resolveComponent("uni-forms-item");
  const _easycom_uni_forms2 = common_vendor.resolveComponent("uni-forms");
  const _easycom_uni_popup2 = common_vendor.resolveComponent("uni-popup");
  const _easycom_uni_popup_dialog2 = common_vendor.resolveComponent("uni-popup-dialog");
  (_component_HeadNav + _easycom_uni_easyinput2 + _easycom_uni_forms_item2 + _easycom_uni_forms2 + _easycom_uni_popup2 + _easycom_uni_popup_dialog2)();
}
const _easycom_uni_easyinput = () => "../../uni_modules/uni-easyinput/components/uni-easyinput/uni-easyinput.js";
const _easycom_uni_forms_item = () => "../../uni_modules/uni-forms/components/uni-forms-item/uni-forms-item.js";
const _easycom_uni_forms = () => "../../uni_modules/uni-forms/components/uni-forms/uni-forms.js";
const _easycom_uni_popup = () => "../../uni_modules/uni-popup/components/uni-popup/uni-popup.js";
const _easycom_uni_popup_dialog = () => "../../uni_modules/uni-popup/components/uni-popup-dialog/uni-popup-dialog.js";
if (!Math) {
  (_easycom_uni_easyinput + _easycom_uni_forms_item + _easycom_uni_forms + _easycom_uni_popup + _easycom_uni_popup_dialog)();
}
const _sfc_main = {
  __name: "passwordLogin",
  setup(__props) {
    common_vendor.ref("http =/ref(/m.hxg777.com/");
    common_vendor.ref("");
    const iptType = common_vendor.ref("password");
    common_vendor.ref("../../static/loginInterface/eyes.png");
    const flag = common_vendor.ref(false);
    const alertDialog = common_vendor.ref();
    let FormData = common_vendor.ref({
      password: "",
      //密码
      phone: ""
      //手机号
    });
    const appPhone = common_vendor.ref("");
    let rules = common_vendor.ref({
      // 验证手机号
      phone: {
        rules: [
          {
            required: true,
            errorMessage: "请输入手机号"
          },
          {
            pattern: "^[1][3,4,5,6,7,8,9][0-9]{9}$",
            errorMessage: "手机号格式不正确"
          }
        ]
      },
      // 验证密码
      password: {
        rules: [
          {
            required: true,
            errorMessage: "请输入密码"
          },
          {
            minLength: 6,
            errorMessage: "至少需要六个字符"
          },
          {
            maxLength: 20,
            errorMessage: "最多需要二十个字符"
          }
        ]
      }
    });
    common_vendor.ref("");
    const shopLogo = common_vendor.ref("../../static/loginInterface/logo.png");
    const form = common_vendor.ref();
    const avatarUrl = common_vendor.ref("");
    const nickName = common_vendor.ref("");
    const encryptedData = common_vendor.ref("");
    const iv = common_vendor.ref("");
    const codes = common_vendor.ref("");
    function wxLogin(e) {
      console.log(e);
      var detail = e.detail;
      if (detail.errMsg == "getPhoneNumber:ok") {
        console.log("用户同意授权");
        detail.code;
        codes.value = detail.code;
        common_vendor.wx$1.getUserInfo({
          success: function(res) {
            console.log(res);
            avatarUrl.value = res.userInfo.avatarUrl;
            nickName.value = res.userInfo.nickName;
            encryptedData.value = res.encryptedData;
            iv.value = res.iv;
            common_vendor.wx$1.login({
              success: function(result) {
                if (result.code) {
                  api_login.getOpenId({
                    jsCode: result.code
                  }).then((res2) => {
                    console.log(res2);
                    if (res2.data.success) {
                      openId.value = res2.data.data.unionid;
                      xcxphone.value = res2.data.data.phone;
                      console.log(res2.data.data.phone);
                      if (res2.data.data.phone) {
                        api_login.dologinWx({
                          openId: openId.value,
                          phone: res2.data.data.phone,
                          headImage: ""
                        }).then((result2) => {
                          common_vendor.index.setStorageSync("token", result2.data.data.token);
                          store_login.login().setToken(result2.data.data.token);
                          common_vendor.index.showToast({
                            icon: "none",
                            duration: 3e3,
                            title: "登录成功",
                            success: () => {
                              setTimeout(
                                function() {
                                  common_vendor.index.switchTab({
                                    url: "/pages/homePage/homepage"
                                  });
                                },
                                1e3
                              );
                            }
                          });
                          console.log(result2);
                        });
                      } else {
                        alertDialog.value.open();
                      }
                    }
                  });
                } else {
                  console.log("登录失败！" + result.errMsg);
                }
              }
            });
          },
          fail: function(res) {
            console.log("用户授权失败！" + res.errMsg);
          }
        });
      }
    }
    function openTost() {
      common_vendor.index.showToast({
        icon: "none",
        duration: 3e3,
        title: "请先阅读并同意《注册协议》和《隐私条款》"
      });
    }
    function closeParameter() {
      alertDialog.value.close();
      getxcxphone(codes.value);
    }
    function chooseavatar(e) {
      console.log(e);
      common_vendor.index.uploadFile({
        url: `https://b2c.yixiekeji.cn/prod-api/api/uploadFileImage`,
        filePath: e.detail.avatarUrl,
        name: "imageFile",
        header: {},
        success: (res) => {
          avatarUrl.value = JSON.parse(res.data).data;
          alertDialog.value.close();
          getxcxphone(codes.value);
        },
        fail: (err) => {
          common_vendor.index.hideLoading();
          common_vendor.index.showToast({
            title: "上传附件失败，请稍候再试！",
            duration: 1e3,
            icon: "none"
          });
          return;
        }
      });
    }
    function close() {
      alertDialog.value.close();
    }
    const openId = common_vendor.ref("");
    const sessionKey = common_vendor.ref("");
    function getOpenid(val) {
      api_login.getOpenId({
        jsCode: val
      }).then((res) => {
        console.log(res);
        if (res.data.success) {
          openId.value = res.data.data.unionid;
          sessionKey.value = res.data.data.sessionkey;
          api_login.dologinWx({
            openId: openId.value,
            phone: xcxphone.value,
            headImage: avatarUrl.value
          }).then((result) => {
            common_vendor.index.setStorageSync("token", result.data.data.token);
            store_login.login().setToken(result.data.data.token);
            common_vendor.index.showToast({
              icon: "none",
              duration: 3e3,
              title: "登录成功",
              success: () => {
                setTimeout(function() {
                  common_vendor.index.switchTab({
                    url: "/pages/homePage/homepage"
                  });
                }, 1e3);
              }
            });
            console.log(result);
          });
        }
      });
    }
    const xcxphone = common_vendor.ref("");
    function getxcxphone(val) {
      api_login.getPhoneNumber({
        jsCode: val
      }).then((res) => {
        console.log(res);
        xcxphone.value = res.data.data;
        common_vendor.wx$1.login({
          success: function(result) {
            if (result.code) {
              var codes2 = result.code;
              getOpenid(codes2);
            } else {
              console.log("登录失败！" + result.errMsg);
            }
          }
        });
      });
    }
    const phoneDialog = common_vendor.ref();
    const unionids = common_vendor.ref("");
    const headImgUrl = common_vendor.ref("");
    function closePhone() {
      phoneDialog.value.close();
    }
    function confimPhone(value) {
      const tests = /^(?:(?:\+|00)86)?1[3-9]\d{9}$/;
      if (tests.test(value)) {
        console.log(value);
        loginApp(unionids.value, value, headImgUrl.value);
        phoneDialog.value.close();
      } else {
        common_vendor.index.showToast({
          icon: "none",
          duration: 3e3,
          title: "手机号码不正确"
        });
      }
    }
    function loginApp(unionid, phone, headImgUrl2) {
      api_login.dologinWx({
        openId: unionid,
        phone,
        headImage: headImgUrl2
      }).then((result) => {
        common_vendor.index.setStorageSync("token", result.data.data.token);
        store_login.login().setToken(result.data.data.token);
        common_vendor.index.showToast({
          icon: "none",
          duration: 3e3,
          title: "登录成功",
          success: () => {
            setTimeout(function() {
              common_vendor.index.switchTab({
                url: "/pages/homePage/homepage"
              });
            }, 1e3);
          }
        });
        console.log(result);
      });
    }
    function changed(val) {
      flag.value = !flag.value;
    }
    function submit() {
      form.value.validate().then((res) => {
        if (flag.value) {
          store_login.login().doLogin(FormData.value).then(() => {
            common_vendor.index.showToast({
              icon: "none",
              duration: 3e3,
              title: "登录成功",
              success: () => {
                setTimeout(function() {
                  common_vendor.index.switchTab({
                    url: "/pages/homePage/homepage"
                  });
                }, 1e3);
              }
            });
          });
        } else {
          common_vendor.index.showToast({
            icon: "none",
            duration: 3e3,
            title: "请先阅读并同意《注册协议》和《隐私条款》"
          });
        }
      }).catch((err) => {
        console.log("表单错误信息：", err);
      });
    }
    function onPageJump(url) {
      common_vendor.index.navigateTo({
        url
      });
    }
    common_vendor.onLoad((options) => {
      shopLogo.value = "../../static/loginInterface/logo.png";
    });
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: common_vendor.p({
          ["nav-background-color"]: "#fff",
          backImageUrl: "2",
          textContent: "登录",
          fontSize: "15",
          fontWeight: "600"
        }),
        b: shopLogo.value,
        c: common_vendor.o(($event) => common_vendor.unref(FormData).phone = $event),
        d: common_vendor.p({
          type: "number",
          placeholder: "请输入手机号码",
          modelValue: common_vendor.unref(FormData).phone
        }),
        e: common_vendor.p({
          name: "phone"
        }),
        f: common_vendor.o(($event) => common_vendor.unref(FormData).password = $event),
        g: common_vendor.p({
          type: iptType.value,
          placeholder: "请输入密码",
          modelValue: common_vendor.unref(FormData).password
        }),
        h: common_vendor.p({
          name: "password"
        }),
        i: common_vendor.sr(form, "abdd990d-1", {
          "k": "form"
        }),
        j: common_vendor.p({
          rules: common_vendor.unref(rules),
          modelValue: common_vendor.unref(FormData)
        }),
        k: flag.value,
        l: common_vendor.o(changed),
        m: common_vendor.s({
          "background-color": flag.value ? "#1C9B64" : "#1C9B6488"
        }),
        n: common_vendor.o(($event) => submit()),
        o: common_vendor.o(($event) => onPageJump("/pages/loginInterface/registerPage")),
        p: common_vendor.o(($event) => onPageJump("/pages/loginInterface/resetPassword")),
        q: common_vendor.s({
          "background-color": flag.value ? "#1C9B64" : "#1C9B6488"
        }),
        r: common_vendor.o(($event) => onPageJump("/pages/loginInterface/loginInterface")),
        s: !flag.value
      }, !flag.value ? {
        t: common_vendor.s({
          "background-color": flag.value ? "#1C9B64" : "#1C9B6488"
        }),
        v: common_vendor.o(openTost)
      } : {
        w: common_vendor.s({
          "background-color": flag.value ? "#1C9B64" : "#1C9B6488"
        }),
        x: common_vendor.o(wxLogin),
        y: !_ctx.logged
      }, {
        z: common_vendor.o(($event) => close()),
        A: common_vendor.o(($event) => closeParameter()),
        B: common_vendor.o(chooseavatar),
        C: !_ctx.logged,
        D: common_vendor.sr(alertDialog, "abdd990d-6", {
          "k": "alertDialog"
        }),
        E: common_vendor.p({
          ["mask-click"]: false
        }),
        F: common_vendor.sr("phoneDialogs", "abdd990d-8,abdd990d-7"),
        G: common_vendor.o(confimPhone),
        H: common_vendor.o(closePhone),
        I: common_vendor.p({
          mode: "input",
          title: "绑定手机号",
          value: appPhone.value,
          placeholder: "请录入手机号",
          ["before-close"]: true
        }),
        J: common_vendor.sr(phoneDialog, "abdd990d-7", {
          "k": "phoneDialog"
        }),
        K: common_vendor.p({
          type: "dialog"
        })
      });
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-abdd990d"]]);
wx.createPage(MiniProgramPage);
