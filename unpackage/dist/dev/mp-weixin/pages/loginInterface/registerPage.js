"use strict";
const common_vendor = require("../../common/vendor.js");
const api_login = require("../../api/login.js");
if (!Array) {
  const _component_HeadNav = common_vendor.resolveComponent("HeadNav");
  const _easycom_uni_easyinput2 = common_vendor.resolveComponent("uni-easyinput");
  const _easycom_uni_forms_item2 = common_vendor.resolveComponent("uni-forms-item");
  const _easycom_uni_forms2 = common_vendor.resolveComponent("uni-forms");
  (_component_HeadNav + _easycom_uni_easyinput2 + _easycom_uni_forms_item2 + _easycom_uni_forms2)();
}
const _easycom_uni_easyinput = () => "../../uni_modules/uni-easyinput/components/uni-easyinput/uni-easyinput.js";
const _easycom_uni_forms_item = () => "../../uni_modules/uni-forms/components/uni-forms-item/uni-forms-item.js";
const _easycom_uni_forms = () => "../../uni_modules/uni-forms/components/uni-forms/uni-forms.js";
if (!Math) {
  (_easycom_uni_easyinput + _easycom_uni_forms_item + _easycom_uni_forms)();
}
const _sfc_main = {
  __name: "registerPage",
  setup(__props) {
    let codeImg = common_vendor.ref("");
    const flag = common_vendor.ref(false);
    let FormData = common_vendor.ref({
      password: "",
      repassword: "",
      phone: "",
      verifyCode: "",
      verifyUUID: "",
      promoterPhone: ""
    });
    const form = common_vendor.ref();
    const resendFlag = common_vendor.ref(true);
    const timer = common_vendor.ref(null);
    const count = common_vendor.ref(60);
    let rules = common_vendor.ref({
      // 手机号验证
      phone: {
        rules: [
          {
            required: true,
            errorMessage: "请输入手机号"
          },
          {
            pattern: "^[1][3,4,5,6,7,8,9][0-9]{9}$",
            errorMessage: "手机号格式不正确"
          }
        ]
      },
      // 密码验证
      password: {
        rules: [
          {
            required: true,
            errorMessage: "请输入密码"
          },
          {
            minLength: 6,
            errorMessage: "至少需要六个字符"
          },
          {
            maxLength: 20,
            errorMessage: "最多需要二十个字符"
          }
        ]
      },
      // 确认密码
      repassword: {
        rules: [
          {
            required: true,
            errorMessage: "请输入密码"
          },
          {
            validateFunction: (rule, value, data, callback) => {
              if (FormData.value.repassword != FormData.value.password) {
                callback("两次密码输入不一致");
              }
              return true;
            }
          }
        ]
      },
      // 验证码验证
      verifyCode: {
        rules: [{
          required: true,
          errorMessage: "请输入验证码"
        }]
      },
      // 手机验证码
      smsCode: {
        rules: [{
          required: true,
          errorMessage: "请输入手机验证码"
        }]
      }
    });
    const shopLogo = common_vendor.ref("../../static/loginInterface/logo.png");
    function verifyCode() {
      api_login.verify().then((res) => {
        codeImg.value = "data:image/jpg;base64," + res.data.data.base64Image;
        FormData.value.verifyUUID = res.data.data.verifyUUID;
      });
    }
    function resendCode() {
      form.value.clearValidate();
      form.value.validateField(["phone", "password", "repassword", "verifyCode"]).then((res) => {
        getSendVerifySMS();
        console.log("表单数据信息：", res);
      }).catch((err) => {
        console.log("表单错误信息：", err);
      });
    }
    function getSendVerifySMS() {
      api_login.sendVerifySMS({
        phone: FormData.value.phone,
        verifycode: FormData.value.verifyCode,
        // verifycode:'1234'
        verifyUUID: FormData.value.verifyUUID,
        type: "reg"
      }).then((res) => {
        console.log(res);
        if (res.data.success) {
          common_vendor.index.showToast({
            icon: "none",
            duration: 3e3,
            title: "验证码已发送"
          });
          resendFlag.value = false;
          getTimer();
          count.value = 60;
        } else {
          common_vendor.index.showToast({
            icon: "none",
            duration: 3e3,
            title: `${res.data.message}`
          });
        }
      });
    }
    function resendCodes() {
      if (count.value != -1) {
        return;
      }
      form.value.clearValidate();
      form.value.validateField(["phone", "password", "repassword", "verifyCode"]).then((res) => {
        getSendVerifySMS();
        console.log("表单数据信息：", res);
      }).catch((err) => {
        console.log("表单错误信息：", err);
      });
    }
    function getTimer() {
      timer.value = setInterval(() => {
        if (count.value == 0) {
          clearInterval(timer.value);
          count.value = -1;
          return;
        }
        count.value--;
      }, 1e3);
    }
    function toggle() {
      verifyCode();
    }
    function changed(val) {
      flag.value = !flag.value;
    }
    function submit() {
      form.value.validate().then((res) => {
        if (flag.value) {
          api_login.register(FormData.value).then((res2) => {
            common_vendor.index.showToast({
              icon: "none",
              duration: 3e3,
              title: "注册成功",
              success: () => {
                setTimeout(function() {
                  common_vendor.index.navigateTo({
                    url: "/pages/loginInterface/passwordLogin"
                  });
                }, 1e3);
              }
            });
          });
        } else {
          common_vendor.index.showToast({
            icon: "none",
            duration: 3e3,
            title: "请先阅读并同意《注册协议》和《隐私条款》"
          });
        }
      }).catch((err) => {
        console.log("表单错误信息：", err);
      });
    }
    common_vendor.onLoad(() => {
      shopLogo.value = "../../static/loginInterface/logo.png";
      verifyCode();
      common_vendor.index.setNavigationBarTitle({
        title: common_vendor.index.getStorageSync("shopName") ? common_vendor.index.getStorageSync("shopName") : "易写科技注册"
      });
    });
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: common_vendor.p({
          ["nav-background-color"]: "#fff",
          backImageUrl: "2",
          textContent: "注册",
          fontSize: "15",
          fontWeight: "600"
        }),
        b: shopLogo.value,
        c: common_vendor.o(($event) => common_vendor.unref(FormData).phone = $event),
        d: common_vendor.p({
          type: "number",
          placeholder: "请输入手机号码",
          modelValue: common_vendor.unref(FormData).phone
        }),
        e: common_vendor.p({
          name: "phone"
        }),
        f: common_vendor.o(($event) => common_vendor.unref(FormData).password = $event),
        g: common_vendor.p({
          type: "password",
          placeholder: "请输入密码",
          modelValue: common_vendor.unref(FormData).password
        }),
        h: common_vendor.p({
          name: "password"
        }),
        i: common_vendor.o(($event) => common_vendor.unref(FormData).repassword = $event),
        j: common_vendor.p({
          type: "password",
          placeholder: "请确认密码",
          modelValue: common_vendor.unref(FormData).repassword
        }),
        k: common_vendor.p({
          name: "repassword"
        }),
        l: common_vendor.o(($event) => common_vendor.unref(FormData).verifyCode = $event),
        m: common_vendor.p({
          placeholder: "图像验证码",
          modelValue: common_vendor.unref(FormData).verifyCode
        }),
        n: common_vendor.p({
          name: "verifyCode"
        }),
        o: common_vendor.unref(codeImg),
        p: common_vendor.o(toggle),
        q: common_vendor.o(($event) => common_vendor.unref(FormData).smsCode = $event),
        r: common_vendor.p({
          placeholder: "测试环境默认:888888",
          modelValue: common_vendor.unref(FormData).smsCode
        }),
        s: resendFlag.value
      }, resendFlag.value ? {
        t: common_vendor.o(resendCode)
      } : common_vendor.e({
        v: !resendFlag.value && count.value != -1
      }, !resendFlag.value && count.value != -1 ? {
        w: common_vendor.t(count.value)
      } : {}, {
        x: common_vendor.o(resendCodes),
        y: count.value != -1 ? "#ccc" : "#1C9B64"
      }), {
        z: common_vendor.p({
          name: "smsCode"
        }),
        A: common_vendor.sr(form, "80c67188-1", {
          "k": "form"
        }),
        B: common_vendor.p({
          rules: common_vendor.unref(rules),
          modelValue: common_vendor.unref(FormData)
        }),
        C: flag.value,
        D: common_vendor.o(changed),
        E: common_vendor.o(($event) => submit()),
        F: common_vendor.s({
          "background-color": flag.value ? "#1C9B64" : "#1C9B6488"
        })
      });
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-80c67188"]]);
wx.createPage(MiniProgramPage);
