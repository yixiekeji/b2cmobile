"use strict";
const common_vendor = require("../../../common/vendor.js");
const api_order = require("../../../api/order.js");
if (!Array) {
  const _component_HeadNav = common_vendor.resolveComponent("HeadNav");
  const _easycom_uni_forms_item2 = common_vendor.resolveComponent("uni-forms-item");
  const _easycom_uni_easyinput2 = common_vendor.resolveComponent("uni-easyinput");
  const _easycom_uni_forms2 = common_vendor.resolveComponent("uni-forms");
  (_component_HeadNav + _easycom_uni_forms_item2 + _easycom_uni_easyinput2 + _easycom_uni_forms2)();
}
const _easycom_uni_forms_item = () => "../../../uni_modules/uni-forms/components/uni-forms-item/uni-forms-item.js";
const _easycom_uni_easyinput = () => "../../../uni_modules/uni-easyinput/components/uni-easyinput/uni-easyinput.js";
const _easycom_uni_forms = () => "../../../uni_modules/uni-forms/components/uni-forms/uni-forms.js";
if (!Math) {
  (_easycom_uni_forms_item + _easycom_uni_easyinput + _easycom_uni_forms)();
}
const _sfc_main = {
  __name: "commentList",
  setup(__props) {
    const flag = common_vendor.ref("");
    const form = common_vendor.ref();
    let formData = common_vendor.ref({
      number: "",
      question: "",
      mode: "1"
    });
    const maxNum = common_vendor.ref(1);
    const id = common_vendor.ref("");
    let rules = common_vendor.ref({
      number: {
        rules: [
          {
            required: true,
            errorMessage: "数量必填"
          },
          {
            validateFunction: function(rule, value, data, callback) {
              console.log(data);
              if (Number(value) > maxNum.value) {
                callback("数量超出");
              }
              return true;
            }
          }
        ]
      },
      question: {
        rules: [
          {
            required: true,
            errorMessage: "问题描述必填"
          },
          {
            minLength: 10,
            errorMessage: "至少需要10字符"
          },
          {
            maxLength: 100,
            errorMessage: "问题描述字符长度超出"
          }
        ]
      }
    });
    const styles = common_vendor.ref({
      "borderColor": "#fff",
      "backgroundColor": "#fff",
      "height": "96rpx",
      "borderRadius": "14rpx"
    });
    const ordersProductList = common_vendor.ref([]);
    function getList() {
      api_order.backapply({
        ordersId: id.value
      }).then((res) => {
        console.log(res);
        ordersProductList.value = res.data.data.ordersProductList;
      });
    }
    const indexid = common_vendor.ref("");
    function pingjia(val, index) {
      common_vendor.index.navigateTo({
        url: "/pages/memberCenter/commentPage/addComment?ordersId=" + id.value + "&ordersProductId=" + val
      });
    }
    function chooseMode(val) {
      formData.value.mode = val;
    }
    function commit(val, num) {
      maxNum.value = num;
      console.log(form.value);
      form.value[indexid.value].validate().then((res) => {
        if (formData.value.mode == 2) {
          api_order.doproductback({
            ordersId: id.value,
            ordersProductId: val,
            number: formData.value.number,
            question: formData.value.question
          }).then((res2) => {
            if (res2.data.success) {
              common_vendor.index.showToast({
                title: "操作成功",
                icon: "none"
              });
              flag.value = "";
            }
          });
        } else {
          api_order.doproductexchange({
            ordersId: id.value,
            ordersProductId: val,
            number: formData.value.number,
            question: formData.value.question
          }).then((res2) => {
            if (res2.data.success) {
              common_vendor.index.showToast({
                title: "操作成功",
                icon: "none"
              });
              flag.value = "";
            }
          });
        }
      }).catch((err) => {
      });
    }
    common_vendor.onLoad((options) => {
      id.value = options.id;
      getList();
    });
    return (_ctx, _cache) => {
      return {
        a: common_vendor.p({
          ["nav-background-color"]: "#f7f7f5",
          backImageUrl: "1",
          textContent: "商品评价",
          fontSize: "15",
          fontWeight: "600"
        }),
        b: common_vendor.f(ordersProductList.value, (item, index, i0) => {
          return {
            a: _ctx.imageUrl + item.productImg,
            b: common_vendor.t(item.productName),
            c: common_vendor.t(item.specInfo),
            d: common_vendor.n(item.specInfo ? "size" : "font22"),
            e: common_vendor.t(item.createTime),
            f: common_vendor.o(($event) => pingjia(item.id)),
            g: "0a85c340-2-" + i0 + "," + ("0a85c340-1-" + i0),
            h: "0a85c340-4-" + i0 + "," + ("0a85c340-3-" + i0),
            i: "0a85c340-3-" + i0 + "," + ("0a85c340-1-" + i0),
            j: common_vendor.p({
              label: "数量",
              name: "number",
              msgs: "可以退货数量为" + item.number,
              required: true
            }),
            k: "0a85c340-6-" + i0 + "," + ("0a85c340-5-" + i0),
            l: "0a85c340-5-" + i0 + "," + ("0a85c340-1-" + i0),
            m: common_vendor.sr(form, "0a85c340-1-" + i0, {
              "k": "form",
              "f": 1
            }),
            n: "0a85c340-1-" + i0,
            o: common_vendor.o(($event) => commit(item.id, item.number)),
            p: common_vendor.n(item.id == flag.value ? "popAct" : "pop")
          };
        }),
        c: common_vendor.o(($event) => chooseMode(1)),
        d: common_vendor.n(common_vendor.unref(formData).mode == 1 ? "active" : ""),
        e: common_vendor.o(($event) => chooseMode(2)),
        f: common_vendor.n(common_vendor.unref(formData).mode == 2 ? "active" : ""),
        g: common_vendor.p({
          label: "服务类型",
          name: "mode",
          required: true
        }),
        h: common_vendor.o(($event) => common_vendor.unref(formData).number = $event),
        i: common_vendor.p({
          type: "number",
          placeholder: "请输入数量",
          styles: styles.value,
          modelValue: common_vendor.unref(formData).number
        }),
        j: common_vendor.o(($event) => common_vendor.unref(formData).question = $event),
        k: common_vendor.p({
          type: "text",
          placeholder: "请输入问题描述",
          styles: styles.value,
          modelValue: common_vendor.unref(formData).question
        }),
        l: common_vendor.p({
          label: "问题描述",
          name: "question",
          msgs: "10-100字",
          required: true
        }),
        m: common_vendor.p({
          rules: common_vendor.unref(rules),
          modelValue: common_vendor.unref(formData),
          ["label-width"]: "80px"
        })
      };
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-0a85c340"]]);
wx.createPage(MiniProgramPage);
