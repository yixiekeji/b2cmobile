"use strict";
const common_vendor = require("../../../common/vendor.js");
const common_assets = require("../../../common/assets.js");
require("../../../store/order.js");
const api_order = require("../../../api/order.js");
if (!Array) {
  const _component_HeadNav = common_vendor.resolveComponent("HeadNav");
  const _easycom_uni_popup_dialog2 = common_vendor.resolveComponent("uni-popup-dialog");
  const _easycom_uni_popup2 = common_vendor.resolveComponent("uni-popup");
  (_component_HeadNav + _easycom_uni_popup_dialog2 + _easycom_uni_popup2)();
}
const _easycom_uni_popup_dialog = () => "../../../uni_modules/uni-popup/components/uni-popup-dialog/uni-popup-dialog.js";
const _easycom_uni_popup = () => "../../../uni_modules/uni-popup/components/uni-popup/uni-popup.js";
if (!Math) {
  (_easycom_uni_popup_dialog + _easycom_uni_popup)();
}
const _sfc_main = {
  __name: "orderDetail",
  setup(__props) {
    let data = common_vendor.ref({});
    const id = common_vendor.ref("");
    common_vendor.ref("");
    const msgType = common_vendor.ref("");
    const itemid = common_vendor.ref("");
    function jumpDEtails(id2, type, iid) {
      if (type == 1) {
        common_vendor.index.navigateTo({
          url: "/pages/homePage/goodsDetail?id=" + id2
        });
      } else {
        common_vendor.index.navigateTo({
          url: "/pages/memberCenter/integralPage/integralDetails?id=" + iid
        });
      }
      console.log(id2);
    }
    function jumpComment(val) {
      common_vendor.index.navigateTo({
        url: "/pages/memberCenter/myOrder/commentList?id=" + val
      });
    }
    function jump(val) {
      common_vendor.index.navigateTo({
        url: "/pages/memberCenter/myOrder/returnsExchanges?id=" + val
      });
    }
    function copyText(val) {
      common_vendor.index.setClipboardData({
        data: val,
        success() {
          common_vendor.index.showToast({
            title: "复制成功",
            icon: "success"
          });
        }
      });
    }
    function getData(val) {
      api_order.memberOrdersGetOrders({
        id: val
      }).then((res) => {
        console.log(res);
        data.value = res.data.data;
      });
    }
    const alertDialog = common_vendor.ref();
    function cancelOrder() {
      alertDialog.value.open();
    }
    function dialogConfirm() {
      api_order.memberOrdersCancelOrder({
        id: id.value
      }).then((res) => {
        getData(id.value);
        if (res.data.success) {
          common_vendor.index.showToast({
            icon: "none",
            duration: 3e3,
            title: `操作成功`
          });
        }
      });
    }
    function dialogClose() {
      alertDialog.value.close();
    }
    const alertDialogCfm = common_vendor.ref();
    function confirmReceipt() {
      alertDialogCfm.value.open();
    }
    function dialogConfirmCfm() {
      if (itemid.value == "") {
        api_order.memberOrdersConfirmOrder({
          id: id.value
        }).then((res) => {
          getData(id.value);
          if (res.data.success) {
            common_vendor.index.showToast({
              icon: "none",
              duration: 3e3,
              title: `操作成功`
            });
          }
        });
      } else {
        api_order.memberOrdersConfirmOrder({
          id: itemid.value
        }).then((res) => {
          getData(id.value);
          itemid.value = "";
          if (res.data.success) {
            common_vendor.index.showToast({
              icon: "none",
              duration: 3e3,
              title: `操作成功`
            });
          }
        });
      }
    }
    function dialogCloseCfm() {
      alertDialogCfm.close();
      itemid.value = "";
    }
    function confirmOrder(data2) {
      common_vendor.index.navigateTo({
        url: "/pages/shoppingCart/paymentMethod?id=" + data2
      });
    }
    common_vendor.onLoad((options) => {
      id.value = options.id;
      getData(options.id);
    });
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: common_vendor.p({
          ["nav-background-color"]: "#f7f7f5",
          backImageUrl: "1",
          textContent: "订单详情",
          fontSize: "15",
          fontWeight: "600"
        }),
        b: common_vendor.unref(data).orders.ordersState == 1
      }, common_vendor.unref(data).orders.ordersState == 1 ? {} : common_vendor.unref(data).orders.ordersState == 2 ? {} : common_vendor.unref(data).orders.ordersState == 3 ? {} : common_vendor.unref(data).orders.ordersState == 4 ? {} : common_vendor.unref(data).orders.ordersState == 5 ? {} : {}, {
        c: common_vendor.unref(data).orders.ordersState == 2,
        d: common_vendor.unref(data).orders.ordersState == 3,
        e: common_vendor.unref(data).orders.ordersState == 4,
        f: common_vendor.unref(data).orders.ordersState == 5,
        g: common_assets._imports_0$4,
        h: common_vendor.t(common_vendor.unref(data).orders.name),
        i: common_vendor.t(common_vendor.unref(data).orders.mobile),
        j: common_vendor.t(common_vendor.unref(data).orders.addressAll),
        k: common_vendor.t(common_vendor.unref(data).orders.addressInfo),
        l: common_vendor.f(common_vendor.unref(data).ordersProductList, (item, index, i0) => {
          return {
            a: _ctx.imageUrl + item.productImg,
            b: common_vendor.t(item.productName),
            c: common_vendor.t(item.specInfo),
            d: common_vendor.n(item.specInfo ? "font2" : "font22"),
            e: common_vendor.t(item.moneyPrice),
            f: common_vendor.t(item.number),
            g: common_vendor.o(($event) => jumpDEtails(item.productId, common_vendor.unref(data).orders.ordersType, item.integralId))
          };
        }),
        m: common_vendor.unref(data).orders.ordersState == 1 || common_vendor.unref(data).orders.ordersState == 2
      }, common_vendor.unref(data).orders.ordersState == 1 || common_vendor.unref(data).orders.ordersState == 2 ? {
        n: common_vendor.o(($event) => cancelOrder(common_vendor.unref(data).orders.id, _ctx.index))
      } : {}, {
        o: common_vendor.unref(data).orders.ordersState == 1
      }, common_vendor.unref(data).orders.ordersState == 1 ? {
        p: common_vendor.o(($event) => confirmOrder(common_vendor.unref(data).orders.id, _ctx.index))
      } : {}, {
        q: common_vendor.unref(data).orders.ordersState == 3 && !common_vendor.unref(data).orders.dorpshoppingFlag
      }, common_vendor.unref(data).orders.ordersState == 3 && !common_vendor.unref(data).orders.dorpshoppingFlag ? {
        r: common_vendor.o(($event) => confirmReceipt(common_vendor.unref(data).orders.id, _ctx.index))
      } : {}, {
        s: common_vendor.unref(data).orders.ordersState == 4
      }, common_vendor.unref(data).orders.ordersState == 4 ? {
        t: common_vendor.o(($event) => jumpComment(common_vendor.unref(data).orders.id))
      } : {}, {
        v: common_vendor.unref(data).orders.ordersState == 4
      }, common_vendor.unref(data).orders.ordersState == 4 ? {
        w: common_vendor.o(($event) => jump(common_vendor.unref(data).orders.id))
      } : {}, {
        x: common_vendor.t(common_vendor.unref(data).orders.ordersSn),
        y: common_vendor.o(($event) => copyText(common_vendor.unref(data).orders.ordersSn)),
        z: common_vendor.t(common_vendor.unref(data).orders.ordersType == "2" ? "积分商城订单" : "普通订单"),
        A: common_vendor.t(common_vendor.unref(data).orders.createTime),
        B: common_vendor.t(common_vendor.unref(data).orders.moneyProduct),
        C: common_vendor.t(common_vendor.unref(data).orders.moneyLogistics),
        D: common_vendor.t(common_vendor.unref(data).orders.moneyCoupon),
        E: common_vendor.unref(data).orders.ordersType == "2"
      }, common_vendor.unref(data).orders.ordersType == "2" ? {
        F: common_vendor.t(common_vendor.unref(data).orders.integral)
      } : {}, {
        G: common_vendor.t(common_vendor.unref(data).orders.moneyOrder),
        H: common_vendor.t(common_vendor.unref(data).orders.remark == "" ? "-" : common_vendor.unref(data).orders.remark),
        I: common_vendor.unref(data).orders.invoiceState != 0
      }, common_vendor.unref(data).orders.invoiceState != 0 ? common_vendor.e({
        J: common_vendor.unref(data).orders.invoiceState && common_vendor.unref(data).orders.invoiceState != "undefined"
      }, common_vendor.unref(data).orders.invoiceState && common_vendor.unref(data).orders.invoiceState != "undefined" ? {
        K: common_vendor.t(common_vendor.unref(data).orders.invoiceState == 1 ? "普通发票" : common_vendor.unref(data).orders.invoiceState == 2 ? "个人" : common_vendor.unref(data).orders.invoiceState == 3 ? "增值税专用发票" : "")
      } : {}, {
        L: common_vendor.unref(data).orders.invoiceTitle && common_vendor.unref(data).orders.invoiceTitle != "undefined"
      }, common_vendor.unref(data).orders.invoiceTitle && common_vendor.unref(data).orders.invoiceTitle != "undefined" ? {
        M: common_vendor.t(common_vendor.unref(data).orders.invoiceTitle)
      } : {}, {
        N: common_vendor.unref(data).orders.invoiceNum && common_vendor.unref(data).orders.invoiceNum != "undefined"
      }, common_vendor.unref(data).orders.invoiceNum && common_vendor.unref(data).orders.invoiceNum != "undefined" ? {
        O: common_vendor.t(common_vendor.unref(data).orders.invoiceNum)
      } : {}, {
        P: common_vendor.unref(data).orders.invoiceAddress && common_vendor.unref(data).orders.invoiceAddress != "undefined"
      }, common_vendor.unref(data).orders.invoiceAddress && common_vendor.unref(data).orders.invoiceAddress != "undefined" ? {
        Q: common_vendor.t(common_vendor.unref(data).orders.invoiceAddress)
      } : {}, {
        R: common_vendor.unref(data).orders.invoicePhone && common_vendor.unref(data).orders.invoicePhone != "undefined"
      }, common_vendor.unref(data).orders.invoicePhone && common_vendor.unref(data).orders.invoicePhone != "undefined" ? {
        S: common_vendor.t(common_vendor.unref(data).orders.invoicePhone)
      } : {}, {
        T: common_vendor.unref(data).orders.invoiceBank && common_vendor.unref(data).orders.invoiceBank != "undefined"
      }, common_vendor.unref(data).orders.invoiceBank && common_vendor.unref(data).orders.invoiceBank != "undefined" ? {
        U: common_vendor.t(common_vendor.unref(data).orders.invoiceBank)
      } : {}, {
        V: common_vendor.unref(data).orders.invoiceCode && common_vendor.unref(data).orders.invoiceCode != "undefined"
      }, common_vendor.unref(data).orders.invoiceCode && common_vendor.unref(data).orders.invoiceCode != "undefined" ? {
        W: common_vendor.t(common_vendor.unref(data).orders.invoiceCode)
      } : {}) : {}, {
        X: common_vendor.unref(data).orders.invoiceState == 0
      }, common_vendor.unref(data).orders.invoiceState == 0 ? {} : {}, {
        Y: common_vendor.unref(data).ordersLogList
      }, common_vendor.unref(data).ordersLogList ? {
        Z: common_vendor.f(common_vendor.unref(data).ordersLogList, (log, k0, i0) => {
          return {
            a: common_vendor.t(log.createTime),
            b: common_vendor.t(log.content)
          };
        })
      } : {}, {
        aa: common_vendor.o(dialogConfirm),
        ab: common_vendor.o(dialogClose),
        ac: common_vendor.p({
          type: msgType.value,
          cancelText: "取消",
          confirmText: "确认",
          content: "确认取消此条订单吗?"
        }),
        ad: common_vendor.sr(alertDialog, "b5b44d88-1", {
          "k": "alertDialog"
        }),
        ae: common_vendor.p({
          type: "dialog"
        }),
        af: common_vendor.o(dialogConfirmCfm),
        ag: common_vendor.o(dialogCloseCfm),
        ah: common_vendor.p({
          type: msgType.value,
          cancelText: "取消",
          confirmText: "确认",
          content: "确认收货吗?"
        }),
        ai: common_vendor.sr(alertDialogCfm, "b5b44d88-3", {
          "k": "alertDialogCfm"
        }),
        aj: common_vendor.p({
          type: "dialog"
        }),
        ak: common_vendor.unref(data).orders.ordersState == 5 ? "linear-gradient(to bottom, #DC3A2F 0%, rgba(255,0,0,0) 30%)" : "linear-gradient(to bottom, #1C9B64 0%, rgba(255,0,0,0) 30%)"
      });
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-b5b44d88"]]);
wx.createPage(MiniProgramPage);
