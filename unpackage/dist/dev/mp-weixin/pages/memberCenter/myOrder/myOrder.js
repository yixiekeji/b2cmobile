"use strict";
const common_vendor = require("../../../common/vendor.js");
const common_assets = require("../../../common/assets.js");
require("../../../store/order.js");
const api_order = require("../../../api/order.js");
if (!Array) {
  const _component_HeadNav = common_vendor.resolveComponent("HeadNav");
  const _easycom_uni_popup_dialog2 = common_vendor.resolveComponent("uni-popup-dialog");
  const _easycom_uni_popup2 = common_vendor.resolveComponent("uni-popup");
  (_component_HeadNav + _easycom_uni_popup_dialog2 + _easycom_uni_popup2)();
}
const _easycom_uni_popup_dialog = () => "../../../uni_modules/uni-popup/components/uni-popup-dialog/uni-popup-dialog.js";
const _easycom_uni_popup = () => "../../../uni_modules/uni-popup/components/uni-popup/uni-popup.js";
if (!Math) {
  (_easycom_uni_popup_dialog + _easycom_uni_popup)();
}
const _sfc_main = {
  __name: "myOrder",
  setup(__props) {
    let tabs = common_vendor.ref([
      {
        id: 0,
        name: "全部"
      },
      {
        id: 1,
        name: "未付款"
      },
      {
        id: 2,
        name: "待发货"
      },
      {
        id: 3,
        name: "已发货"
      },
      {
        id: 4,
        name: "已完成"
      },
      {
        id: 5,
        name: "已取消"
      }
    ]);
    const currentTab = common_vendor.ref(0);
    const tabCurrent = common_vendor.ref("tabNum1");
    common_vendor.ref(0);
    const top = common_vendor.ref("20px");
    let data = common_vendor.ref([]);
    const loading = common_vendor.ref(false);
    const loadingText = common_vendor.ref("加载中...");
    const id = common_vendor.ref("");
    const index = common_vendor.ref("");
    const idCfm = common_vendor.ref("");
    const indexCfm = common_vendor.ref("");
    common_vendor.ref("");
    common_vendor.ref("");
    const page = common_vendor.ref(1);
    const dataFlag = common_vendor.ref(false);
    function swichMenu(val) {
      currentTab.value = val;
      tabCurrent.value = "tabNum" + val;
      page.value = 1;
      data.value = [];
      getData();
    }
    function getData() {
      dataFlag.value = false;
      api_order.memberOrdersList({
        state: currentTab.value,
        page: page.value
      }).then((res) => {
        console.log(res);
        if (res.data.data.ordersList.length == 0 && page.value > 1 && data.value.length > 10) {
          loadingText.value = "到底啦~";
        } else {
          data.value = [...data.value, ...res.data.data.ordersList];
          loading.value = false;
        }
        dataFlag.value = true;
      });
    }
    function watchDetails(id2) {
      common_vendor.index.navigateTo({
        url: "/pages/memberCenter/myOrder/orderDetail?id=" + id2
      });
    }
    function jumpComment(val) {
      common_vendor.index.navigateTo({
        url: "/pages/memberCenter/myOrder/commentList?id=" + val
      });
    }
    const alertDialog = common_vendor.ref();
    function cancelOrder(val, index2) {
      alertDialog.value.open();
      id.value = val;
      index2.value = index2;
    }
    function dialogConfirm() {
      api_order.memberOrdersCancelOrder({
        id: id.value
      }).then((res) => {
        if (currentTab.value != 0) {
          data.value.splice(index.value, 1);
        } else {
          page.value = 1;
          data.value = [];
          getData();
        }
        if (res.data.success) {
          common_vendor.index.showToast({
            icon: "none",
            duration: 3e3,
            title: `操作成功`
          });
        }
      });
    }
    function dialogClose() {
      alertDialog.value.close();
    }
    const alertDialogCfm = common_vendor.ref();
    function confirmReceipt(val, index2) {
      alertDialogCfm.value.open();
      idCfm.value = val;
      indexCfm.value = index2;
    }
    function dialogConfirmCfm() {
      api_order.memberOrdersConfirmOrder({
        id: idCfm.value
      }).then((res) => {
        if (currentTab.value != 0) {
          data.value.splice(indexCfm.value, 1);
        } else {
          page.value = 1;
          data.value = [];
          getData();
        }
        if (res.data.success) {
          common_vendor.index.showToast({
            icon: "none",
            duration: 3e3,
            title: `操作成功`
          });
        }
      });
    }
    function dialogCloseCfm() {
      alertDialogCfm.value.close();
    }
    function orderState(data2) {
      switch (data2) {
        case 1:
          return "未付款";
        case 2:
          return "待发货";
        case 3:
          return "已发货";
        case 4:
          return "已完成";
        case 5:
          return "已取消";
      }
    }
    function confirmOrder(data2) {
      common_vendor.index.navigateTo({
        url: "/pages/shoppingCart/paymentMethod?id=" + data2.id
      });
    }
    function jumpDEtails(id2, type, iid) {
      if (type == 1) {
        common_vendor.index.navigateTo({
          url: "/pages/homePage/goodsDetail?id=" + id2
        });
      } else {
        common_vendor.index.navigateTo({
          url: "/pages/memberCenter/integralPage/integralDetails?id=" + iid
        });
      }
      console.log(id2);
    }
    function jump(val) {
      common_vendor.index.navigateTo({
        url: "/pages/memberCenter/myOrder/returnsExchanges?id=" + val
      });
    }
    common_vendor.onReachBottom(() => {
      if (data.value.length >= 20 * page.value) {
        page.value += 1;
        loading.value = true;
        loadingText.value = "加载中...";
        setTimeout(() => {
          getData();
        }, 800);
      } else {
        loading.value = true;
        loadingText.value = "到底啦~";
      }
    });
    common_vendor.onPullDownRefresh(() => {
      setTimeout(() => {
        data.value = [];
        page.value = 1;
        getData();
        common_vendor.index.stopPullDownRefresh();
      }, 800);
    });
    common_vendor.onLoad((options) => {
      if (options.stateid) {
        swichMenu(options.stateid);
      } else {
        swichMenu(0);
      }
    });
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: common_vendor.p({
          ["nav-background-color"]: "#f7f7f5",
          backImageUrl: "1",
          textContent: "我的订单",
          fontSize: "15",
          fontWeight: "600"
        }),
        b: common_vendor.f(common_vendor.unref(tabs), (item, k0, i0) => {
          return {
            a: common_vendor.t(item.name),
            b: common_vendor.n(currentTab.value == item.id ? "menu-topic-act" : "menu-topic"),
            c: "tabNum" + item.id,
            d: common_vendor.o(($event) => swichMenu(item.id))
          };
        }),
        c: top.value,
        d: common_vendor.unref(data).length != 0
      }, common_vendor.unref(data).length != 0 ? {
        e: common_vendor.f(common_vendor.unref(data), (item, index2, i0) => {
          return common_vendor.e({
            a: common_vendor.t(item.name),
            b: common_vendor.t(item.mobile),
            c: common_vendor.t(orderState(item.ordersState)),
            d: common_vendor.f(item.ordersProductList, (items, k1, i1) => {
              return {
                a: common_vendor.o(($event) => jumpDEtails(items.productId, item.ordersType, item.integralId)),
                b: _ctx.imageUrl + items.productImg,
                c: common_vendor.t(items.productName),
                d: common_vendor.o(($event) => jumpDEtails(items.productId, item.ordersType, item.integralId)),
                e: common_vendor.t(items.specInfo),
                f: common_vendor.n(items.specInfo ? "font2" : "font22"),
                g: common_vendor.t(items.moneyPrice),
                h: common_vendor.t(items.number)
              };
            }),
            e: item.ordersType == "1"
          }, item.ordersType == "1" ? {
            f: common_vendor.t(item.moneyOrder)
          } : {}, {
            g: item.ordersType == "2"
          }, item.ordersType == "2" ? {
            h: common_vendor.t(item.integral),
            i: common_vendor.t(item.moneyOrder == 0 ? "" : "+￥" + item.moneyOrder)
          } : {}, {
            j: common_vendor.o(($event) => watchDetails(item.id)),
            k: item.ordersState == 1 || item.ordersState == 2
          }, item.ordersState == 1 || item.ordersState == 2 ? {
            l: common_vendor.o(($event) => cancelOrder(item.id, index2))
          } : {}, {
            m: item.ordersState == 1
          }, item.ordersState == 1 ? {
            n: common_vendor.o(($event) => confirmOrder(item))
          } : {}, {
            o: item.ordersState == 3 && !item.dorpshoppingFlag
          }, item.ordersState == 3 && !item.dorpshoppingFlag ? {
            p: common_vendor.o(($event) => confirmReceipt(item.id, index2))
          } : {}, {
            q: item.ordersState == 4
          }, item.ordersState == 4 ? {
            r: common_vendor.o(($event) => jumpComment(item.id))
          } : {}, {
            s: item.ordersState == 4
          }, item.ordersState == 4 ? {
            t: common_vendor.o(($event) => jump(item.id))
          } : {});
        })
      } : {}, {
        f: loading.value
      }, loading.value ? {
        g: common_vendor.t(loadingText.value)
      } : {}, {
        h: common_vendor.unref(data).length == 0 && dataFlag.value
      }, common_vendor.unref(data).length == 0 && dataFlag.value ? {
        i: common_assets._imports_0$2
      } : {}, {
        j: common_vendor.o(dialogConfirm),
        k: common_vendor.o(dialogClose),
        l: common_vendor.p({
          type: _ctx.msgType,
          cancelText: "取消",
          confirmText: "确认",
          content: "确认取消此条订单吗?"
        }),
        m: common_vendor.sr(alertDialog, "dd750666-1", {
          "k": "alertDialog"
        }),
        n: common_vendor.p({
          type: "dialog"
        }),
        o: common_vendor.o(dialogConfirmCfm),
        p: common_vendor.o(dialogCloseCfm),
        q: common_vendor.p({
          type: _ctx.msgType,
          cancelText: "取消",
          confirmText: "确认",
          content: "确认收货吗?"
        }),
        r: common_vendor.sr(alertDialogCfm, "dd750666-3", {
          "k": "alertDialogCfm"
        }),
        s: common_vendor.p({
          type: "dialog"
        })
      });
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-dd750666"]]);
wx.createPage(MiniProgramPage);
