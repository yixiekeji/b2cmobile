"use strict";
const common_vendor = require("../../../common/vendor.js");
const common_assets = require("../../../common/assets.js");
require("../../../store/login.js");
const api_member = require("../../../api/member.js");
const api_pay = require("../../../api/pay.js");
const api_integralPage = require("../../../api/integralPage.js");
const store_order = require("../../../store/order.js");
if (!Array) {
  const _component_HeadNav = common_vendor.resolveComponent("HeadNav");
  const _easycom_uni_popup_dialog2 = common_vendor.resolveComponent("uni-popup-dialog");
  const _easycom_uni_popup2 = common_vendor.resolveComponent("uni-popup");
  (_component_HeadNav + _easycom_uni_popup_dialog2 + _easycom_uni_popup2)();
}
const _easycom_uni_popup_dialog = () => "../../../uni_modules/uni-popup/components/uni-popup-dialog/uni-popup-dialog.js";
const _easycom_uni_popup = () => "../../../uni_modules/uni-popup/components/uni-popup/uni-popup.js";
if (!Math) {
  (_easycom_uni_popup_dialog + _easycom_uni_popup)();
}
const _sfc_main = {
  __name: "confirmOrder",
  setup(__props) {
    let details = common_vendor.ref({});
    const addressFlag = common_vendor.ref(false);
    let addressed = common_vendor.ref([]);
    const addressId = common_vendor.ref("");
    const invoice = common_vendor.ref("0");
    const notes = common_vendor.ref("");
    const invoiceId = common_vendor.ref("");
    const invoiceInfo = common_vendor.ref("");
    common_vendor.ref("");
    const state = common_vendor.ref("1");
    const couponSn = common_vendor.ref("");
    const couponMoney = common_vendor.ref("");
    const orderSn = common_vendor.ref("");
    common_vendor.ref(0);
    common_vendor.ref("wxh5");
    let codes = common_vendor.ref("");
    common_vendor.ref(false);
    function getDetails() {
      api_integralPage.ordersjifen({
        integralId: store_order.order().order.productId,
        integralGoodsId: store_order.order().order.productGoodId,
        number: store_order.order().order.number,
        addressId: addressId.value
      }).then((res) => {
        details.value = res.data.data;
      });
    }
    function getAddress(id) {
      if (id == "00") {
        api_member.address().then((res) => {
          console.log(res);
          if (res.data.data.length == 0) {
            addressFlag.value = false;
          } else {
            addressed.value = res.data.data;
            addressId.value = res.data.data[0].id;
            store_order.order().setAddressId(addressId.value);
            addressed.value.length == 0 ? addressFlag.value = false : addressFlag.value = true;
          }
          getDetails();
        });
      } else {
        api_member.getMemberAddress({
          id
        }).then((res) => {
          addressed.value = [];
          addressed.value.push(res.data.data);
          addressId.value = res.data.data.id;
          store_order.order().setAddressId(addressId.value);
          addressed.value.length == 0 ? addressFlag.value = false : addressFlag.value = true;
          getDetails();
        });
      }
    }
    common_vendor.ref();
    common_vendor.reactive([
      {
        id: 2,
        name: "可用优惠券"
      },
      {
        id: 1,
        name: "不可用优惠券"
      }
    ]);
    common_vendor.ref(1);
    common_vendor.ref("tabNum1");
    common_vendor.ref(0);
    common_vendor.ref(0);
    common_vendor.ref(1);
    common_vendor.ref([]);
    common_vendor.ref("");
    common_vendor.ref(false);
    const popup = common_vendor.ref();
    function addNotes() {
      popup.value.open();
    }
    function close() {
      popup.value.close();
    }
    function confirm(value) {
      if (value.length > 50) {
        common_vendor.index.showToast({
          icon: "none",
          duration: 3e3,
          title: "请将订单备注控制在50字以内"
        });
      } else {
        notes.value = value;
        store_order.order().setRemarks(value);
        popup.value.close();
      }
    }
    const popups = common_vendor.ref();
    function confirmOrders() {
      if (addressId.value == "") {
        common_vendor.index.showToast({
          icon: "none",
          duration: 3e3,
          title: "请选择收货地址"
        });
      } else {
        popups.value.open("center");
        api_integralPage.submitintegral({
          addressId: addressId.value,
          invoiceStatus: invoice.value,
          payState: state.value,
          invoiceId: invoiceId.value,
          invoiceInfo: invoiceInfo.value,
          number: store_order.order().order.number,
          integralId: store_order.order().order.productId,
          integralGoodsId: store_order.order().order.productGoodId,
          remark: notes.value
        }).then((res) => {
          if (res.data.success) {
            if (details.value.amount != 0 || details.value.logisticsFeeAmount != 0) {
              orderSn.value = res.data.data.ordersSn;
              payment();
            } else {
              common_vendor.index.navigateTo({
                url: "/pages/shoppingCart/paySuccess?price=" + (details.value.amount + details.value.logisticsFeeAmount) + "&jifen=" + details.value.integral.price * details.value.totalNumber
              });
              popups.value.close("center");
            }
          } else {
            popups.value.close("center");
          }
        });
      }
    }
    let formContent = common_vendor.ref("");
    function payment() {
      {
        common_vendor.index.login({
          provider: "weixin",
          success(res) {
            console.log(res);
            if (res.code) {
              codes.value = res.code;
              api_pay.xcxpayindex({
                code: res.code,
                orderPaySn: orderSn.value
              }).then((result) => {
                if (!result.data.success && result.data.message.indexOf(
                  "out_trade_no已经在其他合单中使用过"
                ) != -1) {
                  console.log("11111", result);
                  payment();
                  return;
                } else if (!result.data.success) {
                  setTimeout(() => {
                    common_vendor.index.navigateTo({
                      url: "/pages/memberCenter/myOrder/myOrder?orderState=0"
                    });
                    popups.value.close("center");
                  }, 1e3);
                }
                console.log(result.data.data);
                common_vendor.wx$1.requestPayment({
                  "timeStamp": result.data.data.timeStamp,
                  //后端返回的时间戳
                  "nonceStr": result.data.data.nonceStr,
                  //后端返回的随机字符串
                  "package": result.data.data.package,
                  //后端返回的prepay_id
                  "signType": "MD5",
                  //后端签名算法,根据后端来,后端MD5这里即为MD5
                  "paySign": result.data.data.paySign,
                  //后端返回的签名
                  success(res2) {
                    console.log("用户支付扣款成功", res2);
                    common_vendor.index.navigateTo({
                      url: "/pages/shoppingCart/paySuccess?price=" + (details.value.amount + details.value.logisticsFeeAmount) + "&jifen=" + details.value.integral.price * details.value.totalNumber
                    });
                  },
                  fail(res2) {
                    console.log("用户支付扣款失败", res2);
                    common_vendor.index.navigateTo({
                      url: "/pages/memberCenter/myOrder/myOrder?orderState=1"
                    });
                  }
                });
              });
            } else {
              console.log("登录失败！" + res.errMsg);
            }
          }
        });
      }
    }
    function jumpAddress() {
      common_vendor.index.redirectTo({
        url: "/pages/memberCenter/address/address?order=jifenorder"
      });
    }
    function jumpInvoice() {
      common_vendor.index.redirectTo({
        url: "/pages/memberCenter/invoice/alertInvoice?invoice=jifeninvoice"
      });
    }
    function getUrlValue(value) {
      var query = location.search.substring(1);
      var hash = location.hash.split("?");
      if (!query && hash.length < 2) {
        return "";
      }
      var vars = query.split("&");
      for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        if (pair[0] == value) {
          return pair[1];
        }
      }
      if (!hash[1]) {
        return "";
      }
      vars = hash[1].split("&");
      for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        if (pair[0] == value) {
          return pair[1];
        }
      }
      return "";
    }
    common_vendor.onLoad((options) => {
      if (store_order.order().order.addressId != "") {
        getAddress(store_order.order().order.addressId);
      } else {
        getAddress("00");
      }
      couponSn.value = store_order.order().order.couponSn ? store_order.order().order.couponSn : "";
      couponMoney.value = store_order.order().order.couponMoney ? store_order.order().order.couponMoney : "";
      invoice.value = store_order.order().order.invoice.invoiceStatus ? store_order.order().order.invoice.invoiceStatus : "";
      invoiceId.value = store_order.order().order.invoice.invoiceId ? store_order.order().order.invoice.invoiceId : "";
      invoiceInfo.value = store_order.order().order.invoice.invoiceTitle ? store_order.order().order.invoice.invoiceTitle : "";
      notes.value = store_order.order().order.remarks ? store_order.order().order.remarks : "";
      orderSn.value = store_order.order().order.orderSn;
      if (getUrlValue("code")) {
        payment();
      }
    });
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: common_vendor.p({
          ["nav-background-color"]: "#f7f7f5",
          backImageUrl: "1",
          textContent: "积分商城确认订单",
          fontSize: "15",
          fontWeight: "600"
        }),
        b: addressFlag.value
      }, addressFlag.value ? {
        c: common_assets._imports_0$4,
        d: common_vendor.t(common_vendor.unref(addressed)[0].name),
        e: common_vendor.t(common_vendor.unref(addressed)[0].mobile),
        f: common_vendor.t(common_vendor.unref(addressed)[0].addAll + common_vendor.unref(addressed)[0].addressInfo),
        g: common_assets._imports_1$4,
        h: common_vendor.o(jumpAddress)
      } : {
        i: common_vendor.o(jumpAddress)
      }, {
        j: common_vendor.unref(details) && common_vendor.unref(details).integral
      }, common_vendor.unref(details) && common_vendor.unref(details).integral ? {
        k: _ctx.imageUrl + common_vendor.unref(details).integral.image
      } : {}, {
        l: common_vendor.t(common_vendor.unref(details).integral.name),
        m: common_vendor.unref(details).integralGoods.normName
      }, common_vendor.unref(details).integralGoods.normName ? {
        n: common_vendor.t(common_vendor.unref(details).integralGoods.normName)
      } : {}, {
        o: common_vendor.t(common_vendor.unref(details).totalNumber),
        p: common_vendor.t(common_vendor.unref(details).integral.price * common_vendor.unref(details).totalNumber),
        q: common_vendor.t(common_vendor.unref(details).amount),
        r: common_vendor.t(common_vendor.unref(details).logisticsFeeAmount),
        s: common_vendor.t(invoiceInfo.value),
        t: common_assets._imports_1$4,
        v: common_vendor.o(jumpInvoice),
        w: common_vendor.t(notes.value == "" ? "无备注" : notes.value),
        x: common_assets._imports_1$4,
        y: common_vendor.o(addNotes),
        z: common_vendor.t(common_vendor.unref(details).integral.price * common_vendor.unref(details).totalNumber),
        A: common_vendor.t(common_vendor.unref(details).amount + common_vendor.unref(details).logisticsFeeAmount == 0 ? "" : "+¥" + (common_vendor.unref(details).amount + common_vendor.unref(details).logisticsFeeAmount)),
        B: common_vendor.o(confirmOrders),
        C: common_vendor.sr("inputClose", "6a95da97-2,6a95da97-1"),
        D: common_vendor.o(confirm),
        E: common_vendor.o(close),
        F: common_vendor.p({
          mode: "input",
          title: "备注",
          value: notes.value,
          placeholder: "请输入内容"
        }),
        G: common_vendor.sr(popup, "6a95da97-1", {
          "k": "popup"
        }),
        H: common_vendor.p({
          type: "dialog"
        }),
        I: _ctx.type === "left" || _ctx.type === "right" ? 1 : "",
        J: common_vendor.sr(popups, "6a95da97-3", {
          "k": "popups"
        }),
        K: common_vendor.p({
          ["is-mask-click"]: false
        }),
        L: common_vendor.unref(formContent)
      });
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-6a95da97"]]);
wx.createPage(MiniProgramPage);
