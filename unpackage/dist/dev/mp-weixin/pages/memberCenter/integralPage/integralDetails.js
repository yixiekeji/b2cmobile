"use strict";
const common_vendor = require("../../../common/vendor.js");
const api_integralPage = require("../../../api/integralPage.js");
const api_reviews = require("../../../api/reviews.js");
const store_order = require("../../../store/order.js");
if (!Array) {
  const _component_HeadNav = common_vendor.resolveComponent("HeadNav");
  const _easycom_uni_swiper_dot2 = common_vendor.resolveComponent("uni-swiper-dot");
  const _easycom_uni_icons2 = common_vendor.resolveComponent("uni-icons");
  const _easycom_uni_data_checkbox2 = common_vendor.resolveComponent("uni-data-checkbox");
  const _easycom_uni_number_box2 = common_vendor.resolveComponent("uni-number-box");
  const _easycom_uni_popup2 = common_vendor.resolveComponent("uni-popup");
  (_component_HeadNav + _easycom_uni_swiper_dot2 + _easycom_uni_icons2 + _easycom_uni_data_checkbox2 + _easycom_uni_number_box2 + _easycom_uni_popup2)();
}
const _easycom_uni_swiper_dot = () => "../../../uni_modules/uni-swiper-dot/components/uni-swiper-dot/uni-swiper-dot.js";
const _easycom_uni_icons = () => "../../../uni_modules/uni-icons/components/uni-icons/uni-icons.js";
const _easycom_uni_data_checkbox = () => "../../../uni_modules/uni-data-checkbox/components/uni-data-checkbox/uni-data-checkbox.js";
const _easycom_uni_number_box = () => "../../../uni_modules/uni-number-box/components/uni-number-box/uni-number-box.js";
const _easycom_uni_popup = () => "../../../uni_modules/uni-popup/components/uni-popup/uni-popup.js";
if (!Math) {
  (_easycom_uni_swiper_dot + _easycom_uni_icons + _easycom_uni_data_checkbox + _easycom_uni_number_box + _easycom_uni_popup)();
}
const _sfc_main = {
  __name: "integralDetails",
  setup(__props) {
    let dotsStyles = common_vendor.ref({
      backgroundColor: "rgba(0, 0, 0,0.3)",
      border: "0px rgba(83, 200, 249,0.3) solid",
      color: "#fff",
      selectedBackgroundColor: "#1C9B64",
      selectedBorder: "2rpx solid #FFFFFF"
    });
    const mode = common_vendor.ref("default");
    common_vendor.ref([]);
    const current = common_vendor.ref(0);
    let details = common_vendor.ref({});
    let norm1s = common_vendor.ref([]);
    const norm1 = common_vendor.ref("");
    const norm1Value = common_vendor.ref("");
    let norm2s = common_vendor.ref([]);
    const norm2 = common_vendor.ref("");
    const norm2Value = common_vendor.ref("");
    common_vendor.ref("");
    let masterImg = common_vendor.ref([]);
    const masterImgs = common_vendor.ref("");
    const mallPrice = common_vendor.ref("");
    const name = common_vendor.ref("");
    const description = common_vendor.ref("");
    const stock = common_vendor.ref("");
    common_vendor.ref(false);
    const productId = common_vendor.ref("");
    const max = common_vendor.ref(1);
    const min = common_vendor.ref(0);
    const numValue = common_vendor.ref(1);
    const productGoodId = common_vendor.ref("");
    const id = common_vendor.ref("");
    const chooseValue = common_vendor.ref("");
    common_vendor.ref("");
    const isSelf = common_vendor.ref("");
    const report = common_vendor.ref("");
    common_vendor.ref("0");
    const popup = common_vendor.ref();
    let integral = common_vendor.ref({});
    const count = common_vendor.ref(1);
    let productCommentList = common_vendor.ref([]);
    const price = common_vendor.ref("");
    const state = common_vendor.ref("");
    function norm2Change(val) {
      getinfor();
      choose();
    }
    function norm1Change(val) {
      getinfor();
      choose();
    }
    function getinfor() {
      if (norm1.value == 99 && norm2.value == 99) {
        return;
      } else if (norm2.value == 99) {
        setTimeout(() => {
          api_integralPage.detailgoods({
            normName: chooseValue.value,
            integralId: id.value
          }).then((res) => {
            mallPrice.value = res.data.data.mallPrice;
            price.value = res.data.data.integralPrice;
            if (res.data.data.stock == 0) {
              min.value = 0;
              max.value = 0;
              numValue.value = 0;
            } else {
              min.value = 1;
              max.value = res.data.data.stock;
              numValue.value = 1;
            }
            productGoodId.value = res.data.data.id;
            stock.value = res.data.data.stock == 0 ? "暂无库存" : "(剩余库存:" + res.data.data.stock + ")";
          });
        }, 100);
      } else if (norm1.value != 99 && norm2.value != 99) {
        if (norm1Value.value != "" && norm2Value.value != "") {
          setTimeout(() => {
            api_integralPage.detailgoods({
              normName: chooseValue.value,
              integralId: id.value
            }).then((res) => {
              console.log(res);
              mallPrice.value = res.data.data.mallPrice;
              price.value = res.data.data.integralPrice;
              if (res.data.data.stock == 0) {
                min.value = 0;
                max.value = 0;
                numValue.value = 0;
              } else {
                min.value = 1;
                max.value = res.data.data.stock;
                numValue.value = 1;
              }
              productGoodId.value = res.data.data.id;
              stock.value = res.data.data.stock == 0 ? "暂无库存" : "(剩余库存:" + res.data.data.stock + ")";
            });
          }, 100);
        }
      }
    }
    function addCart() {
      productId.value = id.value;
      norm1s.value = [];
      norm2s.value = [];
      api_integralPage.jifenDetail({
        integralId: id.value
      }).then((res) => {
        if (res.data.data.product.isNorm == 0) {
          norm1.value = 99;
          norm2.value = 99;
          productGoodId.value = res.data.data.integralGoods.id;
        } else if (res.data.data.product.isNorm == 1) {
          norm1.value = res.data.data.norm1;
          res.data.data.norm1s.forEach((item) => {
            norm1s.value.push({
              text: item,
              value: item
            });
            norm1Value.value = norm1s.value[0].value;
          });
          if (res.data.data.norm2) {
            norm2.value = res.data.data.norm2;
            res.data.data.norm2s.forEach((item) => {
              norm2s.value.push({
                text: item,
                value: item
              });
              norm2Value.value = norm2s.value[0].value;
            });
          } else {
            norm2.value = 99;
          }
          choose();
          getinfor();
        }
        stock.value = res.data.data.integral.stock == 0 ? "暂无库存" : "(剩余库存:" + res.data.data.integral.stock + ")";
        name.value = res.data.data.integral.name;
        integral.value = res.data.data.integral;
        description.value = formatRichText(res.data.data.integral.descinfo);
        masterImg.value = res.data.data.integral.productPictureList;
        masterImgs.value = res.data.data.integral.image;
        mallPrice.value = res.data.data.integral.priceMoney;
        state.value = res.data.data.integral.state;
        price.value = res.data.data.integral.price;
        details.value = res.data.data.product;
        isSelf.value = res.data.data.product.isSelf;
        report.value = res.data.data.report;
      });
    }
    function close() {
      popup.value.close();
    }
    function confirmAddCart(val) {
      if (state.value != 2) {
        common_vendor.index.showToast({
          icon: "none",
          duration: 3e3,
          title: `该商品已下架`
        });
        return;
      }
      if (val == 1) {
        popup.value.close();
      } else {
        store_order.order().resize();
        store_order.order().setProductId(productId.value);
        store_order.order().setProductGoodId(productGoodId.value);
        store_order.order().setNumber(count.value);
        console.log(store_order.order().order);
        popup.value.close();
        if (common_vendor.index.getStorageSync("token")) {
          common_vendor.index.navigateTo({
            url: "/pages/memberCenter/integralPage/confirmOrder"
          });
        } else {
          common_vendor.index.navigateTo({
            url: "/pages/loginInterface/passwordLogin"
          });
        }
      }
    }
    function jumpDetails() {
      common_vendor.index.navigateTo({
        url: "/pages/homePage/goodsDetail?id=" + id.value
      });
    }
    function choose() {
      let aaa = "";
      if (norm1.value != 99 && norm1Value.value != "") {
        aaa = norm1.value + ",";
      } else {
        aaa = "";
      }
      let bbb = "";
      if (norm1Value.value != "" && norm2Value.value != "") {
        bbb = ";";
      } else {
        bbb = "";
      }
      let ccc = "";
      if (norm2.value != 99 && norm2Value.value != "") {
        ccc = norm2.value + ",";
      } else {
        ccc = "";
      }
      chooseValue.value = aaa + norm1Value.value + bbb + ccc + norm2Value.value;
    }
    function getComment() {
      api_reviews.comment({
        productId: id.value
      }).then((res) => {
        console.log(res);
        let data = res.data.data;
        data.forEach((item) => {
          if (item.replyContent.length > 51) {
            item.textOpenFlag = true;
          } else {
            item.textOpenFlag = null;
          }
        });
        productCommentList.value = [...data];
      });
    }
    function formatRichText(html) {
      let newContent = html.replace(/<img[^>]*>/gi, function(match, capture) {
        match = match.replace(/style="[^"]+"/gi, "").replace(/style='[^']+'/gi, "");
        match = match.replace(/width="[^"]+"/gi, "").replace(/width='[^']+'/gi, "");
        match = match.replace(/height="[^"]+"/gi, "").replace(/height='[^']+'/gi, "");
        return match;
      });
      newContent = newContent.replace(/style="[^"]+"/gi, function(match, capture) {
        match = match.replace(/width:[^;]+;/gi, "width:100%;").replace(/width:[^;]+;/gi, "width:100%;");
        return match;
      });
      newContent = newContent.replace(/<br[^>]*\/>/gi, "");
      newContent = newContent.replace(/\<img/gi, '<img style="width:100%;height:auto;display:block;"');
      return newContent;
    }
    common_vendor.onLoad((options) => {
      id.value = options.id;
      addCart();
      getComment();
    });
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: common_vendor.o(_ctx.test),
        b: common_vendor.p({
          ["nav-background-color"]: "#f7f7f5",
          backImageUrl: "1",
          textContent: "积分商城详情",
          fontSize: "16",
          fontWeight: "600"
        }),
        c: common_vendor.unref(masterImg)
      }, common_vendor.unref(masterImg) ? {
        d: common_vendor.f(common_vendor.unref(masterImg), (item, index, i0) => {
          return {
            a: _ctx.imageUrl + item.imagePath,
            b: index
          };
        })
      } : {
        e: _ctx.imageUrl + masterImgs.value
      }, {
        f: common_vendor.o((...args) => _ctx.change && _ctx.change(...args)),
        g: common_vendor.p({
          info: common_vendor.unref(masterImg),
          current: current.value,
          field: "content",
          mode: mode.value,
          ["dots-styles"]: common_vendor.unref(dotsStyles)
        }),
        h: common_vendor.t(price.value),
        i: common_vendor.unref(integral).isWithMoney == "1"
      }, common_vendor.unref(integral).isWithMoney == "1" ? {
        j: common_vendor.t(mallPrice.value)
      } : {}, {
        k: common_vendor.t(common_vendor.unref(integral).saleNum),
        l: common_vendor.t(common_vendor.unref(integral).marketPrice),
        m: common_vendor.t(common_vendor.unref(integral).endTime),
        n: common_vendor.t(common_vendor.unref(integral).name),
        o: common_vendor.t(chooseValue.value),
        p: common_vendor.t(stock.value),
        q: common_vendor.p({
          type: "right",
          size: "17",
          color: "#999999"
        }),
        r: norm2.value != 99
      }, norm2.value != 99 ? {
        s: common_vendor.t(norm2.value),
        t: common_vendor.o(norm2Change),
        v: common_vendor.o(($event) => norm2Value.value = $event),
        w: common_vendor.p({
          selectedColor: "rgba(28, 155, 100, 0.4)",
          selectedTextColor: "#1C9B64",
          mode: "tag",
          localdata: common_vendor.unref(norm2s),
          modelValue: norm2Value.value
        })
      } : {}, {
        x: norm1.value != 99
      }, norm1.value != 99 ? {
        y: common_vendor.t(norm1.value),
        z: common_vendor.o(norm1Change),
        A: common_vendor.o(($event) => norm1Value.value = $event),
        B: common_vendor.p({
          selectedColor: "rgba(28, 155, 100, 0.4)",
          selectedTextColor: "#1C9B64",
          mode: "tag",
          localdata: common_vendor.unref(norm1s),
          modelValue: norm1Value.value
        })
      } : {}, {
        C: common_vendor.t(common_vendor.unref(integral).purchase),
        D: common_vendor.o(($event) => _ctx.addShopCarts($event, count.value, _ctx.item.id)),
        E: common_vendor.o(($event) => count.value = $event),
        F: common_vendor.p({
          min: "1",
          max: common_vendor.unref(integral).purchase,
          modelValue: count.value
        }),
        G: description.value,
        H: common_vendor.o(($event) => jumpDetails()),
        I: common_vendor.o(($event) => confirmAddCart()),
        J: _ctx.imageUrl + masterImgs.value,
        K: common_vendor.t(name.value),
        L: common_vendor.t(mallPrice.value),
        M: common_vendor.o(close),
        N: common_vendor.p({
          type: "closeempty",
          color: "#999",
          size: "18"
        }),
        O: norm1Value.value != "" || norm2Value.value != ""
      }, norm1Value.value != "" || norm2Value.value != "" ? {
        P: common_vendor.t(chooseValue.value)
      } : {}, {
        Q: norm2.value != 99
      }, norm2.value != 99 ? {
        R: common_vendor.t(norm2.value),
        S: common_vendor.o(norm2Change),
        T: common_vendor.o(($event) => norm2Value.value = $event),
        U: common_vendor.p({
          selectedColor: "rgba(28, 155, 100, 0.1)",
          selectedTextColor: "#1C9B64",
          mode: "tag",
          localdata: common_vendor.unref(norm2s),
          modelValue: norm2Value.value
        })
      } : {}, {
        V: norm1.value != 99
      }, norm1.value != 99 ? {
        W: common_vendor.t(norm1.value),
        X: common_vendor.o(norm1Change),
        Y: common_vendor.o(($event) => norm1Value.value = $event),
        Z: common_vendor.p({
          selectedColor: "rgba(28, 155, 100, 0.1)",
          selectedTextColor: "#1C9B64",
          mode: "tag",
          localdata: common_vendor.unref(norm1s),
          modelValue: norm1Value.value
        })
      } : {}, {
        aa: common_vendor.t(stock.value),
        ab: common_vendor.o(($event) => numValue.value = $event),
        ac: common_vendor.p({
          max: max.value,
          min: min.value,
          modelValue: numValue.value
        }),
        ad: common_vendor.t(stock.value == "暂无库存" ? stock.value : "确定"),
        ae: common_vendor.o(($event) => confirmAddCart(1)),
        af: stock.value == "暂无库存",
        ag: common_vendor.sr(popup, "7254f446-6", {
          "k": "popup"
        }),
        ah: common_vendor.p({
          type: "bottom"
        })
      });
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-7254f446"]]);
wx.createPage(MiniProgramPage);
