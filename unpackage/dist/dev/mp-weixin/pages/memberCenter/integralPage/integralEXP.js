"use strict";
const common_vendor = require("../../../common/vendor.js");
const api_integralPage = require("../../../api/integralPage.js");
const api_member = require("../../../api/member.js");
if (!Array) {
  const _component_HeadNav = common_vendor.resolveComponent("HeadNav");
  _component_HeadNav();
}
const _sfc_main = {
  __name: "integralEXP",
  setup(__props) {
    const page = common_vendor.ref(1);
    const loading = common_vendor.ref(false);
    const loadingText = common_vendor.ref("加载中...");
    let tabs = common_vendor.ref([
      {
        id: 1,
        name: "积分"
      },
      {
        id: 2,
        name: "经验值"
      }
    ]);
    const currentTab = common_vendor.ref(0);
    const tabCurrent = common_vendor.ref("tabNum1");
    common_vendor.ref(0);
    const top = common_vendor.ref(0);
    let memberGradeIntegralLogsList = common_vendor.ref([]);
    function swichMenu(id) {
      memberGradeIntegralLogsList.value = [];
      page.value = 1;
      currentTab.value = id;
      tabCurrent.value = "tabNum" + id;
      getList();
    }
    function getList() {
      if (currentTab.value == 0) {
        api_integralPage.integral({
          page: page.value
        }).then((res) => {
          if (res.data.data.memberGradeIntegralLogsList.length == 0 && pageValue.value > 1 && list.value.length > 10) {
            loadingText.value = "到底啦~";
          } else {
            memberGradeIntegralLogsList.value = [
              ...memberGradeIntegralLogsList.value,
              ...res.data.data.memberGradeIntegralLogsList
            ];
            loading.value = false;
          }
        });
      } else {
        api_integralPage.grade({
          page: page.value
        }).then((res) => {
          if (res.data.data.memberGradeIntegralLogsList.length == 0 && pageValue.value > 1 && list.value.length > 10) {
            loadingText.value = "到底啦~";
          } else {
            memberGradeIntegralLogsList.value = [
              ...memberGradeIntegralLogsList.value,
              ...res.data.data.memberGradeIntegralLogsList
            ];
            loading.value = false;
          }
        });
      }
    }
    let gradeIntegral = common_vendor.ref({});
    function getData() {
      api_member.memberindex().then((res) => {
        console.log(res);
        tabs.value[0].name = "积分（" + res.data.data.member.integral + "）";
        tabs.value[1].name = "经验值（" + res.data.data.member.gradeValue + "）";
        gradeIntegral.value = res.data.data.member;
      });
    }
    common_vendor.onShow(() => {
      page.value = 1;
      memberGradeIntegralLogsList.value = [];
      loading.value = false;
      getData();
      getList();
    });
    common_vendor.onReachBottom(() => {
      if (memberGradeIntegralLogsList.value.length >= 20 * page.value) {
        page.value += 1;
        loading.value = true;
        loadingText.value = "加载中...";
        setTimeout(() => {
          getList();
        }, 800);
      } else {
        loading.value = true;
        loadingText.value = "到底啦~";
      }
    });
    common_vendor.onPullDownRefresh(() => {
      setTimeout(() => {
        page.value = 1;
        memberGradeIntegralLogsList.value = [];
        getList();
        getData();
        common_vendor.index.stopPullDownRefresh();
      }, 800);
    });
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: common_vendor.o(_ctx.test),
        b: common_vendor.p({
          ["nav-background-color"]: "#F5F5F7",
          backImageUrl: "1",
          textContent: "积分和经验值",
          fontSize: "16",
          fontWeight: "600"
        }),
        c: common_vendor.f(common_vendor.unref(tabs), (item, k0, i0) => {
          return {
            a: common_vendor.t(item.name),
            b: common_vendor.n(currentTab.value == item.id - 1 ? "menu-topic-act" : "menu-topic"),
            c: "tabNum" + item.id,
            d: item.id - 1,
            e: common_vendor.o(($event) => swichMenu(item.id - 1), item.id - 1)
          };
        }),
        d: top.value,
        e: common_vendor.f(common_vendor.unref(memberGradeIntegralLogsList), (item, k0, i0) => {
          return {
            a: common_vendor.t(item.createTime),
            b: common_vendor.t(item.optDes),
            c: common_vendor.t(item.optType == "6" || item.optType == "9" || item.optType == "7" ? "-" : "+"),
            d: common_vendor.t(item.value),
            e: common_vendor.t(item.optType == "6" || item.optType == "9" || item.optType == "7" ? "减少" : "增加")
          };
        }),
        f: common_vendor.t(currentTab.value == 0 ? "积分" : "经验值"),
        g: loading.value
      }, loading.value ? {
        h: common_vendor.t(loadingText.value)
      } : {});
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-5f1fe7e1"]]);
wx.createPage(MiniProgramPage);
