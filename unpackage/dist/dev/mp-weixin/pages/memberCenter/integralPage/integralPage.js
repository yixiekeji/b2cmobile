"use strict";
const common_vendor = require("../../../common/vendor.js");
const common_assets = require("../../../common/assets.js");
const api_integralPage = require("../../../api/integralPage.js");
if (!Array) {
  const _component_HeadNav = common_vendor.resolveComponent("HeadNav");
  const _easycom_uni_swiper_dot2 = common_vendor.resolveComponent("uni-swiper-dot");
  const _easycom_uni_icons2 = common_vendor.resolveComponent("uni-icons");
  (_component_HeadNav + _easycom_uni_swiper_dot2 + _easycom_uni_icons2)();
}
const _easycom_uni_swiper_dot = () => "../../../uni_modules/uni-swiper-dot/components/uni-swiper-dot/uni-swiper-dot.js";
const _easycom_uni_icons = () => "../../../uni_modules/uni-icons/components/uni-icons/uni-icons.js";
if (!Math) {
  (_easycom_uni_swiper_dot + _easycom_uni_icons)();
}
const _sfc_main = {
  __name: "integralPage",
  setup(__props) {
    const pageSize = common_vendor.ref();
    let bannerList = common_vendor.ref([]);
    const current = common_vendor.ref(0);
    let modes = common_vendor.ref("dot");
    const dotsStyles = common_vendor.reactive({
      backgroundColor: "rgba(0, 0, 0,0.3)",
      border: "0px rgba(83, 200, 249,0.3) solid",
      color: "#fff",
      selectedBackgroundColor: "#1C9B64",
      selectedBorder: "2rpx solid #FFFFFF"
    });
    const openFlag = common_vendor.ref(false);
    const classificationFlag = common_vendor.ref(true);
    const cateId = common_vendor.ref("0");
    let shopCates = common_vendor.ref([]);
    const page = common_vendor.ref(1);
    const loading = common_vendor.ref(false);
    const loadingText = common_vendor.ref("加载中...");
    let integrals = common_vendor.ref([]);
    const dataFlag = common_vendor.ref(false);
    function classification(e) {
      if (e.target.id) {
        cateId.value = e.target.id;
        integrals.value = [];
        page.value = 1;
        loading.value = false;
        getList();
      }
    }
    function open() {
      openFlag.value = !openFlag.value;
      classificationFlag.value = !classificationFlag.value;
    }
    function getIntegralType() {
      api_integralPage.integralType().then((res) => {
        shopCates.value = res.data.data;
      });
    }
    function change(e) {
      current.value = e.detail.current;
    }
    function getBannerList() {
      api_integralPage.integralBanner().then((res) => {
        bannerList.value = res.data.data;
      });
    }
    function getList() {
      dataFlag.value = false;
      api_integralPage.jifen({
        type: cateId.value,
        sort: 0,
        page: page.value
      }).then((res) => {
        pageSize.value = res.data.data.pageSize;
        if (res.data.data.integrals.length == 0 && page.value > 1) {
          loadingText.value = "到底啦~";
        } else {
          integrals.value = [...integrals.value, ...res.data.data.integrals];
          loading.value = false;
        }
        dataFlag.value = true;
      });
    }
    let issignData = common_vendor.ref({});
    function getissign() {
      api_integralPage.issign().then((res) => {
        issignData.value = res.data.data;
      });
    }
    function dosign() {
      api_integralPage.sign().then((res) => {
        if (res.data.success) {
          common_vendor.index.showToast({
            icon: "none",
            duration: 3e3,
            title: `签到成功`
          });
          getissign();
        }
      });
    }
    function jumpLogin() {
      common_vendor.index.navigateTo({
        url: "/pages/loginInterface/passwordLogin"
      });
    }
    function jumpDetails(val) {
      common_vendor.index.navigateTo({
        url: "/pages/memberCenter/integralPage/integralDetails?id=" + val
      });
    }
    function pageJump(val) {
      common_vendor.index.navigateTo({
        url: val
      });
    }
    common_vendor.onShow(() => {
      page.value = 1;
      cateId.value = 0;
      integrals.value = [];
      loading.value = false;
      getList();
      getissign();
      getBannerList();
      getIntegralType();
    });
    common_vendor.onReachBottom(() => {
      if (integrals.value.length >= pageSize.value * page.value) {
        page.value += 1;
        loading.value = true;
        loadingText.value = "加载中...";
        setTimeout(() => {
          getList();
        }, 800);
      } else {
        loading.value = true;
        loadingText.value = "到底啦~";
      }
    });
    common_vendor.onPullDownRefresh(() => {
      setTimeout(() => {
        page.value = 1;
        cateId.value = 0;
        integrals.value = [];
        getList();
        common_vendor.index.stopPullDownRefresh();
      }, 800);
    });
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: common_vendor.p({
          ["nav-background-color"]: "#FFFFFF",
          backImageUrl: "1",
          textContent: "积分商城",
          fontSize: "16",
          fontWeight: "600"
        }),
        b: common_vendor.unref(bannerList).length != 0
      }, common_vendor.unref(bannerList).length != 0 ? {
        c: common_vendor.f(common_vendor.unref(bannerList), (item, index, i0) => {
          return {
            a: common_vendor.o(($event) => pageJump(item.linkUrl), index),
            b: _ctx.imageUrl + item.image,
            c: index
          };
        }),
        d: common_vendor.o(change),
        e: common_vendor.p({
          info: common_vendor.unref(bannerList),
          current: current.value,
          field: "content",
          mode: common_vendor.unref(modes),
          ["dots-styles"]: dotsStyles
        })
      } : {}, {
        f: common_vendor.unref(issignData).member
      }, common_vendor.unref(issignData).member ? {
        g: common_vendor.t(common_vendor.unref(issignData).member.integral)
      } : {
        h: common_vendor.p({
          type: "person-filled",
          color: "#1C9B64",
          size: "30"
        }),
        i: common_vendor.o(jumpLogin)
      }, {
        j: common_vendor.p({
          type: "calendar",
          color: "#1C9B64",
          size: "30"
        }),
        k: common_vendor.unref(issignData).isSign == "0"
      }, common_vendor.unref(issignData).isSign == "0" ? {} : {}, {
        l: common_vendor.o(dosign),
        m: common_assets._imports_0$12,
        n: common_vendor.n(cateId.value == 0 ? "active" : ""),
        o: common_vendor.f(common_vendor.unref(shopCates), (item, index, i0) => {
          return {
            a: common_vendor.t(item.name),
            b: common_vendor.n(cateId.value == item.id ? "active" : ""),
            c: item.id
          };
        }),
        p: common_vendor.n(classificationFlag.value ? "classificationEvery" : "classificationAllEvery"),
        q: common_vendor.o(($event) => classification($event)),
        r: !classificationFlag.value
      }, !classificationFlag.value ? {} : {}, {
        s: common_vendor.o(open),
        t: common_vendor.p({
          type: openFlag.value ? "bottom" : "top",
          size: "22"
        }),
        v: common_vendor.f(common_vendor.unref(integrals), (item, k0, i0) => {
          return common_vendor.e({
            a: _ctx.imageUrl + item.image,
            b: common_vendor.t(item.name),
            c: item.gradeValue == 1
          }, item.gradeValue == 1 ? {} : {}, {
            d: item.gradeValue == 2
          }, item.gradeValue == 2 ? {} : {}, {
            e: item.gradeValue == 3
          }, item.gradeValue == 3 ? {} : {}, {
            f: item.gradeValue == 4
          }, item.gradeValue == 4 ? {} : {}, {
            g: item.gradeValue == 5
          }, item.gradeValue == 5 ? {} : {}, {
            h: common_vendor.t(item.marketPrice),
            i: common_vendor.t(item.saleNum),
            j: common_vendor.t(item.price),
            k: item.isWithMoney == "1"
          }, item.isWithMoney == "1" ? {
            l: common_vendor.t(item.priceMoney)
          } : {}, {
            m: common_vendor.o(($event) => jumpDetails(item.id))
          });
        }),
        w: common_vendor.unref(integrals).length == 0 && dataFlag.value
      }, common_vendor.unref(integrals).length == 0 && dataFlag.value ? {
        x: common_assets._imports_0$2
      } : {}, {
        y: loading.value
      }, loading.value ? {
        z: common_vendor.t(loadingText.value)
      } : {});
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-1c7b6c44"]]);
wx.createPage(MiniProgramPage);
