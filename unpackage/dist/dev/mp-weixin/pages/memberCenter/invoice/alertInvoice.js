"use strict";
const common_vendor = require("../../../common/vendor.js");
const store_order = require("../../../store/order.js");
const api_shopInvoice = require("../../../api/shopInvoice.js");
if (!Array) {
  const _component_HeadNav = common_vendor.resolveComponent("HeadNav");
  _component_HeadNav();
}
const _sfc_main = {
  __name: "alertInvoice",
  setup(__props) {
    const state = common_vendor.ref("1");
    const comOrInd = common_vendor.ref("1");
    const flagTitle = common_vendor.ref(false);
    const flagNum = common_vendor.ref(false);
    const flagAddress = common_vendor.ref(false);
    const flagPhone = common_vendor.ref(false);
    const flagBank = common_vendor.ref(false);
    const flagCode = common_vendor.ref(false);
    const flagTitleValue = common_vendor.ref("请输入发票抬头");
    let shopInvoice = common_vendor.ref({
      id: "",
      invoiceTitle: "",
      //发票抬头
      invoiceNum: "",
      //发票识别码
      invoiceAddress: "",
      //发票地址
      invoicePhone: "",
      //发票手机号
      invoiceBank: "",
      //发票银行
      invoiceCode: ""
      //发票银行账号
    });
    let personal = common_vendor.ref({
      id: "",
      invoiceTitle: "",
      //发票抬头
      invoiceNum: "",
      //发票识别码
      invoiceAddress: "",
      //发票地址
      invoicePhone: "",
      //发票手机号
      invoiceBank: "",
      //发票银行
      invoiceCode: ""
      //发票银行账号
    });
    let shopInvoice1 = common_vendor.ref({
      id: "",
      invoiceTitle: "",
      //发票抬头
      invoiceNum: "",
      //发票识别码
      invoiceAddress: "",
      //发票地址
      invoicePhone: "",
      //发票手机号
      invoiceBank: "",
      //发票银行
      invoiceCode: ""
      //发票银行账号
    });
    let shopInvoice2 = common_vendor.ref({
      id: "",
      invoiceTitle: "",
      //发票抬头
      invoiceNum: "",
      //发票识别码
      invoiceAddress: "",
      //发票地址
      invoicePhone: "",
      //发票手机号
      invoiceBank: "",
      //发票银行
      invoiceCode: ""
      //发票银行账号
    });
    const invoice = common_vendor.ref("");
    function InvoiceType(val) {
      if (state.value != val) {
        if (val == 1) {
          shopInvoice.value = {
            ...shopInvoice1.value
          };
          comOrInd.value = 1;
        } else {
          shopInvoice.value = {
            ...shopInvoice2.value
          };
          comOrInd.value = 1;
        }
        state.value = val;
        rules();
      }
    }
    function ComOrInd(comOrInds) {
      if (comOrInds != comOrInd.value) {
        if (comOrInds == 1) {
          rules();
          shopInvoice.value = {
            ...shopInvoice1.value
          };
          comOrInd.value = comOrInds;
        } else {
          rules();
          shopInvoice.value = {
            ...personal.value
          };
          comOrInd.value = comOrInds;
        }
      }
    }
    function rules() {
      flagTitle.value = false;
      flagNum.value = false;
      flagAddress.value = false;
      flagPhone.value = false;
      flagBank.value = false;
      flagCode.value = false;
    }
    function getDetails() {
      api_shopInvoice.shopInvoiceList().then((res) => {
        if (res.data.data.ordersInvoice1) {
          shopInvoice1.value = res.data.data.ordersInvoice1;
          shopInvoice.value = res.data.data.ordersInvoice1;
        }
        if (res.data.data.ordersInvoice2) {
          shopInvoice2.value = res.data.data.ordersInvoice2;
        }
      });
    }
    function cancleInvoice() {
      if (invoice.value == "invoice") {
        store_order.order().setInvoice({
          invoiceStatus: 0,
          invoiceId: "",
          invoiceTitle: ""
        });
        common_vendor.index.redirectTo({
          url: "/pages/shoppingCart/confirmOrder"
        });
      } else if (invoice.value == "jifeninvoice") {
        store_order.order().setInvoice({
          invoiceStatus: 0,
          invoiceId: "",
          invoiceTitle: ""
        });
        common_vendor.index.redirectTo({
          url: "/pages/memberCenter/integralPage/confirmOrder"
        });
      }
    }
    function saveInvoice() {
      if (comOrInd.value == 1) {
        if (state.value == 2) {
          if (shopInvoice.value.invoiceTitle == "") {
            flagTitleValue.value = "请输入发票抬头";
            flagTitle.value = true;
          } else if (shopInvoice.value.invoiceTitle.length < 2 || shopInvoice.value.invoiceTitle.length > 50) {
            flagTitleValue.value = "发票抬头最多50个字符，最少2个字符";
            flagTitle.value = true;
          } else {
            flagTitle.value = false;
          }
          shopInvoice.value.invoiceNum == "" ? flagNum.value = true : flagNum.value = false, shopInvoice.value.invoiceAddress == "" ? flagAddress.value = true : flagAddress.value = false, shopInvoice.value.invoicePhone == "" ? flagPhone.value = true : flagPhone.value = false, shopInvoice.value.invoiceBank == "" ? flagBank.value = true : flagBank.value = false, shopInvoice.value.invoiceCode == "" ? flagCode.value = true : flagCode.value = false;
          if (shopInvoice.value.invoiceTitle != "" && shopInvoice.value.invoiceNum != "" && shopInvoice.value.invoiceAddress != "" && shopInvoice.value.invoicePhone != "" && shopInvoice.value.invoiceBank != "" && shopInvoice.value.invoiceCode != "") {
            shopInvoice.value.state = state.value;
            api_shopInvoice.shopInvoiceCreateOrUpdate(
              shopInvoice.value
            ).then((res) => {
              common_vendor.index.showToast({
                icon: "none",
                duration: 3e3,
                title: `保存成功`
              });
              if (invoice.value == "invoice") {
                store_order.order().setInvoice({
                  invoiceStatus: 3,
                  invoiceId: res.data.data,
                  invoiceTitle: shopInvoice.value.invoiceTitle
                });
                common_vendor.index.redirectTo({
                  url: "/pages/shoppingCart/confirmOrder"
                });
              } else if (invoice.value == "jifeninvoice") {
                store_order.order().setInvoice({
                  invoiceStatus: 3,
                  invoiceId: res.data.data,
                  invoiceTitle: shopInvoice.value.invoiceTitle
                });
                common_vendor.index.redirectTo({
                  url: "/pages/memberCenter/integralPage/confirmOrder"
                });
              }
              console.log(res);
            });
          }
        } else if (state.value == 1) {
          if (shopInvoice.value.invoiceTitle == "") {
            flagTitleValue.value = "请输入发票抬头";
            flagTitle.value = true;
          } else if (shopInvoice.value.invoiceTitle.length < 2 || shopInvoice.value.invoiceTitle.length > 50) {
            flagTitleValue.value = "发票抬头最多50个字符，最少2个字符";
            flagTitle.value = true;
          } else {
            flagTitle.value = false;
          }
          shopInvoice.value.invoiceNum == "" ? flagNum.value = true : flagNum.value = false;
          if (shopInvoice.value.invoiceTitle != "" && shopInvoice.value.invoiceNum != "") {
            shopInvoice.value.state = state.value;
            api_shopInvoice.shopInvoiceCreateOrUpdate(
              shopInvoice.value
            ).then((res) => {
              common_vendor.index.showToast({
                icon: "none",
                duration: 3e3,
                title: `保存成功`
              });
              console.log(res);
              if (invoice.value == "invoice") {
                store_order.order().setInvoice({
                  invoiceStatus: 1,
                  invoiceId: res.data.data,
                  invoiceTitle: shopInvoice.value.invoiceTitle
                });
                common_vendor.index.redirectTo({
                  url: "/pages/shoppingCart/confirmOrder"
                });
              } else if (invoice.value == "jifeninvoice") {
                store_order.order().setInvoice({
                  invoiceStatus: 1,
                  invoiceId: res.data.data,
                  invoiceTitle: shopInvoice.value.invoiceTitle
                });
                common_vendor.index.redirectTo({
                  url: "/pages/memberCenter/integralPage/confirmOrder"
                });
              }
            });
          }
        }
      } else {
        shopInvoice.value.invoiceTitle == "" ? flagTitle.value = true : flagTitle.value = false;
        if (shopInvoice.value.invoiceTitle != "") {
          if (invoice.value == "invoice") {
            store_order.order().setInvoice({
              invoiceStatus: 2,
              invoiceId: "",
              invoiceTitle: shopInvoice.value.invoiceTitle
            });
            common_vendor.index.redirectTo({
              url: "/pages/shoppingCart/confirmOrder"
            });
          } else if (invoice.value == "jifeninvoice") {
            store_order.order().setInvoice({
              invoiceStatus: 2,
              invoiceId: "",
              invoiceTitle: shopInvoice.value.invoiceTitle
            });
            common_vendor.index.redirectTo({
              url: "/pages/memberCenter/integralPage/confirmOrder"
            });
          }
        }
      }
    }
    common_vendor.onLoad((options) => {
      if (options.invoice) {
        invoice.value = options.invoice;
      }
      getDetails();
    });
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: common_vendor.p({
          ["nav-background-color"]: "#f7f7f5",
          backImageUrl: "1",
          textContent: "发票中心",
          fontSize: "15",
          fontWeight: "600"
        }),
        b: common_vendor.o(($event) => InvoiceType(1)),
        c: common_vendor.s({
          color: state.value == 1 ? "#1C9B64" : "#999999"
        }),
        d: common_vendor.s({
          borderColor: state.value == 1 ? "#1C9B64" : "#999999"
        }),
        e: common_vendor.s({
          backgroundColor: state.value == 1 ? "#1C9B6444" : "#ffffff00"
        }),
        f: common_vendor.o(($event) => InvoiceType(2)),
        g: common_vendor.s({
          color: state.value == 2 ? "#1C9B64" : "#999999"
        }),
        h: common_vendor.s({
          borderColor: state.value == 2 ? "#1C9B64" : "#999999"
        }),
        i: common_vendor.s({
          backgroundColor: state.value == 2 ? "#1C9B6444" : "#ffffff00"
        }),
        j: common_vendor.o(($event) => ComOrInd("1")),
        k: common_vendor.s({
          color: comOrInd.value == 1 ? "#1C9B64" : "#999999"
        }),
        l: common_vendor.s({
          borderColor: comOrInd.value == 1 ? "#1C9B64" : "#999999"
        }),
        m: common_vendor.s({
          backgroundColor: comOrInd.value == 1 ? "#1C9B6444" : "#ffffff00"
        }),
        n: state.value == 1 && (invoice.value == "invoice" || invoice.value == "jifeninvoice")
      }, state.value == 1 && (invoice.value == "invoice" || invoice.value == "jifeninvoice") ? {
        o: common_vendor.o(($event) => ComOrInd("2")),
        p: common_vendor.s({
          color: comOrInd.value == 2 ? "#1C9B64" : "#999999"
        }),
        q: common_vendor.s({
          borderColor: comOrInd.value == 2 ? "#1C9B64" : "#999999"
        }),
        r: common_vendor.s({
          backgroundColor: comOrInd.value == 2 ? "#1C9B6444" : "#ffffff00"
        })
      } : {}, {
        s: common_vendor.unref(shopInvoice).invoiceTitle,
        t: common_vendor.o(($event) => common_vendor.unref(shopInvoice).invoiceTitle = $event.detail.value),
        v: flagTitle.value
      }, flagTitle.value ? {
        w: common_vendor.t(flagTitleValue.value)
      } : {}, {
        x: comOrInd.value == 1
      }, comOrInd.value == 1 ? common_vendor.e({
        y: common_vendor.unref(shopInvoice).invoiceNum,
        z: common_vendor.o(($event) => common_vendor.unref(shopInvoice).invoiceNum = $event.detail.value),
        A: flagNum.value
      }, flagNum.value ? {} : {}) : {}, {
        B: comOrInd.value == 1
      }, comOrInd.value == 1 ? common_vendor.e({
        C: common_vendor.unref(shopInvoice).invoiceAddress,
        D: common_vendor.o(($event) => common_vendor.unref(shopInvoice).invoiceAddress = $event.detail.value),
        E: flagAddress.value
      }, flagAddress.value ? {} : {}) : {}, {
        F: comOrInd.value == 1
      }, comOrInd.value == 1 ? common_vendor.e({
        G: common_vendor.unref(shopInvoice).invoicePhone,
        H: common_vendor.o(($event) => common_vendor.unref(shopInvoice).invoicePhone = $event.detail.value),
        I: flagPhone.value
      }, flagPhone.value ? {} : {}) : {}, {
        J: comOrInd.value == 1
      }, comOrInd.value == 1 ? common_vendor.e({
        K: common_vendor.unref(shopInvoice).invoiceBank,
        L: common_vendor.o(($event) => common_vendor.unref(shopInvoice).invoiceBank = $event.detail.value),
        M: flagBank.value
      }, flagBank.value ? {} : {}) : {}, {
        N: comOrInd.value == 1
      }, comOrInd.value == 1 ? common_vendor.e({
        O: common_vendor.unref(shopInvoice).invoiceCode,
        P: common_vendor.o(($event) => common_vendor.unref(shopInvoice).invoiceCode = $event.detail.value),
        Q: flagCode.value
      }, flagCode.value ? {} : {}) : {}, {
        R: common_vendor.o(saveInvoice),
        S: invoice.value == "invoice" || invoice.value == "jifeninvoice"
      }, invoice.value == "invoice" || invoice.value == "jifeninvoice" ? {
        T: common_vendor.o(cancleInvoice)
      } : {});
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-0221d5c8"]]);
wx.createPage(MiniProgramPage);
