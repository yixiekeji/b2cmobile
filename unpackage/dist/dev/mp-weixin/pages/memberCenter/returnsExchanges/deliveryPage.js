"use strict";
const common_vendor = require("../../../common/vendor.js");
const api_order = require("../../../api/order.js");
if (!Array) {
  const _component_HeadNav = common_vendor.resolveComponent("HeadNav");
  const _easycom_uni_data_select2 = common_vendor.resolveComponent("uni-data-select");
  const _easycom_uni_forms_item2 = common_vendor.resolveComponent("uni-forms-item");
  const _easycom_uni_easyinput2 = common_vendor.resolveComponent("uni-easyinput");
  const _easycom_uni_forms2 = common_vendor.resolveComponent("uni-forms");
  (_component_HeadNav + _easycom_uni_data_select2 + _easycom_uni_forms_item2 + _easycom_uni_easyinput2 + _easycom_uni_forms2)();
}
const _easycom_uni_data_select = () => "../../../uni_modules/uni-data-select/components/uni-data-select/uni-data-select.js";
const _easycom_uni_forms_item = () => "../../../uni_modules/uni-forms/components/uni-forms-item/uni-forms-item.js";
const _easycom_uni_easyinput = () => "../../../uni_modules/uni-easyinput/components/uni-easyinput/uni-easyinput.js";
const _easycom_uni_forms = () => "../../../uni_modules/uni-forms/components/uni-forms/uni-forms.js";
if (!Math) {
  (_easycom_uni_data_select + _easycom_uni_forms_item + _easycom_uni_easyinput + _easycom_uni_forms)();
}
const _sfc_main = {
  __name: "deliveryPage",
  setup(__props) {
    const flag = common_vendor.ref("");
    const styled = common_vendor.reactive({
      color: "#333",
      backgroundColor: "#fff",
      disableColor: "#F7F6F6",
      borderColor: "rgb(229, 229, 229)",
      height: "40px",
      borderRadius: "12px",
      fontSize: "12px"
    });
    common_vendor.reactive({
      fontSize: "12px",
      color: "#555"
    });
    const form = common_vendor.ref();
    let formData = common_vendor.ref({
      logisticsNumber: "",
      logisticsId: "",
      deliverName: "",
      id: ""
    });
    let companyList = common_vendor.ref([]);
    const id = common_vendor.ref("");
    let rules = common_vendor.ref({
      logisticsId: {
        rules: [{
          required: true,
          errorMessage: "物流公司必选"
        }]
      },
      logisticsNumber: {
        rules: [{
          required: true,
          errorMessage: "快递单号必填"
        }]
      }
    });
    common_vendor.ref({
      "borderColor": "#fff",
      "backgroundColor": "#fff",
      "height": "96rpx",
      "borderRadius": "14rpx"
    });
    let memberProductBack = common_vendor.ref();
    function getBackList() {
      if (flag.value == "back") {
        api_order.backDeliverGoods({
          memberProductBackId: id.value
        }).then((res) => {
          console.log(res);
          companyList.value = [];
          res.data.data.courierCompanyList.forEach((item) => {
            companyList.value.push({
              value: item.id,
              text: item.companyName
            });
          });
          memberProductBack.value = res.data.data.memberProductBack;
        });
      } else if (flag.value == "exchange") {
        api_order.exchangeDeliverGoods({
          memberProductExchangeId: id.value
        }).then((res) => {
          console.log(res);
          companyList.value = [];
          res.data.data.courierCompanyList.forEach((item) => {
            companyList.value.push({
              value: item.id,
              text: item.companyName
            });
          });
          memberProductBack.value = res.data.data.memberProductExchange;
        });
      }
    }
    function commit() {
      form.value.validate().then((res) => {
        if (flag.value == "back") {
          api_order.doBackDeliverGoods({
            logisticsId: formData.value.logisticsId,
            logisticsNumber: formData.value.logisticsNumber,
            memberProductBackId: id.value
          }).then((res2) => {
            if (res2.data.success) {
              common_vendor.index.showToast({
                title: "操作成功",
                duration: 2e3,
                icon: "none"
              });
              common_vendor.index.navigateBack({
                delta: 1
              });
            }
          });
        } else if (flag.value == "exchange") {
          api_order.doExchangeDeliverGoods({
            logisticsId: formData.value.logisticsId,
            logisticsNumber: formData.value.logisticsNumber,
            memberProductExchangeId: id.value
          }).then((res2) => {
            if (res2.data.success) {
              common_vendor.index.showToast({
                title: "操作成功",
                duration: 2e3,
                icon: "none"
              });
              common_vendor.index.navigateBack({
                delta: 1
              });
            }
          });
        }
      }).catch((err) => {
      });
    }
    common_vendor.onLoad((options) => {
      memberProductBack.value = [];
      id.value = options.id;
      flag.value = options.flag;
      getBackList();
    });
    return (_ctx, _cache) => {
      return {
        a: common_vendor.p({
          ["nav-background-color"]: "#f7f7f5",
          backImageUrl: "1",
          textContent: "发货",
          fontSize: "15",
          fontWeight: "600"
        }),
        b: _ctx.imageUrl + common_vendor.unref(memberProductBack).productImg,
        c: common_vendor.t(common_vendor.unref(memberProductBack).productName),
        d: common_vendor.t(common_vendor.unref(memberProductBack).createTime),
        e: common_vendor.o(_ctx.change),
        f: common_vendor.o(($event) => common_vendor.unref(formData).logisticsId = $event),
        g: common_vendor.p({
          localdata: common_vendor.unref(companyList),
          modelValue: common_vendor.unref(formData).logisticsId
        }),
        h: common_vendor.p({
          label: "快递公司",
          name: "logisticsId",
          required: true
        }),
        i: common_vendor.o(($event) => common_vendor.unref(formData).logisticsNumber = $event),
        j: common_vendor.p({
          type: "text",
          placeholder: "请输入快递单号",
          styles: styled,
          modelValue: common_vendor.unref(formData).logisticsNumber
        }),
        k: common_vendor.p({
          label: "快递单号",
          name: "logisticsNumber",
          required: true
        }),
        l: common_vendor.sr(form, "9d33cab7-1", {
          "k": "form"
        }),
        m: common_vendor.p({
          rules: common_vendor.unref(rules),
          modelValue: common_vendor.unref(formData),
          ["label-width"]: "73px"
        }),
        n: common_vendor.o(commit)
      };
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-9d33cab7"]]);
wx.createPage(MiniProgramPage);
