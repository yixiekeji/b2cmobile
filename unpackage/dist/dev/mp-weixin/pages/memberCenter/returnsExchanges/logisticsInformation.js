"use strict";
const common_vendor = require("../../../common/vendor.js");
const api_order = require("../../../api/order.js");
if (!Array) {
  const _component_HeadNav = common_vendor.resolveComponent("HeadNav");
  _component_HeadNav();
}
const _sfc_main = {
  __name: "logisticsInformation",
  setup(__props) {
    const id = common_vendor.ref("");
    let memberProductBack = common_vendor.ref({});
    let memberProductBackLogList = common_vendor.ref([]);
    const flag = common_vendor.ref("");
    function getList() {
      if (flag.value == "back") {
        api_order.lookBackLogistics({
          backid: id.value
        }).then((res) => {
          memberProductBack.value = res.data.data.memberProductBack;
          memberProductBackLogList.value = res.data.data.memberProductBackLogList;
          console.log(res);
        });
      } else if (flag.value == "exchange") {
        api_order.lookExchangeLogistics({
          exchangeid: id.value
        }).then((res) => {
          memberProductBack.value = res.data.data.memberProductBack;
          memberProductBackLogList.value = res.data.data.memberProductExchangeLogList;
          console.log(res);
        });
      }
    }
    common_vendor.onLoad((options) => {
      id.value = options.id;
      flag.value = options.flag;
      console.log(options.flag);
      getList();
    });
    return (_ctx, _cache) => {
      return {
        a: common_vendor.p({
          ["nav-background-color"]: "#f7f7f5",
          backImageUrl: "1",
          textContent: "物流信息",
          fontSize: "15",
          fontWeight: "600"
        }),
        b: common_vendor.f(common_vendor.unref(memberProductBackLogList), (item, k0, i0) => {
          return {
            a: common_vendor.t(item.createTime),
            b: common_vendor.t(item.content)
          };
        })
      };
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-5258637c"]]);
wx.createPage(MiniProgramPage);
