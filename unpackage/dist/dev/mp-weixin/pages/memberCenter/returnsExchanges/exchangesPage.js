"use strict";
const common_vendor = require("../../../common/vendor.js");
const common_assets = require("../../../common/assets.js");
const api_order = require("../../../api/order.js");
if (!Array) {
  const _component_HeadNav = common_vendor.resolveComponent("HeadNav");
  _component_HeadNav();
}
const _sfc_main = {
  __name: "exchangesPage",
  setup(__props) {
    const flag = common_vendor.ref(false);
    common_vendor.ref({
      num: "",
      de: "2",
      mode: "1"
    });
    common_vendor.ref("");
    common_vendor.ref({
      num: {
        num: [
          {
            required: true,
            errorMessage: "数量必填"
          },
          {
            validateFunction: function(rule, value, data, callback) {
              if (Number(value) > 100) {
                callback("数量超出");
              }
              return true;
            }
          }
        ]
      },
      de: {
        rules: [
          {
            required: true,
            errorMessage: "问题描述必填"
          },
          {
            minLength: 10,
            errorMessage: "至少需要10字符"
          },
          {
            maxLength: 100,
            errorMessage: "问题描述字符长度超出"
          }
        ]
      }
    });
    common_vendor.ref({
      "borderColor": "#fff",
      "backgroundColor": "#fff",
      "height": "96rpx",
      "borderRadius": "14rpx"
    });
    common_vendor.ref("0");
    const page = common_vendor.ref(1);
    const loading = common_vendor.ref(false);
    const loadingText = common_vendor.ref("加载中...");
    let list = common_vendor.ref([]);
    const dataFlag = common_vendor.ref(false);
    function getBackList() {
      dataFlag.value = false;
      api_order.exchange({
        page: page.value
      }).then((res) => {
        console.log(res);
        if (res.data.data.memberProductExchangeList.length == 0 && page.value > 1 && list.value.length > 10) {
          loadingText.value = "到底啦~";
        } else {
          list.value = [...list.value, ...res.data.data.memberProductExchangeList];
          loading.value = false;
        }
        dataFlag.value = true;
      });
    }
    function details(val) {
      if (val == flag.value) {
        flag.value = "";
      } else {
        flag.value = val;
      }
    }
    function logisticsDetails(val) {
      common_vendor.index.navigateTo({
        url: "/pages/memberCenter/returnsExchanges/logisticsInformation?id=" + val + "&flag=exchange"
      });
    }
    function delivery(val) {
      common_vendor.index.navigateTo({
        url: "/pages/memberCenter/returnsExchanges/deliveryPage?id=" + val + "&flag=exchange"
      });
    }
    function ex(html) {
      let newContent = html.replace(/<img[^>]*>/gi, function(match, capture) {
        match = match.replace(/style="[^"]+"/gi, "").replace(/style='[^']+'/gi, "");
        match = match.replace(/width="[^"]+"/gi, "").replace(/width='[^']+'/gi, "");
        match = match.replace(/height="[^"]+"/gi, "").replace(/height='[^']+'/gi, "");
        return match;
      });
      newContent = newContent.replace(/&lt;/gi, "<").replace(/&gt;/gi, ">");
      newContent = newContent.replace(/style="[^"]+"/gi, function(match, capture) {
        match = match.replace(/width:[^;]+;/gi, "width:100%;").replace(/width:[^;]+;/gi, "width:100%;");
        return match;
      });
      newContent = newContent.replace(/<br[^>]*\/>/gi, "");
      newContent = newContent.replace(/\<img/gi, '<img style="max-width:100%;height:auto;display:block;"');
      return newContent;
    }
    common_vendor.onShow(() => {
      list.value = [];
      page.value = 1;
      getBackList();
    });
    common_vendor.onReachBottom(() => {
      if (list.value.length >= 20 * page.value) {
        page.value += 1;
        loading.value = true;
        loadingText.value = "加载中...";
        setTimeout(() => {
          getBackList();
        }, 800);
      } else {
        loading.value = true;
        loadingText.value = "到底啦~";
      }
    });
    common_vendor.onPullDownRefresh(() => {
      setTimeout(() => {
        common_vendor.index.stopPullDownRefresh();
        list.value = [];
        loading.value = false;
        page.value = 1;
        getBackList();
      }, 800);
    });
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: common_vendor.p({
          ["nav-background-color"]: "#f7f7f5",
          backImageUrl: "1",
          textContent: "换货",
          fontSize: "15",
          fontWeight: "600"
        }),
        b: common_vendor.f(common_vendor.unref(list), (item, k0, i0) => {
          return common_vendor.e({
            a: common_vendor.t(item.ordersSn),
            b: item.state == 1
          }, item.state == 1 ? {} : {}, {
            c: item.state == 2
          }, item.state == 2 ? {} : {}, {
            d: item.state == 3
          }, item.state == 3 ? {} : {}, {
            e: item.state == 4
          }, item.state == 4 ? {} : {}, {
            f: item.state == 5
          }, item.state == 5 ? {} : {}, {
            g: item.state == 6
          }, item.state == 6 ? {} : {}, {
            h: item.state == 7
          }, item.state == 7 ? {} : {}, {
            i: item.state == 8
          }, item.state == 8 ? {} : {}, {
            j: common_vendor.t(item.createTime),
            k: _ctx.imageUrl + item.productImg,
            l: common_vendor.t(item.productName),
            m: common_vendor.t(item.createTime),
            n: common_vendor.t(flag.value == item.id ? "关闭" : "查看"),
            o: common_vendor.o(($event) => details(item.id)),
            p: item.state != 1 && item.state != 2 && item.state != 7
          }, item.state != 1 && item.state != 2 && item.state != 7 ? {
            q: common_vendor.o(($event) => logisticsDetails(item.id))
          } : {}, {
            r: item.state == 2
          }, item.state == 2 ? {
            s: common_vendor.o(($event) => delivery(item.id))
          } : {}, {
            t: ex(item.question),
            v: common_vendor.t(item.number),
            w: item.state == 1
          }, item.state == 1 ? {} : {}, {
            x: item.state == 2
          }, item.state == 2 ? {} : {}, {
            y: item.state == 3
          }, item.state == 3 ? {} : {}, {
            z: item.state == 4
          }, item.state == 4 ? {} : {}, {
            A: item.state == 5
          }, item.state == 5 ? {} : {}, {
            B: item.state == 6
          }, item.state == 6 ? {} : {}, {
            C: item.state == 7
          }, item.state == 7 ? {} : {}, {
            D: item.state == 8
          }, item.state == 8 ? {} : {}, {
            E: common_vendor.t(item.addressAll),
            F: common_vendor.t(item.addressInfo),
            G: common_vendor.t(item.changeName),
            H: common_vendor.t(item.phone),
            I: common_vendor.t(item.logisticsName ? item.logisticsName : "-"),
            J: common_vendor.t(item.logisticsNumber ? item.logisticsNumber : "-"),
            K: common_vendor.t(item.addressAll2 ? item.addressAll2 : "-"),
            L: common_vendor.t(item.addressInfo2),
            M: common_vendor.t(item.changeName2 ? item.changeName2 : "-"),
            N: common_vendor.t(item.phone2 ? item.phone2 : "-"),
            O: common_vendor.t(item.logisticsName2 ? item.logisticsName2 : "-"),
            P: common_vendor.t(item.logisticsNumber2 ? item.logisticsNumber2 : "-"),
            Q: common_vendor.t(item.remark ? item.remark : "-"),
            R: common_vendor.n(flag.value == item.id ? "popAct" : "pop")
          });
        }),
        c: common_vendor.unref(list).length == 0 && dataFlag.value
      }, common_vendor.unref(list).length == 0 && dataFlag.value ? {
        d: common_assets._imports_0$2
      } : {}, {
        e: loading.value
      }, loading.value ? {
        f: common_vendor.t(loadingText.value)
      } : {});
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-75ceac5a"]]);
wx.createPage(MiniProgramPage);
