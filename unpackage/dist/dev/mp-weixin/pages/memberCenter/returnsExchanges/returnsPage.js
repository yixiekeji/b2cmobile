"use strict";
const common_vendor = require("../../../common/vendor.js");
const common_assets = require("../../../common/assets.js");
const api_order = require("../../../api/order.js");
if (!Array) {
  const _component_HeadNav = common_vendor.resolveComponent("HeadNav");
  const _easycom_uni_popup_dialog2 = common_vendor.resolveComponent("uni-popup-dialog");
  const _easycom_uni_popup2 = common_vendor.resolveComponent("uni-popup");
  (_component_HeadNav + _easycom_uni_popup_dialog2 + _easycom_uni_popup2)();
}
const _easycom_uni_popup_dialog = () => "../../../uni_modules/uni-popup/components/uni-popup-dialog/uni-popup-dialog.js";
const _easycom_uni_popup = () => "../../../uni_modules/uni-popup/components/uni-popup/uni-popup.js";
if (!Math) {
  (_easycom_uni_popup_dialog + _easycom_uni_popup)();
}
const _sfc_main = {
  __name: "returnsPage",
  setup(__props) {
    const flag = common_vendor.ref(false);
    common_vendor.ref({
      num: "",
      de: "2",
      mode: "1"
    });
    common_vendor.ref("");
    common_vendor.ref({
      "borderColor": "#fff",
      "backgroundColor": "#fff",
      "height": "96rpx",
      "borderRadius": "14rpx"
    });
    let tabs = common_vendor.ref([
      {
        id: 1,
        name: "退货"
      },
      {
        id: 2,
        name: "换货"
      }
    ]);
    const currentTab = common_vendor.ref(0);
    const tabCurrent = common_vendor.ref("tabNum1");
    common_vendor.ref(0);
    const top = common_vendor.ref(0);
    const page = common_vendor.ref(1);
    const loading = common_vendor.ref(false);
    const loadingText = common_vendor.ref("加载中...");
    let list = common_vendor.ref([]);
    const dataFlag = common_vendor.ref(false);
    function swichMenu(val) {
      list.value = [];
      page.value = 1;
      currentTab.value = val;
      tabCurrent.value = "tabNum" + val;
      if (val == "0") {
        getBackList();
      } else {
        getExchangeList();
      }
    }
    function getBackList() {
      dataFlag.value = false;
      api_order.back({
        page: page.value
      }).then((res) => {
        console.log(res);
        if (res.data.data.memberProductBackList.length == 0 && page.value > 1 && list.value.length > 10) {
          loadingText.value = "到底啦~";
        } else {
          list.value = [...list.value, ...res.data.data.memberProductBackList];
          loading.value = false;
        }
        dataFlag.value = true;
      });
    }
    function getExchangeList() {
      dataFlag.value = false;
      api_order.exchange({
        page: page.value
      }).then((res) => {
        console.log(res);
        if (res.data.data.memberProductExchangeList.length == 0 && page.value > 1 && list.value.length > 10) {
          loadingText.value = "到底啦~";
        } else {
          list.value = [...list.value, ...res.data.data.memberProductExchangeList];
          loading.value = false;
        }
        dataFlag.value = true;
      });
    }
    function details(val) {
      if (val == flag.value) {
        flag.value = "";
      } else {
        flag.value = val;
      }
    }
    function logisticsDetails(val) {
      if (currentTab.value == "0") {
        common_vendor.index.navigateTo({
          url: "/pages/memberCenter/returnsExchanges/logisticsInformation?id=" + val + "&flag=back"
        });
      } else {
        common_vendor.index.navigateTo({
          url: "/pages/memberCenter/returnsExchanges/logisticsInformation?id=" + val + "&flag=exchange"
        });
      }
    }
    function delivery(val) {
      if (currentTab.value == "0") {
        common_vendor.index.navigateTo({
          url: "/pages/memberCenter/returnsExchanges/deliveryPage?id=" + val + "&flag=back"
        });
      } else {
        common_vendor.index.navigateTo({
          url: "/pages/memberCenter/returnsExchanges/deliveryPage?id=" + val + "&flag=exchange"
        });
      }
    }
    const alertDialogCfm = common_vendor.ref();
    const idCfm = common_vendor.ref("");
    function confirm(val) {
      alertDialogCfm.value.open();
      idCfm.value = val;
    }
    function dialogConfirmCfm() {
      api_order.doReceiveExchangeGoods({
        exchangeId: idCfm.value
      }).then((res) => {
        if (res.data.success) {
          page.value = 1;
          list.value = [];
          getExchangeList();
          common_vendor.index.showToast({
            icon: "none",
            duration: 3e3,
            title: `操作成功`
          });
        }
      });
    }
    function dialogCloseCfm() {
      alertDialogCfm.value.close();
    }
    function ex(html) {
      let newContent = html.replace(/<img[^>]*>/gi, function(match, capture) {
        match = match.replace(/style="[^"]+"/gi, "").replace(/style='[^']+'/gi, "");
        match = match.replace(/width="[^"]+"/gi, "").replace(/width='[^']+'/gi, "");
        match = match.replace(/height="[^"]+"/gi, "").replace(/height='[^']+'/gi, "");
        return match;
      });
      newContent = newContent.replace(/&lt;/gi, "<").replace(/&gt;/gi, ">");
      newContent = newContent.replace(/style="[^"]+"/gi, function(match, capture) {
        match = match.replace(/width:[^;]+;/gi, "width:100%;").replace(/width:[^;]+;/gi, "width:100%;");
        return match;
      });
      newContent = newContent.replace(/<br[^>]*\/>/gi, "");
      newContent = newContent.replace(/\<img/gi, '<img style="max-width:100%;height:auto;display:block;"');
      return newContent;
    }
    common_vendor.onShow(() => {
      list.value = [];
      page.value = 1;
      currentTab.value = 0;
      getBackList();
    });
    common_vendor.onReachBottom(() => {
      if (list.value.length >= 20 * page.value) {
        page.value += 1;
        loading.value = true;
        loadingText.value = "加载中...";
        setTimeout(() => {
          if (currentTab.value == "0") {
            getBackList();
          } else {
            getExchangeList();
          }
        }, 800);
      } else {
        loading.value = true;
        loadingText.value = "到底啦~";
      }
    });
    common_vendor.onPullDownRefresh(() => {
      setTimeout(() => {
        common_vendor.index.stopPullDownRefresh();
        list.value = [];
        loading.value = false;
        page.value = 1;
        if (currentTab.value == "0") {
          getBackList();
        } else {
          getExchangeList();
        }
      }, 800);
    });
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: common_vendor.p({
          ["nav-background-color"]: "#f7f7f5",
          backImageUrl: "1",
          textContent: "退换货",
          fontSize: "15",
          fontWeight: "600"
        }),
        b: common_vendor.f(common_vendor.unref(tabs), (item, k0, i0) => {
          return {
            a: common_vendor.t(item.name),
            b: common_vendor.n(currentTab.value == item.id - 1 ? "menu-topic-act" : "menu-topic"),
            c: "tabNum" + item.id,
            d: item.id - 1,
            e: common_vendor.o(($event) => swichMenu(item.id - 1), item.id - 1)
          };
        }),
        c: top.value,
        d: currentTab.value == "0"
      }, currentTab.value == "0" ? {
        e: common_vendor.f(common_vendor.unref(list), (item, k0, i0) => {
          return common_vendor.e({
            a: common_vendor.t(item.ordersSn),
            b: item.stateReturn == 1
          }, item.stateReturn == 1 ? {} : {}, {
            c: item.stateReturn == 2
          }, item.stateReturn == 2 ? {} : {}, {
            d: item.stateReturn == 3
          }, item.stateReturn == 3 ? {} : {}, {
            e: item.stateReturn == 4
          }, item.stateReturn == 4 ? {} : {}, {
            f: item.stateReturn == 5
          }, item.stateReturn == 5 ? {} : {}, {
            g: common_vendor.t(item.createTime),
            h: _ctx.imageUrl + item.productImg,
            i: common_vendor.t(item.productName),
            j: common_vendor.t(item.createTime),
            k: common_vendor.t(flag.value == item.id ? "关闭" : "查看"),
            l: common_vendor.o(($event) => details(item.id)),
            m: item.stateReturn != 1 && item.stateReturn != 2 && item.stateReturn != 5
          }, item.stateReturn != 1 && item.stateReturn != 2 && item.stateReturn != 5 ? {
            n: common_vendor.o(($event) => logisticsDetails(item.id))
          } : {}, {
            o: item.stateReturn == 2
          }, item.stateReturn == 2 ? {
            p: common_vendor.o(($event) => delivery(item.id))
          } : {}, {
            q: ex(item.question),
            r: common_vendor.t(item.number),
            s: common_vendor.t(item.backMoney),
            t: item.stateReturn == 1
          }, item.stateReturn == 1 ? {} : {}, {
            v: item.stateReturn == 2
          }, item.stateReturn == 2 ? {} : {}, {
            w: item.stateReturn == 3
          }, item.stateReturn == 3 ? {} : {}, {
            x: item.stateReturn == 4
          }, item.stateReturn == 4 ? {} : {}, {
            y: item.stateReturn == 5
          }, item.stateReturn == 5 ? {} : {}, {
            z: common_vendor.t(item.addressAll),
            A: common_vendor.t(item.addressInfo),
            B: common_vendor.t(item.contactPhone ? item.contactPhone : "-"),
            C: common_vendor.t(item.contactName ? item.contactName : "-"),
            D: common_vendor.t(item.remark ? item.remark : "-"),
            E: common_vendor.n(flag.value == item.id ? "popAct" : "pop")
          });
        })
      } : {}, {
        f: currentTab.value == "1"
      }, currentTab.value == "1" ? {
        g: common_vendor.f(common_vendor.unref(list), (item, k0, i0) => {
          return common_vendor.e({
            a: common_vendor.t(item.ordersSn),
            b: item.state == 1
          }, item.state == 1 ? {} : {}, {
            c: item.state == 2
          }, item.state == 2 ? {} : {}, {
            d: item.state == 3
          }, item.state == 3 ? {} : {}, {
            e: item.state == 4
          }, item.state == 4 ? {} : {}, {
            f: item.state == 5
          }, item.state == 5 ? {} : {}, {
            g: item.state == 6
          }, item.state == 6 ? {} : {}, {
            h: item.state == 7
          }, item.state == 7 ? {} : {}, {
            i: item.state == 8
          }, item.state == 8 ? {} : {}, {
            j: common_vendor.t(item.createTime),
            k: _ctx.imageUrl + item.productImg,
            l: common_vendor.t(item.productName),
            m: common_vendor.t(item.createTime),
            n: common_vendor.t(flag.value == item.id ? "关闭" : "查看"),
            o: common_vendor.o(($event) => details(item.id)),
            p: item.state != 1 && item.state != 2 && item.state != 7
          }, item.state != 1 && item.state != 2 && item.state != 7 ? {
            q: common_vendor.o(($event) => logisticsDetails(item.id))
          } : {}, {
            r: item.state == 2
          }, item.state == 2 ? {
            s: common_vendor.o(($event) => delivery(item.id))
          } : {}, {
            t: item.state == 5
          }, item.state == 5 ? {
            v: common_vendor.o(($event) => confirm(item.id))
          } : {}, {
            w: ex(item.question),
            x: common_vendor.t(item.number),
            y: item.state == 1
          }, item.state == 1 ? {} : {}, {
            z: item.state == 2
          }, item.state == 2 ? {} : {}, {
            A: item.state == 3
          }, item.state == 3 ? {} : {}, {
            B: item.state == 4
          }, item.state == 4 ? {} : {}, {
            C: item.state == 5
          }, item.state == 5 ? {} : {}, {
            D: item.state == 6
          }, item.state == 6 ? {} : {}, {
            E: item.state == 7
          }, item.state == 7 ? {} : {}, {
            F: item.state == 8
          }, item.state == 8 ? {} : {}, {
            G: common_vendor.t(item.addressAll),
            H: common_vendor.t(item.addressInfo),
            I: common_vendor.t(item.changeName),
            J: common_vendor.t(item.phone),
            K: common_vendor.t(item.logisticsName ? item.logisticsName : "-"),
            L: common_vendor.t(item.logisticsNumber ? item.logisticsNumber : "-"),
            M: common_vendor.t(item.addressAll2 ? item.addressAll2 : "-"),
            N: common_vendor.t(item.addressInfo2),
            O: common_vendor.t(item.changeName2 ? item.changeName2 : "-"),
            P: common_vendor.t(item.phone2 ? item.phone2 : "-"),
            Q: common_vendor.t(item.logisticsName2 ? item.logisticsName2 : "-"),
            R: common_vendor.t(item.logisticsNumber2 ? item.logisticsNumber2 : "-"),
            S: common_vendor.t(item.remark ? item.remark : "-"),
            T: common_vendor.n(flag.value == item.id ? "popActs" : "pop")
          });
        })
      } : {}, {
        h: common_vendor.unref(list).length == 0 && dataFlag.value
      }, common_vendor.unref(list).length == 0 && dataFlag.value ? {
        i: common_assets._imports_0$2
      } : {}, {
        j: loading.value
      }, loading.value ? {
        k: common_vendor.t(loadingText.value)
      } : {}, {
        l: common_vendor.o(dialogConfirmCfm),
        m: common_vendor.o(dialogCloseCfm),
        n: common_vendor.p({
          type: _ctx.msgType,
          cancelText: "取消",
          confirmText: "确认",
          content: "确认收货吗?"
        }),
        o: common_vendor.sr(alertDialogCfm, "32a448ac-1", {
          "k": "alertDialogCfm"
        }),
        p: common_vendor.p({
          type: "dialog"
        })
      });
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-32a448ac"]]);
wx.createPage(MiniProgramPage);
