"use strict";
const common_vendor = require("../../../common/vendor.js");
const common_assets = require("../../../common/assets.js");
const api_member = require("../../../api/member.js");
if (!Array) {
  const _component_HeadNav = common_vendor.resolveComponent("HeadNav");
  const _easycom_uni_list_item2 = common_vendor.resolveComponent("uni-list-item");
  const _easycom_uni_list2 = common_vendor.resolveComponent("uni-list");
  (_component_HeadNav + _easycom_uni_list_item2 + _easycom_uni_list2)();
}
const _easycom_uni_list_item = () => "../../../uni_modules/uni-list/components/uni-list-item/uni-list-item.js";
const _easycom_uni_list = () => "../../../uni_modules/uni-list/components/uni-list/uni-list.js";
if (!Math) {
  (_easycom_uni_list_item + _easycom_uni_list)();
}
const _sfc_main = {
  __name: "personalInformation",
  setup(__props) {
    let data = common_vendor.ref({});
    function getData() {
      api_member.memberInfo().then((res) => {
        data.value = res.data.data;
      });
    }
    function gender(data2) {
      if (data2 == 0) {
        return "保密";
      } else if (data2 == 1) {
        return "男";
      } else if (data2 == 2) {
        return "女";
      }
    }
    function jumpEdit(url) {
      common_vendor.index.navigateTo({
        url
      });
    }
    common_vendor.onShow(() => {
      getData();
    });
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: common_vendor.p({
          ["nav-background-color"]: "#fff",
          backImageUrl: "1",
          textContent: "个人资料",
          fontSize: "15",
          fontWeight: "600"
        }),
        b: common_vendor.unref(data).image.indexOf("http") != -1
      }, common_vendor.unref(data).image.indexOf("http") != -1 ? {
        c: common_vendor.o(($event) => jumpEdit("/pages/memberCenter/personalInformation/alterNikename?flag=headImg")),
        d: common_vendor.unref(data).image
      } : common_vendor.unref(data).image ? {
        f: common_vendor.o(($event) => jumpEdit("/pages/memberCenter/personalInformation/alterNikename?flag=headImg")),
        g: _ctx.imageUrl + common_vendor.unref(data).image
      } : {
        h: common_vendor.o(($event) => jumpEdit("/pages/memberCenter/personalInformation/alterNikename?flag=headImg")),
        i: common_assets._imports_0$5
      }, {
        e: common_vendor.unref(data).image,
        j: common_vendor.o(($event) => jumpEdit("/pages/memberCenter/alertPhoneNumber")),
        k: common_vendor.p({
          showArrow: true,
          clickable: true,
          title: "手机号",
          rightText: common_vendor.unref(data).phone,
          border: false
        }),
        l: common_vendor.o(($event) => jumpEdit("/pages/memberCenter/personalInformation/alterNikename?flag=email")),
        m: common_vendor.p({
          showArrow: true,
          clickable: true,
          title: "邮箱",
          rightText: common_vendor.unref(data).email ? common_vendor.unref(data).email : "",
          border: false
        }),
        n: common_vendor.o(($event) => jumpEdit("/pages/memberCenter/personalInformation/alterNikename?flag=name")),
        o: common_vendor.p({
          showArrow: true,
          clickable: true,
          title: "昵称",
          rightText: common_vendor.unref(data).name ? common_vendor.unref(data).name : "",
          border: false
        }),
        p: common_vendor.o(($event) => jumpEdit("/pages/memberCenter/personalInformation/alterNikename?flag=gender")),
        q: common_vendor.p({
          showArrow: true,
          clickable: true,
          title: "性别",
          rightText: gender(common_vendor.unref(data).gender),
          border: false
        }),
        r: common_vendor.o(($event) => jumpEdit("/pages/memberCenter/personalInformation/alterNikename?flag=birthday")),
        s: common_vendor.p({
          showArrow: true,
          clickable: true,
          title: "生日",
          rightText: common_vendor.unref(data).birthday ? common_vendor.unref(data).birthday : "",
          border: false
        }),
        t: common_vendor.o(($event) => jumpEdit("/pages/memberCenter/personalInformation/alterNikename?flag=addAll")),
        v: common_vendor.p({
          showArrow: true,
          clickable: true,
          title: "地区",
          rightText: common_vendor.unref(data).addAll ? common_vendor.unref(data).addAll : "",
          border: false
        }),
        w: common_vendor.p({
          border: false
        })
      });
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-f2b9feea"]]);
wx.createPage(MiniProgramPage);
