"use strict";
const common_vendor = require("../../../common/vendor.js");
const api_member = require("../../../api/member.js");
if (!Array) {
  const _component_HeadNav = common_vendor.resolveComponent("HeadNav");
  const _easycom_uni_datetime_picker2 = common_vendor.resolveComponent("uni-datetime-picker");
  const _easycom_pick_regions2 = common_vendor.resolveComponent("pick-regions");
  const _easycom_uni_file_picker2 = common_vendor.resolveComponent("uni-file-picker");
  (_component_HeadNav + _easycom_uni_datetime_picker2 + _easycom_pick_regions2 + _easycom_uni_file_picker2)();
}
const _easycom_uni_datetime_picker = () => "../../../uni_modules/uni-datetime-picker/components/uni-datetime-picker/uni-datetime-picker.js";
const _easycom_pick_regions = () => "../../../components/pick-regions/pick-regions.js";
const _easycom_uni_file_picker = () => "../../../uni_modules/uni-file-picker/components/uni-file-picker/uni-file-picker.js";
if (!Math) {
  (_easycom_uni_datetime_picker + _easycom_pick_regions + _easycom_uni_file_picker)();
}
const _sfc_main = {
  __name: "alterNikename",
  setup(__props) {
    const flag = common_vendor.ref("");
    let region = common_vendor.ref([]);
    common_vendor.ref(["广东省", "广州市", "番禺区"]);
    const defaultRegionCode = common_vendor.ref("440113");
    let array = common_vendor.ref(["保密", "男", "女"]);
    let formData = common_vendor.ref({
      name: "",
      email: "",
      birthday: "",
      gender: 0
    });
    const nameFlag = common_vendor.ref(false);
    const emailFlag = common_vendor.ref(false);
    const emailRegex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/;
    let imageValue = common_vendor.ref([]);
    const imgFlag = common_vendor.ref(false);
    const imageStyles = common_vendor.reactive({
      width: 90,
      height: 90
    });
    let upImg = common_vendor.ref("");
    let data = common_vendor.ref({});
    const index = common_vendor.ref("");
    const regionName = common_vendor.ref("");
    const regionId = common_vendor.ref("");
    function handleGetRegion(region2) {
      region2.value = region2;
      regionId.value = region2.value.map((item) => item.id).join("-");
      regionName.value = region2.value.map((item) => item.regionName).join(" ");
    }
    function bindPickerChange(e) {
      console.log(e.detail.value);
      formData.value.gender = Number(e.detail.value);
      console.log(formData.value.gender == "" && formData.value.gender != 0);
    }
    function getData() {
      api_member.memberInfo().then((res) => {
        console.log(res.data.data.name);
        res.data.data.name != void 0 ? formData.value.name = res.data.data.name : formData.value.name = "";
        res.data.data.email != void 0 ? formData.value.email = res.data.data.email : formData.value.email = "";
        res.data.data.birthday != void 0 ? formData.value.birthday = res.data.data.birthday : formData.value.birthday = "";
        res.data.data.gender != void 0 ? formData.value.gender = res.data.data.gender : formData.value.gender = "";
        res.data.data.addAll != void 0 ? getaddress(res.data.data) : "";
        data.value = res.data.data;
      });
    }
    function submit() {
      if (flag.value != "addAll" && flag.value != "headImg") {
        if (formData.value[flag.value] == "" && formData.value.gender != 0) {
          common_vendor.index.showToast({
            icon: "none",
            duration: 3e3,
            title: "请填写内容"
          });
          return;
        } else if (flag.value == "name" && (2 > formData.value[flag.value].length || 6 < formData.value[flag.value].length)) {
          nameFlag.value = true;
          return;
        } else if (flag.value == "email" && !emailRegex.test(formData.value[flag.value])) {
          emailFlag.value = true;
          return;
        }
        let obj = {};
        obj[flag.value] = formData.value[flag.value];
        api_member.memberUpdateInfo({
          type: flag.value,
          value: formData.value[flag.value]
        }).then((res) => {
          common_vendor.index.navigateBack({
            delta: 1
          });
        });
      } else if (flag.value == "headImg") {
        if (upImg.value == "") {
          imgFlag.value = true;
          return;
        } else {
          imgFlag.value = false;
        }
        api_member.memberUpdateInfo({
          type: "headimg",
          value: upImg.value
        }).then((res) => {
          common_vendor.index.navigateBack({
            delta: 1
          });
        });
      } else {
        let addALL = regionId.value + "-" + regionName.value;
        let obj = {};
        obj[flag.value] = addALL;
        api_member.memberUpdateInfo({
          type: "addAll",
          value: addALL
        }).then((res) => {
          common_vendor.index.navigateBack({
            delta: 1
          });
        });
      }
    }
    function getaddress(data2) {
      region.value = [];
      regionName.value = data2.addAll;
      regions.value.push({
        id: data2.provinceId,
        regionName: data2.addAll.split(" ")[0]
      }, {
        id: data2.areaId,
        regionName: data2.addAll.split(" ")[1]
      }, {
        id: data2.cityId,
        regionName: data2.addAll.split(" ")[2]
      });
    }
    function title(data2) {
      switch (data2) {
        case "name":
          return "修改昵称";
        case "email":
          return "修改邮箱";
        case "gender":
          return "修改性别";
        case "birthday":
          return "修改生日";
        case "addAll":
          return "修改地区";
      }
    }
    function select(e) {
      console.log(e.tempFiles[0].file);
      common_vendor.index.uploadFile({
        url: `https://b2c.yixiekeji.cn/prod-api/api/uploadFileImage`,
        filePath: e.tempFilePaths[0],
        name: "imageFile",
        header: {
          // "Content-Type": "multipart/form-data; boundary=----WebKitFormBoundaryHEkzvzS0GejAYXXA"
          // boundary=----WebKitFormBoundaryNKkJ14EwhaGBw4bW
          // 'token':'eb992333-034c-4b82-a446-06a137641f7a'
        },
        success: (res) => {
          console.log(JSON.parse(res.data).data);
          upImg.value = JSON.parse(res.data).data;
        },
        fail: (err) => {
          common_vendor.index.hideLoading();
          common_vendor.index.showToast({
            title: "上传附件失败，请稍候再试！",
            duration: 1e3,
            icon: "none"
          });
          return;
        }
      });
    }
    function handleDelete(err) {
      upImg.value = "";
    }
    common_vendor.onLoad((options) => {
      getData();
      flag.value = options.flag;
    });
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: common_vendor.p({
          ["nav-background-color"]: "#F5F5F7",
          backImageUrl: "1",
          textContent: title(flag.value),
          fontWeight: "500",
          fontSize: "15"
        }),
        b: flag.value == "name"
      }, flag.value == "name" ? {
        c: common_vendor.unref(formData).name,
        d: common_vendor.o(($event) => common_vendor.unref(formData).name = $event.detail.value)
      } : {}, {
        e: flag.value == "name" && nameFlag.value
      }, flag.value == "name" && nameFlag.value ? {} : {}, {
        f: flag.value == "email"
      }, flag.value == "email" ? {
        g: common_vendor.unref(formData).email,
        h: common_vendor.o(($event) => common_vendor.unref(formData).email = $event.detail.value)
      } : {}, {
        i: flag.value == "email" && emailFlag.value
      }, flag.value == "email" && emailFlag.value ? {} : {}, {
        j: flag.value == "birthday"
      }, flag.value == "birthday" ? {
        k: common_vendor.o(_ctx.maskClick),
        l: common_vendor.o(($event) => common_vendor.unref(formData).birthday = $event),
        m: common_vendor.p({
          type: "date",
          ["clear-icon"]: false,
          modelValue: common_vendor.unref(formData).birthday
        })
      } : {}, {
        n: flag.value == "addAll"
      }, flag.value == "addAll" ? {
        o: common_vendor.t(regionName.value == "" ? "请选择" : regionName.value),
        p: common_vendor.o(handleGetRegion),
        q: common_vendor.p({
          defaultRegion: defaultRegionCode.value
        })
      } : {}, {
        r: flag.value == "gender"
      }, flag.value == "gender" ? {
        s: common_vendor.t(common_vendor.unref(array)[common_vendor.unref(formData).gender]),
        t: common_vendor.o(bindPickerChange),
        v: index.value,
        w: common_vendor.unref(array)
      } : {}, {
        x: flag.value == "headImg"
      }, flag.value == "headImg" ? {
        y: common_vendor.o(_ctx.progress),
        z: common_vendor.o(_ctx.success),
        A: common_vendor.o(_ctx.fail),
        B: common_vendor.o(select),
        C: common_vendor.o(handleDelete),
        D: common_vendor.o(($event) => common_vendor.isRef(imageValue) ? imageValue.value = $event : imageValue = $event),
        E: common_vendor.p({
          ["file-mediatype"]: "image",
          mode: "grid",
          limit: 1,
          ["image-styles"]: imageStyles,
          modelValue: common_vendor.unref(imageValue)
        })
      } : {}, {
        F: imgFlag.value
      }, imgFlag.value ? {} : {}, {
        G: common_vendor.o(submit)
      });
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-99be847c"]]);
wx.createPage(MiniProgramPage);
