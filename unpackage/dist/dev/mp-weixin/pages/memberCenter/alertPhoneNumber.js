"use strict";
const common_vendor = require("../../common/vendor.js");
const api_member = require("../../api/member.js");
const api_login = require("../../api/login.js");
if (!Array) {
  const _component_HeadNav = common_vendor.resolveComponent("HeadNav");
  const _easycom_uni_easyinput2 = common_vendor.resolveComponent("uni-easyinput");
  const _easycom_uni_forms_item2 = common_vendor.resolveComponent("uni-forms-item");
  const _easycom_uni_forms2 = common_vendor.resolveComponent("uni-forms");
  (_component_HeadNav + _easycom_uni_easyinput2 + _easycom_uni_forms_item2 + _easycom_uni_forms2)();
}
const _easycom_uni_easyinput = () => "../../uni_modules/uni-easyinput/components/uni-easyinput/uni-easyinput.js";
const _easycom_uni_forms_item = () => "../../uni_modules/uni-forms/components/uni-forms-item/uni-forms-item.js";
const _easycom_uni_forms = () => "../../uni_modules/uni-forms/components/uni-forms/uni-forms.js";
if (!Math) {
  (_easycom_uni_easyinput + _easycom_uni_forms_item + _easycom_uni_forms)();
}
const _sfc_main = {
  __name: "alertPhoneNumber",
  setup(__props) {
    common_vendor.ref({});
    const form = common_vendor.ref();
    const timer = common_vendor.ref();
    const codeImg = common_vendor.ref("");
    const resendFlag = common_vendor.ref(true);
    const count = common_vendor.ref(60);
    let yphone = common_vendor.ref("");
    let formData = common_vendor.ref({
      phone: "",
      smsCode: "",
      verifyCode: "",
      verifyUUID: ""
    });
    let rules = common_vendor.ref({
      phone: {
        rules: [
          {
            required: true,
            errorMessage: "手机号码必填"
          },
          {
            pattern: /^1[3|4|5|6|7|8|9][0-9]\d{8}$/,
            errorMessage: "手机号码格式不正确"
          }
        ]
      },
      smsCode: {
        rules: [{
          required: true,
          errorMessage: "手机验证码必填"
        }]
      },
      verifyCode: {
        rules: [{
          required: true,
          errorMessage: "验证码必填"
        }]
      }
    });
    function getMemberInfo() {
      api_member.memberInfo().then((res) => {
        console.log(res);
        yphone.value = res.data.data.phone;
      });
    }
    function getVerify() {
      api_login.verify().then((res) => {
        codeImg.value = "data:image/jpg;base64," + res.data.data.base64Image;
        formData.value.verifyUUID = res.data.data.verifyUUID;
      });
    }
    function resendCode() {
      form.value.clearValidate();
      form.value.validateField(["phone", "verifyCode"]).then((res) => {
        getSendVerifySMS();
      }).catch((err) => {
      });
    }
    function getSendVerifySMS() {
      api_login.sendVerifySMS({
        phone: formData.value.phone,
        verifycode: formData.value.verifyCode,
        verifyUUID: formData.value.verifyUUID,
        type: ""
      }).then((res) => {
        console.log(res);
        if (res.data.success) {
          common_vendor.index.showToast({
            icon: "none",
            duration: 3e3,
            title: "验证码已发送"
          });
          resendFlag.value = false;
          getTimer();
          count.value = 60;
        } else {
          common_vendor.index.showToast({
            icon: "none",
            duration: 3e3,
            title: `${res.data.message}`
          });
          getVerify();
        }
      });
    }
    function resendCodes() {
      if (count.value != -1) {
        return;
      }
      form.value.clearValidate();
      form.value.validateField(["phone", "verifyCode"]).then((res) => {
        getSendVerifySMS();
      }).catch((err) => {
      });
    }
    function getTimer() {
      timer.value = setInterval(() => {
        if (count.value == 0) {
          clearInterval(timer.value);
          count.value = -1;
          return;
        }
        count.value--;
      }, 1e3);
    }
    function register() {
      form.value.validate().then((res) => {
        api_login.updatephone(formData.value).then((res2) => {
          if (res2.data.success) {
            common_vendor.index.showToast({
              title: "换绑成功",
              duration: 2e3,
              icon: "none"
            });
            getVerify();
            common_vendor.index.switchTab({
              url: "/pages/memberCenter/memberCenter"
            });
          }
        });
      }).catch((err) => {
      });
    }
    common_vendor.onShow(() => {
      if (common_vendor.index.getStorageSync("token")) {
        getVerify();
        getMemberInfo();
      } else {
        common_vendor.index.navigateTo({
          url: "/pages/loginPage/loginPage"
        });
      }
    });
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: common_vendor.p({
          ["nav-background-color"]: "#F5F5F7",
          backImageUrl: "1",
          textContent: "更换绑定手机号",
          fontWeight: "500",
          fontSize: "15"
        }),
        b: common_vendor.t(common_vendor.unref(yphone)),
        c: common_vendor.o(($event) => common_vendor.unref(formData).phone = $event),
        d: common_vendor.p({
          styles: {
            "borderColor": "#fff",
            "backgroundColor": "#F5F5F7",
            "height": "96rpx",
            "borderRadius": "48rpx"
          },
          type: "number",
          placeholder: "请输入新手机号",
          modelValue: common_vendor.unref(formData).phone
        }),
        e: common_vendor.p({
          name: "phone",
          label: "新手机号码",
          required: true
        }),
        f: common_vendor.o(($event) => common_vendor.unref(formData).verifyCode = $event),
        g: common_vendor.p({
          styles: {
            "borderColor": "#fff",
            "backgroundColor": "#F5F5F7",
            "height": "96rpx",
            "borderRadius": "48rpx"
          },
          placeholder: "图像验证码",
          modelValue: common_vendor.unref(formData).verifyCode
        }),
        h: codeImg.value,
        i: common_vendor.o(getVerify),
        j: common_vendor.p({
          name: "verifyCode",
          label: "图形验证码",
          required: true
        }),
        k: common_vendor.o(($event) => common_vendor.unref(formData).smsCode = $event),
        l: common_vendor.p({
          styles: {
            "borderColor": "#fff",
            "backgroundColor": "#F5F5F7",
            "height": "96rpx",
            "borderRadius": "48rpx",
            "width": "500rpx"
          },
          placeholder: "短信验证码",
          modelValue: common_vendor.unref(formData).smsCode
        }),
        m: resendFlag.value
      }, resendFlag.value ? {
        n: common_vendor.o(resendCode)
      } : common_vendor.e({
        o: !resendFlag.value && count.value != -1
      }, !resendFlag.value && count.value != -1 ? {
        p: common_vendor.t(count.value)
      } : {}, {
        q: common_vendor.o(resendCodes),
        r: count.value != -1 ? "#ccc" : "#1C9B64"
      }), {
        s: common_vendor.p({
          name: "smsCode",
          label: "短信验证码",
          required: true
        }),
        t: common_vendor.sr(form, "0b4bee8e-1", {
          "k": "form"
        }),
        v: common_vendor.p({
          modelValue: common_vendor.unref(formData),
          rules: common_vendor.unref(rules),
          ["label-width"]: "166rpx"
        }),
        w: common_vendor.o(register)
      });
    };
  }
};
wx.createPage(_sfc_main);
