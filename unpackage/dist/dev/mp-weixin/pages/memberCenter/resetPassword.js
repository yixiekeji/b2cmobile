"use strict";
const common_vendor = require("../../common/vendor.js");
const api_member = require("../../api/member.js");
const api_login = require("../../api/login.js");
const store_login = require("../../store/login.js");
if (!Array) {
  const _component_HeadNav = common_vendor.resolveComponent("HeadNav");
  const _easycom_uni_easyinput2 = common_vendor.resolveComponent("uni-easyinput");
  const _easycom_uni_forms_item2 = common_vendor.resolveComponent("uni-forms-item");
  const _easycom_uni_forms2 = common_vendor.resolveComponent("uni-forms");
  (_component_HeadNav + _easycom_uni_easyinput2 + _easycom_uni_forms_item2 + _easycom_uni_forms2)();
}
const _easycom_uni_easyinput = () => "../../uni_modules/uni-easyinput/components/uni-easyinput/uni-easyinput.js";
const _easycom_uni_forms_item = () => "../../uni_modules/uni-forms/components/uni-forms-item/uni-forms-item.js";
const _easycom_uni_forms = () => "../../uni_modules/uni-forms/components/uni-forms/uni-forms.js";
if (!Math) {
  (_easycom_uni_easyinput + _easycom_uni_forms_item + _easycom_uni_forms)();
}
const _sfc_main = {
  __name: "resetPassword",
  setup(__props) {
    common_vendor.ref({});
    const form = common_vendor.ref();
    const timer = common_vendor.ref();
    const codeImg = common_vendor.ref("");
    const resendFlag = common_vendor.ref(true);
    const count = common_vendor.ref(60);
    let formData = common_vendor.ref({
      userName: "",
      password: "",
      repassword: "",
      phone: "",
      smsCode: "",
      verifyCode: "",
      verifyUUID: ""
    });
    let rules = common_vendor.ref({
      password: {
        rules: [
          {
            required: true,
            errorMessage: "密码必填"
          },
          {
            minLength: 6,
            errorMessage: "至少需要六个字符"
          },
          {
            maxLength: 20,
            errorMessage: "最多需要二十个字符"
          }
        ]
      },
      repassword: {
        rules: [{
          required: true,
          errorMessage: "两次密码不一致"
        }, {
          validateFunction: function(rule, value, data, callback) {
            if (formData.value.password !== formData.value.repassword) {
              callback("两次密码不一致");
            }
            return true;
          }
        }]
      },
      smsCode: {
        rules: [{
          required: true,
          errorMessage: "手机验证码必填"
        }]
      },
      verifyCode: {
        rules: [{
          required: true,
          errorMessage: "验证码必填"
        }]
      }
    });
    function getMemberInfo() {
      api_member.memberInfo().then((res) => {
        console.log(res);
        formData.value.phone = res.data.data.phone;
        formData.value.userName = res.data.data.name;
      });
    }
    function getVerify() {
      api_login.verify().then((res) => {
        codeImg.value = "data:image/jpg;base64," + res.data.data.base64Image;
        formData.value.verifyUUID = res.data.data.verifyUUID;
      });
    }
    function resendCode() {
      form.value.clearValidate();
      form.value.validateField(["phone", "verifyCode"]).then((res) => {
        getSendVerifySMS();
      }).catch((err) => {
      });
    }
    function getSendVerifySMS() {
      api_login.sendVerifySMS({
        phone: formData.value.phone,
        verifycode: formData.value.verifyCode,
        verifyUUID: formData.value.verifyUUID,
        type: ""
      }).then((res) => {
        console.log(res);
        if (res.data.success) {
          common_vendor.index.showToast({
            icon: "none",
            duration: 3e3,
            title: "验证码已发送"
          });
          resendFlag.value = false;
          getTimer();
          count.value = 60;
        } else {
          common_vendor.index.showToast({
            icon: "none",
            duration: 3e3,
            title: `${res.data.message}`
          });
          getVerify();
        }
      });
    }
    function resendCodes() {
      if (count.value != -1) {
        return;
      }
      form.value.clearValidate();
      form.value.validateField(["phone", "verifyCode"]).then((res) => {
        getSendVerifySMS();
      }).catch((err) => {
      });
    }
    function getTimer() {
      timer.value = setInterval(() => {
        if (count.value == 0) {
          clearInterval(timer.value);
          count.value = -1;
          return;
        }
        count.value--;
      }, 1e3);
    }
    function register() {
      form.value.validate().then((res) => {
        api_login.doforgetpassword(formData.value).then((res2) => {
          if (res2.data.success) {
            common_vendor.index.showToast({
              title: "操作成功，请重新登录",
              duration: 2e3,
              icon: "none",
              success: () => {
                setTimeout(function() {
                  loginOut();
                }, 1e3);
              }
            });
          }
          getVerify();
        });
      }).catch((err) => {
      });
    }
    function loginOut() {
      store_login.login().loginOut().then(() => {
        common_vendor.index.navigateTo({
          url: "/pages/loginInterface/passwordLogin"
        });
      }).catch(() => {
      });
    }
    common_vendor.onShow(() => {
      getVerify();
      getMemberInfo();
    });
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: common_vendor.p({
          ["nav-background-color"]: "#F5F5F7",
          backImageUrl: "1",
          textContent: "重置密码",
          fontWeight: "500",
          fontSize: "15"
        }),
        b: common_vendor.t(common_vendor.unref(formData).phone),
        c: common_vendor.o(($event) => common_vendor.unref(formData).password = $event),
        d: common_vendor.p({
          styles: {
            "borderColor": "#fff",
            "backgroundColor": "#F5F5F7",
            "height": "96rpx",
            "borderRadius": "14rpx"
          },
          type: "password",
          placeholder: "请输入密码,6-20位字符",
          modelValue: common_vendor.unref(formData).password
        }),
        e: common_vendor.p({
          name: "password",
          label: "新密码",
          required: true
        }),
        f: common_vendor.o(($event) => common_vendor.unref(formData).repassword = $event),
        g: common_vendor.p({
          styles: {
            "borderColor": "#fff",
            "backgroundColor": "#F5F5F7",
            "height": "96rpx",
            "borderRadius": "14rpx"
          },
          type: "password",
          placeholder: "确认密码",
          modelValue: common_vendor.unref(formData).repassword
        }),
        h: common_vendor.p({
          name: "repassword",
          label: "确认密码",
          required: true
        }),
        i: common_vendor.o(($event) => common_vendor.unref(formData).verifyCode = $event),
        j: common_vendor.p({
          styles: {
            "borderColor": "#fff",
            "backgroundColor": "#F5F5F7",
            "height": "96rpx",
            "borderRadius": "14rpx"
          },
          placeholder: "图像验证码",
          modelValue: common_vendor.unref(formData).verifyCode
        }),
        k: codeImg.value,
        l: common_vendor.o(getVerify),
        m: common_vendor.p({
          name: "verifyCode",
          label: "图形验证码",
          required: true
        }),
        n: common_vendor.o(($event) => common_vendor.unref(formData).smsCode = $event),
        o: common_vendor.p({
          styles: {
            "borderColor": "#fff",
            "backgroundColor": "#F5F5F7",
            "height": "96rpx",
            "borderRadius": "14rpx",
            "width": "500rpx"
          },
          placeholder: "测试环境默认:888888",
          modelValue: common_vendor.unref(formData).smsCode
        }),
        p: resendFlag.value
      }, resendFlag.value ? {
        q: common_vendor.o(resendCode)
      } : common_vendor.e({
        r: !resendFlag.value && count.value != -1
      }, !resendFlag.value && count.value != -1 ? {
        s: common_vendor.t(count.value)
      } : {}, {
        t: common_vendor.o(resendCodes),
        v: count.value != -1 ? "#ccc" : "#1C9B64"
      }), {
        w: common_vendor.p({
          name: "smsCode",
          label: "短信验证码",
          required: true
        }),
        x: common_vendor.sr(form, "64e49c1c-1", {
          "k": "form"
        }),
        y: common_vendor.p({
          modelValue: common_vendor.unref(formData),
          rules: common_vendor.unref(rules),
          ["label-width"]: "166rpx"
        }),
        z: common_vendor.o(register)
      });
    };
  }
};
wx.createPage(_sfc_main);
