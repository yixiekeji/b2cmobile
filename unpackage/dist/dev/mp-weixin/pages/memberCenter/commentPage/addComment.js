"use strict";
const common_vendor = require("../../../common/vendor.js");
const api_member = require("../../../api/member.js");
if (!Array) {
  const _component_HeadNav = common_vendor.resolveComponent("HeadNav");
  const _easycom_uni_rate2 = common_vendor.resolveComponent("uni-rate");
  const _easycom_uni_forms_item2 = common_vendor.resolveComponent("uni-forms-item");
  const _easycom_uni_easyinput2 = common_vendor.resolveComponent("uni-easyinput");
  const _easycom_uni_file_picker2 = common_vendor.resolveComponent("uni-file-picker");
  const _easycom_uni_forms2 = common_vendor.resolveComponent("uni-forms");
  (_component_HeadNav + _easycom_uni_rate2 + _easycom_uni_forms_item2 + _easycom_uni_easyinput2 + _easycom_uni_file_picker2 + _easycom_uni_forms2)();
}
const _easycom_uni_rate = () => "../../../uni_modules/uni-rate/components/uni-rate/uni-rate.js";
const _easycom_uni_forms_item = () => "../../../uni_modules/uni-forms/components/uni-forms-item/uni-forms-item.js";
const _easycom_uni_easyinput = () => "../../../uni_modules/uni-easyinput/components/uni-easyinput/uni-easyinput.js";
const _easycom_uni_file_picker = () => "../../../uni_modules/uni-file-picker/components/uni-file-picker/uni-file-picker.js";
const _easycom_uni_forms = () => "../../../uni_modules/uni-forms/components/uni-forms/uni-forms.js";
if (!Math) {
  (_easycom_uni_rate + _easycom_uni_forms_item + _easycom_uni_easyinput + _easycom_uni_file_picker + _easycom_uni_forms)();
}
const _sfc_main = {
  __name: "addComment",
  setup(__props) {
    const form = common_vendor.ref();
    common_vendor.ref(false);
    let formData = common_vendor.ref({
      grade: 5,
      content: "",
      imgs: "",
      ordersId: "",
      ordersProductId: "",
      productCommentId: ""
    });
    common_vendor.ref("");
    let imageValue = common_vendor.ref([]);
    const styles = common_vendor.reactive({
      color: "#999",
      backgroundColor: "#F5F5F7",
      disableColor: "#F7F6F6",
      borderColor: "#00000000",
      height: "109px",
      borderRadius: "12px",
      fontSize: "15px"
    });
    const imageStyles = common_vendor.reactive({
      width: 90,
      height: 90
    });
    common_vendor.ref("");
    let upImg = common_vendor.ref([]);
    let productCommentList = common_vendor.ref([]);
    let rules = common_vendor.ref({
      content: {
        rules: [
          {
            required: true,
            errorMessage: "评价内容必填"
          },
          {
            minLength: 2,
            errorMessage: "至少需要2字符"
          },
          {
            maxLength: 100,
            errorMessage: "最多需要100字符"
          }
        ]
      }
    });
    function getDetails() {
      api_member.orderscomment({
        ordersId: formData.value.ordersId
      }).then((res) => {
        res.data.data.ordersProductList.forEach((item) => {
          if (item.id == formData.value.ordersProductId) {
            productCommentList.value = [];
            productCommentList.value.push(item);
          }
        });
        console.log(res.data.data.ordersProductList[0].id);
        api_member.getProductComment({
          ordersId: formData.value.ordersId,
          ordersProductId: formData.value.ordersProductId
        }).then((result) => {
          imageValue.value = [];
          formData.value.content = result.data.data.content;
          formData.value.grade = result.data.data.grade;
          formData.value.productCommentId = result.data.data.id;
          result.data.data.productCommentPictureList.forEach((item) => {
            imageValue.value.push({
              url: "http://img.yixiekeji.com:8080/b2cimage/" + item.imagePath,
              id: item.imagePath
            });
            upImg.value.push({
              path: item.imagePath,
              id: item.imagePath
            });
          });
          console.log(result);
        });
      });
    }
    function gradeChange(e) {
      formData.value.grade = e.value;
    }
    function select(e) {
      common_vendor.index.uploadFile({
        url: `https://b2c.yixiekeji.cn/prod-api/api/uploadFileImage`,
        filePath: e.tempFilePaths[0],
        name: "imageFile",
        header: {},
        success: (res) => {
          upImg.value.push({
            id: e.tempFiles[0].uuid,
            path: JSON.parse(res.data).data
          });
        },
        fail: (err) => {
          common_vendor.index.hideLoading();
          common_vendor.index.showToast({
            title: "上传附件失败，请稍候再试！",
            duration: 1e3,
            icon: "none"
          });
          return;
        }
      });
    }
    function handleDelete(err) {
      console.log(err);
      upImg.value.forEach((item, index) => {
        if (item.id == err.tempFile.uuid || item.id == err.tempFile.id) {
          upImg.value.splice(index, 1);
        }
      });
    }
    function commit() {
      form.value.validate().then((res) => {
        formData.value.imgs = "";
        upImg.value.forEach((item) => {
          formData.value.imgs += item.path + ",";
        });
        api_member.savecomment(formData.value).then((res2) => {
          console.log(res2);
          if (res2.data.success) {
            common_vendor.index.showToast({
              title: "上传评价成功",
              duration: 1e3,
              icon: "none"
            });
            common_vendor.index.navigateBack({
              delta: 1
            });
          }
        });
      }).catch((err) => {
      });
    }
    common_vendor.onLoad((options) => {
      formData.value.ordersId = options.ordersId;
      formData.value.ordersProductId = options.ordersProductId;
      getDetails();
    });
    return (_ctx, _cache) => {
      return {
        a: common_vendor.p({
          ["nav-background-color"]: "#f7f7f5",
          backImageUrl: "1",
          textContent: "商品评价",
          fontSize: "15",
          fontWeight: "600"
        }),
        b: common_vendor.f(common_vendor.unref(productCommentList), (item, k0, i0) => {
          return {
            a: _ctx.imageUrl + item.productImg,
            b: common_vendor.t(item.productName),
            c: common_vendor.t(item.specInfo),
            d: common_vendor.n(item.specInfo ? "size" : "font22"),
            e: common_vendor.t(item.createTime)
          };
        }),
        c: common_vendor.o(gradeChange),
        d: common_vendor.p({
          size: "22",
          value: common_vendor.unref(formData).grade,
          color: "#bbb",
          ["active-color"]: "#FF9500"
        }),
        e: common_vendor.p({
          label: "商品评价",
          name: "image"
        }),
        f: common_vendor.o(($event) => common_vendor.unref(formData).content = $event),
        g: common_vendor.p({
          type: "textarea",
          placeholder: "感觉怎么样?和大家分享一下吧(2-100字符)",
          styles,
          modelValue: common_vendor.unref(formData).content
        }),
        h: common_vendor.p({
          label: "",
          name: "content",
          msgs: "",
          ["label-width"]: "0px"
        }),
        i: common_vendor.o(_ctx.progress),
        j: common_vendor.o(_ctx.success),
        k: common_vendor.o(_ctx.fail),
        l: common_vendor.o(select),
        m: common_vendor.o(handleDelete),
        n: common_vendor.o(($event) => common_vendor.isRef(imageValue) ? imageValue.value = $event : imageValue = $event),
        o: common_vendor.p({
          ["file-mediatype"]: "image",
          mode: "grid",
          limit: 5,
          ["image-styles"]: imageStyles,
          modelValue: common_vendor.unref(imageValue)
        }),
        p: common_vendor.p({
          label: "",
          name: "image",
          ["label-width"]: "0px"
        }),
        q: common_vendor.sr(form, "eac85e22-1", {
          "k": "form"
        }),
        r: common_vendor.p({
          rules: common_vendor.unref(rules),
          modelValue: common_vendor.unref(formData),
          ["label-width"]: "66px"
        }),
        s: common_vendor.o(commit)
      };
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-eac85e22"]]);
wx.createPage(MiniProgramPage);
