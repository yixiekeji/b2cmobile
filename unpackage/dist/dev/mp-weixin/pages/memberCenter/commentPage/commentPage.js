"use strict";
const common_vendor = require("../../../common/vendor.js");
const common_assets = require("../../../common/assets.js");
const api_member = require("../../../api/member.js");
if (!Array) {
  const _component_HeadNav = common_vendor.resolveComponent("HeadNav");
  const _easycom_uni_icons2 = common_vendor.resolveComponent("uni-icons");
  const _easycom_uni_rate2 = common_vendor.resolveComponent("uni-rate");
  const _easycom_uni_popup_dialog2 = common_vendor.resolveComponent("uni-popup-dialog");
  const _easycom_uni_popup2 = common_vendor.resolveComponent("uni-popup");
  (_component_HeadNav + _easycom_uni_icons2 + _easycom_uni_rate2 + _easycom_uni_popup_dialog2 + _easycom_uni_popup2)();
}
const _easycom_uni_icons = () => "../../../uni_modules/uni-icons/components/uni-icons/uni-icons.js";
const _easycom_uni_rate = () => "../../../uni_modules/uni-rate/components/uni-rate/uni-rate.js";
const _easycom_uni_popup_dialog = () => "../../../uni_modules/uni-popup/components/uni-popup-dialog/uni-popup-dialog.js";
const _easycom_uni_popup = () => "../../../uni_modules/uni-popup/components/uni-popup/uni-popup.js";
if (!Math) {
  (_easycom_uni_icons + _easycom_uni_rate + _easycom_uni_popup_dialog + _easycom_uni_popup)();
}
const _sfc_main = {
  __name: "commentPage",
  setup(__props) {
    const loading = common_vendor.ref(false);
    const loadingText = common_vendor.ref("加载中...");
    const flag = common_vendor.ref("");
    const page = common_vendor.ref(1);
    const id = common_vendor.ref("");
    common_vendor.ref({
      "borderColor": "#fff",
      "backgroundColor": "#fff",
      "height": "96rpx",
      "borderRadius": "14rpx"
    });
    const dataFlag = common_vendor.ref(false);
    function details(val) {
      if (val == flag.value) {
        flag.value = "";
      } else {
        flag.value = val;
      }
    }
    let productCommentList = common_vendor.ref([]);
    function getList() {
      dataFlag.value = false;
      api_member.commentlist({
        page: page.value
      }).then((res) => {
        if (res.data.data.productCommentList.length == 0 && page.value > 1 && productCommentList.value.length > 30) {
          loadingText.value = "到底啦~";
        } else {
          let data = res.data.data.productCommentList;
          data.forEach((item) => {
            if (item.replyContent.length > 51) {
              item.textOpenFlag = null;
            } else {
              item.textOpenFlag = null;
            }
          });
          productCommentList.value = [...productCommentList.value, ...data];
          loading.value = false;
        }
        dataFlag.value = true;
      });
    }
    const alertDialog = common_vendor.ref();
    function delet(val) {
      id.value = val;
      alertDialog.value.open();
    }
    function dialogConfirm() {
      api_member.deleted({
        productCommentId: id.value
      }).then((res) => {
        if (res.data.success) {
          common_vendor.index.showToast({
            title: "操作成功",
            duration: 1e3,
            icon: "none"
          });
          productCommentList.value = [];
          page.value = 1;
          id.value = "";
          getList();
        }
      });
    }
    function dialogClose() {
      id.value = "";
      alertDialog.value.close();
    }
    let popup = common_vendor.ref();
    let imgUrl = common_vendor.ref("");
    function openPopup(val) {
      imgUrl.value = val;
      popup.value.open();
    }
    function closePopup() {
      popup.value.close();
    }
    common_vendor.onLoad(() => {
      productCommentList.value = [];
      page.value = 1;
      getList();
    });
    common_vendor.onReachBottom(() => {
      if (productCommentList.value.length >= 20 * page.value) {
        page.value += 1;
        loading.value = true;
        loadingText.value = "加载中...";
        setTimeout(() => {
          getList();
        }, 800);
      } else {
        loading.value = true;
        loadingText.value = "到底啦~";
      }
    });
    common_vendor.onPullDownRefresh(() => {
      setTimeout(() => {
        common_vendor.index.stopPullDownRefresh();
        productCommentList.value = [];
        loading.value = false;
        page.value = 1;
        getList();
      }, 800);
    });
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: common_vendor.p({
          ["nav-background-color"]: "#f7f7f5",
          backImageUrl: "1",
          textContent: "我的评论",
          fontSize: "15",
          fontWeight: "600"
        }),
        b: common_vendor.f(common_vendor.unref(productCommentList), (item, k0, i0) => {
          return common_vendor.e({
            a: _ctx.imageUrl + item.productImg,
            b: common_vendor.t(item.productName),
            c: common_vendor.t(item.specInfo),
            d: common_vendor.n(item.specInfo ? "size" : "font22"),
            e: common_vendor.t(item.createTime),
            f: common_vendor.t(flag.value == item.id ? "关闭评论" : "查看评论"),
            g: common_vendor.o(($event) => details(item.id)),
            h: common_vendor.o(($event) => delet(item.id)),
            i: item.memberHeadImg && item.memberHeadImg.indexOf("http") != -1
          }, item.memberHeadImg && item.memberHeadImg.indexOf("http") != -1 ? {
            j: item.memberHeadImg
          } : item.memberHeadImg ? {
            l: _ctx.imageUrl + item.memberHeadImg
          } : {
            m: "6cf93acb-1-" + i0,
            n: common_vendor.p({
              type: "contact",
              size: "40",
              color: "#1C9B64"
            })
          }, {
            k: item.memberHeadImg,
            o: common_vendor.t(item.memberName),
            p: "6cf93acb-2-" + i0,
            q: common_vendor.p({
              size: "18",
              value: item.grade,
              color: "#bbb",
              ["active-color"]: "#1C9B64"
            }),
            r: common_vendor.t(item.content),
            s: common_vendor.f(item.productCommentPictureList, (itemImg, k1, i1) => {
              return {
                a: common_vendor.o(($event) => openPopup(itemImg.imagePath)),
                b: _ctx.imageUrl + itemImg.imagePath
              };
            }),
            t: item.replyContent
          }, item.replyContent ? common_vendor.e({
            v: common_vendor.t(item.replyContent),
            w: item.textOpenFlag ? "58rpx" : "",
            x: item.textOpenFlag ? 1 : "",
            y: item.textOpenFlag !== null
          }, item.textOpenFlag !== null ? {
            z: common_vendor.t(item.textOpenFlag ? "【展开】" : "【收起】"),
            A: common_vendor.o(($event) => item.textOpenFlag = !item.textOpenFlag)
          } : {}) : {}, {
            B: common_vendor.n(flag.value == item.id ? "popAct" : "pop")
          });
        }),
        c: common_vendor.o(dialogConfirm),
        d: common_vendor.o(dialogClose),
        e: common_vendor.p({
          type: _ctx.msgType,
          cancelText: "取消",
          confirmText: "确认",
          content: "是否删除此条评价？"
        }),
        f: common_vendor.sr(alertDialog, "6cf93acb-3", {
          "k": "alertDialog"
        }),
        g: common_vendor.p({
          type: "dialog"
        }),
        h: loading.value
      }, loading.value ? {
        i: common_vendor.t(loadingText.value)
      } : {}, {
        j: common_vendor.unref(productCommentList).length == 0 && dataFlag.value
      }, common_vendor.unref(productCommentList).length == 0 && dataFlag.value ? {
        k: common_assets._imports_0$2
      } : {}, {
        l: common_vendor.o(closePopup),
        m: common_vendor.p({
          type: "closeempty",
          color: "#999",
          size: "18"
        }),
        n: _ctx.imageUrl + common_vendor.unref(imgUrl),
        o: common_vendor.sr(popup, "6cf93acb-5", {
          "k": "popup"
        }),
        p: common_vendor.p({
          type: "center"
        })
      });
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-6cf93acb"]]);
wx.createPage(MiniProgramPage);
