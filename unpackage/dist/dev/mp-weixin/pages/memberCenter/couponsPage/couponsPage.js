"use strict";
const common_vendor = require("../../../common/vendor.js");
const common_assets = require("../../../common/assets.js");
const api_coupon = require("../../../api/coupon.js");
const api_goodsDetail = require("../../../api/goodsDetail.js");
if (!Array) {
  const _component_HeadNav = common_vendor.resolveComponent("HeadNav");
  _component_HeadNav();
}
const _sfc_main = {
  __name: "couponsPage",
  setup(__props) {
    let list = common_vendor.ref([]);
    const id = common_vendor.ref("");
    const couponName = common_vendor.ref("");
    const useEndTime = common_vendor.ref("");
    const couponValue = common_vendor.ref("");
    const minAmount = common_vendor.ref("");
    const linkProduct = common_vendor.ref("");
    const flag = common_vendor.ref("");
    const dataFlag = common_vendor.ref(false);
    function getList() {
      dataFlag.value = false;
      api_coupon.couponProduct({
        couponId: id.value
      }).then((res) => {
        list.value = res.data.data;
        dataFlag.value = true;
      });
    }
    function jumpDetail(val) {
      common_vendor.index.navigateTo({
        url: "/pages/homePage/goodsDetail?id=" + val
      });
    }
    function getCoupon() {
      api_goodsDetail.reveivecoupon({
        couponId: id.value
      }).then((res) => {
        if (res.data.success) {
          common_vendor.index.showToast({
            icon: "none",
            duration: 3e3,
            title: `领取成功`
          });
        }
      });
    }
    common_vendor.onLoad((options) => {
      id.value = options.id;
      couponName.value = options.couponName;
      useEndTime.value = options.useEndTime;
      couponValue.value = options.couponValue;
      minAmount.value = options.minAmount;
      linkProduct.value = options.linkProduct;
      if (options.flag) {
        flag.value = options.flag;
      } else {
        flag.value = "";
      }
      getList();
    });
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: common_vendor.p({
          ["nav-background-color"]: "#fff",
          backImageUrl: "1",
          textContent: "优惠券适用商品",
          fontSize: "15",
          fontWeight: "600"
        }),
        b: common_vendor.t(couponName.value),
        c: common_vendor.t(useEndTime.value),
        d: common_vendor.t(couponValue.value),
        e: common_vendor.t(minAmount.value),
        f: common_vendor.t(linkProduct.value == 1 ? "全部商品" : "部分商品"),
        g: flag.value != "1"
      }, flag.value != "1" ? {
        h: common_vendor.o(($event) => getCoupon())
      } : {}, {
        i: common_vendor.unref(list).length != 0
      }, common_vendor.unref(list).length != 0 ? {
        j: common_vendor.f(common_vendor.unref(list), (item, k0, i0) => {
          return {
            a: _ctx.imageUrl + item.product.masterImg,
            b: common_vendor.o(($event) => jumpDetail(item.productId)),
            c: common_vendor.t(item.product.name),
            d: common_vendor.t(item.product.mallPrice),
            e: common_vendor.o(($event) => jumpDetail(item.product.id))
          };
        })
      } : {}, {
        k: common_vendor.unref(list).length == 0 && dataFlag.value
      }, common_vendor.unref(list).length == 0 && dataFlag.value ? {
        l: common_assets._imports_0$2
      } : {});
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-0e9e4b18"]]);
wx.createPage(MiniProgramPage);
