"use strict";
const common_vendor = require("../../../common/vendor.js");
const api_member = require("../../../api/member.js");
if (!Array) {
  const _component_HeadNav = common_vendor.resolveComponent("HeadNav");
  const _easycom_pick_regions2 = common_vendor.resolveComponent("pick-regions");
  (_component_HeadNav + _easycom_pick_regions2)();
}
const _easycom_pick_regions = () => "../../../components/pick-regions/pick-regions.js";
if (!Math) {
  _easycom_pick_regions();
}
const _sfc_main = {
  __name: "addAddress",
  setup(__props) {
    let regions = common_vendor.ref([]);
    common_vendor.ref(["广东省", "广州市", "番禺区"]);
    const defaultRegionCode = common_vendor.ref("440113");
    const state = common_vendor.ref("");
    const name = common_vendor.ref("");
    const mobile = common_vendor.ref("");
    const addressInfo = common_vendor.ref("");
    const switchChecked = common_vendor.ref(false);
    const addressDetailFlag = common_vendor.ref(false);
    const addressFlag = common_vendor.ref(false);
    const phoneFlag = common_vendor.ref(false);
    const nameFlag = common_vendor.ref(false);
    const id = common_vendor.ref("");
    const nameplace = common_vendor.ref("请输入收货人名称");
    const phonePlace = common_vendor.ref("请输入收货手机号");
    const detailPlace = common_vendor.ref("请输入详细地址");
    let regionName = common_vendor.ref("");
    function handleGetRegion(region) {
      regions.value = [...region];
      regionName.value = region.map((item) => item.regionName).join(" ");
      console.log(regions.value);
    }
    function swichChange(e) {
      state.value = e.detail.value;
    }
    function save() {
      if (name.value == "") {
        nameFlag.value = true;
        nameplace.value = "请输入收货人名称";
      } else if (name.value.length < 2) {
        nameFlag.value = true;
        nameplace.value = "收货人名称至少为两位字符";
      } else {
        nameFlag.value = false;
      }
      if (mobile.value == "") {
        phoneFlag.value = true;
        phonePlace.value = "请输入收货手机号";
      } else if (!/^1(3\d|4[5-9]|5[0-35-9]|6[567]|7[0-8]|8\d|9[0-35-9])\d{8}$/.test(mobile.value)) {
        phoneFlag.value = true;
        phonePlace.value = "手机号格式不正确";
      } else {
        phoneFlag.value = false;
      }
      if (addressInfo.value == "") {
        addressDetailFlag.value = true;
        detailPlace.value = "请输入详细地址";
      } else if (addressInfo.value.length < 2) {
        addressDetailFlag.value = true;
        detailPlace.value = "详细收货地址至少为两位字符";
      } else {
        addressDetailFlag.value = false;
      }
      regionName.value == "" ? addressFlag.value = true : addressFlag.value = false;
      if (!nameFlag.value && !phoneFlag.value && !addressDetailFlag.value && !addressFlag.value) {
        if (id.value) {
          console.log(regions.value);
          api_member.updateaddress({
            addAll: regionName.value,
            addressInfo: addressInfo.value,
            mobile: mobile.value,
            name: name.value,
            state: state.value == true ? 1 : 0,
            provinceId: regions.value[0].id,
            areaId: regions.value[1].id,
            cityId: regions.value[2].id,
            id: id.value
          }).then((res) => {
            id.value = "";
            common_vendor.index.navigateBack({
              delta: 1
            });
          });
        } else {
          api_member.createaddress({
            addAll: regionName.value,
            addressInfo: addressInfo.value,
            mobile: mobile.value,
            name: name.value,
            state: state.value == true ? 1 : 0,
            provinceId: regions.value[0].id,
            areaId: regions.value[1].id,
            cityId: regions.value[2].id
          }).then((res) => {
            id.value = "";
            common_vendor.index.navigateBack({
              delta: 1
            });
          });
        }
      }
    }
    function getDetails(id2) {
      api_member.getMemberAddress({
        id: id2
      }).then((res) => {
        regionName.value = res.data.data.addAll;
        addressInfo.value = res.data.data.addressInfo;
        mobile.value = res.data.data.mobile;
        name.value = res.data.data.name;
        state.value = res.data.data.state;
        switchChecked.value = res.data.data.state == 0 ? false : true;
        regions.value.push({
          id: res.data.data.provinceId,
          regionName: res.data.data.addAll.split(" ")[0]
        }, {
          id: res.data.data.areaId,
          regionName: res.data.data.addAll.split(" ")[1]
        }, {
          id: res.data.data.cityId,
          regionName: res.data.data.addAll.split(" ")[2]
        });
      });
    }
    common_vendor.onLoad((options) => {
      if (options.id) {
        id.value = options.id;
        getDetails(options.id);
      }
    });
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: common_vendor.p({
          ["nav-background-color"]: "#fff",
          backImageUrl: "1",
          textContent: "收货地址",
          fontSize: "15",
          fontWeight: "600"
        }),
        b: name.value,
        c: common_vendor.o(($event) => name.value = $event.detail.value),
        d: nameFlag.value
      }, nameFlag.value ? {
        e: common_vendor.t(nameplace.value)
      } : {}, {
        f: mobile.value,
        g: common_vendor.o(($event) => mobile.value = $event.detail.value),
        h: phoneFlag.value
      }, phoneFlag.value ? {
        i: common_vendor.t(phonePlace.value)
      } : {}, {
        j: common_vendor.unref(regionName),
        k: common_vendor.o(($event) => common_vendor.isRef(regionName) ? regionName.value = $event.detail.value : regionName = $event.detail.value),
        l: common_vendor.o(handleGetRegion),
        m: common_vendor.p({
          defaultRegion: defaultRegionCode.value
        }),
        n: addressFlag.value
      }, addressFlag.value ? {} : {}, {
        o: addressInfo.value,
        p: common_vendor.o(($event) => addressInfo.value = $event.detail.value),
        q: addressDetailFlag.value
      }, addressDetailFlag.value ? {
        r: common_vendor.t(detailPlace.value)
      } : {}, {
        s: switchChecked.value,
        t: common_vendor.o(swichChange),
        v: common_vendor.o(save)
      });
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-6186d2d7"]]);
wx.createPage(MiniProgramPage);
