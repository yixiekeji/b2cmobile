"use strict";
const common_vendor = require("../../../common/vendor.js");
const common_assets = require("../../../common/assets.js");
const api_member = require("../../../api/member.js");
const store_order = require("../../../store/order.js");
if (!Array) {
  const _component_HeadNav = common_vendor.resolveComponent("HeadNav");
  const _easycom_uni_popup_dialog2 = common_vendor.resolveComponent("uni-popup-dialog");
  const _easycom_uni_popup2 = common_vendor.resolveComponent("uni-popup");
  (_component_HeadNav + _easycom_uni_popup_dialog2 + _easycom_uni_popup2)();
}
const _easycom_uni_popup_dialog = () => "../../../uni_modules/uni-popup/components/uni-popup-dialog/uni-popup-dialog.js";
const _easycom_uni_popup = () => "../../../uni_modules/uni-popup/components/uni-popup/uni-popup.js";
if (!Math) {
  (_easycom_uni_popup_dialog + _easycom_uni_popup)();
}
const _sfc_main = {
  __name: "address",
  setup(__props) {
    let list = common_vendor.ref([]);
    const radioFlag = common_vendor.ref(false);
    common_vendor.ref(false);
    const orders = common_vendor.ref("");
    const deleteId = common_vendor.ref("");
    const alertDialog = common_vendor.ref();
    function getList() {
      radioFlag.value = false;
      api_member.address().then((res) => {
        list.value = [...res.data.data];
        radioFlag.value = true;
        console.log(res);
      });
    }
    function changeState(id) {
      console.log(id);
      api_member.auditaddress({
        id
      }).then((res) => {
        getList();
      });
    }
    function addAddress() {
      common_vendor.index.navigateTo({
        url: "/pages/memberCenter/address/addAddress"
      });
    }
    function edit(item) {
      common_vendor.index.navigateTo({
        url: "/pages/memberCenter/address/addAddress?id=" + item.id
      });
    }
    function deletes(item) {
      alertDialog.value.open();
      deleteId.value = item.id;
    }
    function dialogConfirm() {
      api_member.deleteaddress({
        id: deleteId.value
      }).then((res) => {
        getList();
      });
    }
    function dialogClose() {
      alertDialog.value.close();
    }
    function jumpOrder(addressId) {
      if (orders.value == "order") {
        store_order.order().setAddressId(addressId);
        common_vendor.index.redirectTo({
          url: "/pages/shoppingCart/confirmOrder"
        });
      } else if (orders.value == "jifenorder") {
        store_order.order().setAddressId(addressId);
        common_vendor.index.redirectTo({
          url: "/pages/memberCenter/integralPage/confirmOrder"
        });
      }
    }
    common_vendor.onLoad((options) => {
      if (options.order) {
        orders.value = options.order;
      }
      getList();
    });
    common_vendor.onShow(() => {
      getList();
    });
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: common_vendor.p({
          ["nav-background-color"]: "#F5F5F7",
          backImageUrl: "1",
          textContent: "收货地址",
          fontWeight: "600",
          fontSize: "15"
        }),
        b: common_vendor.unref(list).length != 0
      }, common_vendor.unref(list).length != 0 ? {
        c: common_vendor.f(common_vendor.unref(list), (item, k0, i0) => {
          return common_vendor.e({
            a: common_vendor.t(item.name),
            b: common_vendor.t(item.mobile),
            c: common_vendor.t(item.addAll + item.addressInfo),
            d: common_vendor.o(($event) => jumpOrder(item.id))
          }, radioFlag.value ? {
            e: item.state == 0 ? false : true,
            f: common_vendor.o(($event) => changeState(item.id))
          } : {}, {
            g: common_vendor.o(($event) => edit(item)),
            h: common_vendor.o(($event) => deletes(item))
          });
        }),
        d: common_assets._imports_0$4,
        e: radioFlag.value
      } : {}, {
        f: common_vendor.unref(list).length == 0
      }, common_vendor.unref(list).length == 0 ? {
        g: common_assets._imports_1$3
      } : {}, {
        h: common_vendor.o(addAddress),
        i: common_vendor.o(dialogConfirm),
        j: common_vendor.o(dialogClose),
        k: common_vendor.p({
          type: _ctx.msgType,
          cancelText: "取消",
          confirmText: "确认",
          content: "确认取消此条地址吗?"
        }),
        l: common_vendor.sr(alertDialog, "5a790d0d-1", {
          "k": "alertDialog"
        }),
        m: common_vendor.p({
          type: "dialog"
        })
      });
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-5a790d0d"]]);
wx.createPage(MiniProgramPage);
