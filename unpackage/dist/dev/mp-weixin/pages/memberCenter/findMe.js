"use strict";
const common_vendor = require("../../common/vendor.js");
const _sfc_main = {};
if (!Array) {
  const _component_HeadNav = common_vendor.resolveComponent("HeadNav");
  const _easycom_uni_list_item2 = common_vendor.resolveComponent("uni-list-item");
  const _easycom_uni_list2 = common_vendor.resolveComponent("uni-list");
  (_component_HeadNav + _easycom_uni_list_item2 + _easycom_uni_list2)();
}
const _easycom_uni_list_item = () => "../../uni_modules/uni-list/components/uni-list-item/uni-list-item.js";
const _easycom_uni_list = () => "../../uni_modules/uni-list/components/uni-list/uni-list.js";
if (!Math) {
  (_easycom_uni_list_item + _easycom_uni_list)();
}
function _sfc_render(_ctx, _cache) {
  return {
    a: common_vendor.p({
      ["nav-background-color"]: "#fff",
      backImageUrl: "1",
      textContent: "联系我们",
      fontSize: "15",
      fontWeight: "600"
    }),
    b: common_vendor.p({
      title: "公司名称",
      rightText: "北京易写科技有限公司",
      border: false
    }),
    c: common_vendor.p({
      title: "联系电话",
      rightText: "18612670879",
      border: false
    }),
    d: common_vendor.p({
      title: "微 信 号",
      rightText: "18612670879",
      border: false
    }),
    e: common_vendor.p({
      title: "Q  Q  号",
      rightText: "43006111",
      border: false
    }),
    f: common_vendor.p({
      border: false,
      title: "公司简介",
      note: "       北京易写科技有限公司是国内最专注的电商服务的软件公司，公司位于北京市，是一家拥有自主知识产权的高新科技公司，并相聚获得中关村高新、国家高新认证。\r\n       核心成员均来自一线互联网电商公司，有阿里、京东、海尔电商等工作经历，对互联网、电子商务有深刻理解，有着丰富的电商运营经验，具有大型电商平台架构和技术开发经验。\r\n       北京易写科技有限公司旗下B2B2C、B2C、B2B、O2O拥有自主的知识产权。公司定位中高端电子商务提供商、适合二次开发的电子商务系统。\r\n       旗下采用Java作为开发语言，结合团队在阿里、京东、海尔电商的实际开发经验，不断开发完善，产品从丰富产品功能、提升用户体验、优化系统性能等方法入手，经过多次升级改版，多次为大型集团公司定制开发，我们的客户有国家电网、中信集团、中电普华、国电通、自然堂、桔子分期、报喜鸟、中能国际、武汉艺术品交易中心等，优质的服务得到客户的一致好评。\r\n       除此之外，我们还提供区块链溯源、在线教育、知识服务等各种类软件的开发服务，有需求随时联系我们。\r\n       官网：http://www.yixiekeji.com/\r\n"
    }),
    g: common_vendor.p({
      border: false
    })
  };
}
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__scopeId", "data-v-cfd2809f"]]);
wx.createPage(MiniProgramPage);
