"use strict";
const common_vendor = require("../../common/vendor.js");
const common_assets = require("../../common/assets.js");
const api_member = require("../../api/member.js");
const store_login = require("../../store/login.js");
const api_login = require("../../api/login.js");
if (!Array) {
  const _component_HeadNav = common_vendor.resolveComponent("HeadNav");
  const _easycom_uni_steps2 = common_vendor.resolveComponent("uni-steps");
  const _easycom_uni_icons2 = common_vendor.resolveComponent("uni-icons");
  const _easycom_uni_popup_dialog2 = common_vendor.resolveComponent("uni-popup-dialog");
  const _easycom_uni_popup2 = common_vendor.resolveComponent("uni-popup");
  (_component_HeadNav + _easycom_uni_steps2 + _easycom_uni_icons2 + _easycom_uni_popup_dialog2 + _easycom_uni_popup2)();
}
const _easycom_uni_steps = () => "../../uni_modules/uni-steps/components/uni-steps/uni-steps.js";
const _easycom_uni_icons = () => "../../uni_modules/uni-icons/components/uni-icons/uni-icons.js";
const _easycom_uni_popup_dialog = () => "../../uni_modules/uni-popup/components/uni-popup-dialog/uni-popup-dialog.js";
const _easycom_uni_popup = () => "../../uni_modules/uni-popup/components/uni-popup/uni-popup.js";
if (!Math) {
  (_easycom_uni_steps + _easycom_uni_icons + _easycom_uni_popup_dialog + _easycom_uni_popup)();
}
const _sfc_main = {
  __name: "memberCenter",
  setup(__props) {
    let data = common_vendor.ref({});
    common_vendor.ref("Hello");
    common_vendor.ref(false);
    common_vendor.ref("rotate1");
    common_vendor.ref("white");
    common_vendor.ref(true);
    const actived = common_vendor.ref(0);
    let list1 = common_vendor.ref([{
      title: "注册会员",
      desc: 0
    }, {
      title: "铜牌会员",
      desc: 2e3
    }, {
      title: "银牌会员",
      desc: 5e3
    }, {
      title: "金牌会员",
      desc: 1e4
    }, {
      title: "钻石会员",
      desc: 3e4
    }]);
    const couponSize = common_vendor.ref();
    const alertDialog = common_vendor.ref();
    const shareFlag = common_vendor.ref();
    let flag = common_vendor.ref(false);
    function getData() {
      api_member.memberindex().then((res) => {
        console.log(res);
        if (res.data.success) {
          data.value = res.data.data.member;
          couponSize.value = res.data.data.couponSize;
          list1.value = [{
            title: "注册会员",
            desc: res.data.data.gradeConfig.grade1
          }, {
            title: "铜牌会员",
            desc: res.data.data.gradeConfig.grade2
          }, {
            title: "银牌会员",
            desc: res.data.data.gradeConfig.grade3
          }, {
            title: "金牌会员",
            desc: res.data.data.gradeConfig.grade4
          }, {
            title: "钻石会员",
            desc: res.data.data.gradeConfig.grade5
          }];
          if (data.value.grade == 1) {
            actived.value = 0;
          } else if (data.value.grade == 2) {
            actived.value = 1;
          } else if (data.value.grade == 3) {
            actived.value = 2;
          } else if (data.value.grade == 4) {
            actived.value = 3;
          } else if (data.value.grade == 5) {
            actived.value = 4;
          }
          flag.value = true;
        } else {
          data.value = {};
          actived.value = 0;
          list1.value = [{
            title: "注册会员",
            desc: "-"
          }, {
            title: "铜牌会员",
            desc: "-"
          }, {
            title: "银牌会员",
            desc: "-"
          }, {
            title: "金牌会员",
            desc: "-"
          }, {
            title: "钻石会员",
            desc: "-"
          }];
          flag.value = false;
        }
      });
    }
    function goOut() {
      alertDialog.value.open();
    }
    function dialogConfirm() {
      store_login.login().loginOut().then(() => {
        common_vendor.index.switchTab({
          url: "/pages/homePage/homepage"
        });
      }).catch(() => {
      });
    }
    function dialogClose() {
      alertDialog.value.close();
    }
    function wxLogin(e) {
      console.log(e);
      var detail = e.detail;
      if (detail.errMsg == "getPhoneNumber:ok") {
        console.log("用户同意授权");
        var code = detail.code;
        codes.value = detail.code;
        console.log(code);
        common_vendor.wx$1.login({
          success: function(result) {
            if (result.code) {
              api_login.getOpenId({
                jsCode: result.code
              }).then((res) => {
                console.log(res);
                if (res.data.success) {
                  openId.value = res.data.data.unionid;
                  xcxphone.value = res.data.data.phone;
                  console.log(res.data.data.phone);
                  if (res.data.data.phone) {
                    api_login.dologinWx({
                      openId: openId.value,
                      phone: res.data.data.phone,
                      headImage: ""
                    }).then((result2) => {
                      common_vendor.index.setStorageSync("token", result2.data.data.token);
                      store_login.login().setToken(result2.data.data.token);
                      common_vendor.index.showToast({
                        icon: "none",
                        duration: 3e3,
                        title: "登录成功",
                        success: () => {
                          setTimeout(
                            function() {
                              common_vendor.index.switchTab({
                                url: "/pages/homePage/homepage"
                              });
                            },
                            1e3
                          );
                        }
                      });
                      console.log(result2);
                    });
                  } else {
                    alertDialog.value.open();
                  }
                }
              });
            } else {
              console.log("登录失败！" + result.errMsg);
            }
          }
        });
      }
    }
    function orderJump(url) {
      if (common_vendor.index.getStorageSync("token")) {
        common_vendor.index.redirectTo({
          url
        });
      } else {
        common_vendor.index.navigateTo({
          url: "/pages/loginInterface/passwordLogin"
        });
      }
    }
    function onPageJump(url) {
      if (common_vendor.index.getStorageSync("token")) {
        common_vendor.index.navigateTo({
          url
        });
      } else {
        common_vendor.index.navigateTo({
          url: "/pages/loginInterface/passwordLogin"
        });
      }
    }
    function onPageJumps(url) {
      if (common_vendor.index.getStorageSync("token")) {
        common_vendor.index.navigateTo({
          url: url + data.value.phone
        });
      } else {
        common_vendor.index.navigateTo({
          url: "/pages/loginInterface/passwordLogin"
        });
      }
    }
    common_vendor.onShow(() => {
      getData();
    });
    common_vendor.onTabItemTap(() => {
      getData();
    });
    common_vendor.onShareAppMessage((res) => {
      console.log(res);
      return {
        title: "禾鲜谷小程序",
        path: `/pages/homePage/homePage`
        // imageUrl: '/static/imgs/mylogo.png'
      };
    });
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: common_vendor.o(_ctx.test),
        b: common_vendor.p({
          ["nav-background-color"]: "#FEE9A9",
          backImageUrl: "0",
          textContent: "用户中心",
          fontSize: "15",
          fontWeight: "600"
        }),
        c: common_vendor.unref(data).image
      }, common_vendor.unref(data).image ? {
        d: common_vendor.o(($event) => onPageJump("/pages/memberCenter/personalInformation/alterNikename?flag=headImg")),
        e: common_vendor.unref(data).image.indexOf("https") != -1 ? common_vendor.unref(data).image : _ctx.imageUrl + common_vendor.unref(data).image
      } : {
        f: common_vendor.o(($event) => onPageJump("/pages/memberCenter/personalInformation/alterNikename?flag=headImg")),
        g: common_assets._imports_0$5
      }, {
        h: common_vendor.t(common_vendor.unref(data).name ? common_vendor.unref(data).name : common_vendor.unref(data).phone ? common_vendor.unref(data).phone.substr(-4) : "登录/注册"),
        i: common_vendor.o(wxLogin),
        j: common_vendor.unref(flag)
      }, common_vendor.unref(flag) ? common_vendor.e({
        k: common_vendor.unref(data).grade == 1
      }, common_vendor.unref(data).grade == 1 ? {
        l: common_assets._imports_1$2
      } : {}, {
        m: common_vendor.unref(data).grade == 2
      }, common_vendor.unref(data).grade == 2 ? {
        n: common_assets._imports_2$2
      } : {}, {
        o: common_vendor.unref(data).grade == 3
      }, common_vendor.unref(data).grade == 3 ? {
        p: common_assets._imports_3$1
      } : {}, {
        q: common_vendor.unref(data).grade == 4
      }, common_vendor.unref(data).grade == 4 ? {
        r: common_assets._imports_4$1
      } : {}, {
        s: common_vendor.unref(data).grade == 5
      }, common_vendor.unref(data).grade == 5 ? {
        t: common_assets._imports_5$1
      } : {}, {
        v: common_vendor.unref(data).grade == 1
      }, common_vendor.unref(data).grade == 1 ? {} : {}, {
        w: common_vendor.unref(data).grade == 2
      }, common_vendor.unref(data).grade == 2 ? {} : {}, {
        x: common_vendor.unref(data).grade == 3
      }, common_vendor.unref(data).grade == 3 ? {} : {}, {
        y: common_vendor.unref(data).grade == 4
      }, common_vendor.unref(data).grade == 4 ? {} : {}, {
        z: common_vendor.unref(data).grade == 5
      }, common_vendor.unref(data).grade == 5 ? {} : {}) : {}, {
        A: common_vendor.unref(flag)
      }, common_vendor.unref(flag) ? {
        B: common_vendor.t(common_vendor.unref(data).createTime ? common_vendor.unref(data).createTime : "")
      } : {}, {
        C: common_assets._imports_6$1,
        D: common_vendor.o(($event) => onPageJump("/pages/memberCenter/personalInformation/personalInformation")),
        E: !common_vendor.unref(flag)
      }, !common_vendor.unref(flag) ? {} : {
        F: common_vendor.t(common_vendor.unref(data).integral ? common_vendor.unref(data).integral : 0)
      }, {
        G: common_vendor.o(($event) => onPageJump("/pages/memberCenter/integralPage/integralEXP")),
        H: !common_vendor.unref(flag)
      }, !common_vendor.unref(flag) ? {} : {
        I: common_vendor.t(couponSize.value ? couponSize.value : 0)
      }, {
        J: common_vendor.o(($event) => onPageJump("/pages/memberCenter/myCoupon")),
        K: common_vendor.p({
          options: common_vendor.unref(list1),
          ["active-icon"]: "circle-filled",
          active: actived.value
        }),
        L: common_vendor.p({
          type: "forward",
          size: "16",
          color: "#CACACA"
        }),
        M: common_vendor.o(($event) => orderJump("/pages/memberCenter/myOrder/myOrder")),
        N: common_assets._imports_7$1,
        O: common_vendor.o(($event) => orderJump("/pages/memberCenter/myOrder/myOrder?stateid=1")),
        P: common_assets._imports_8,
        Q: common_vendor.o(($event) => orderJump("/pages/memberCenter/myOrder/myOrder?stateid=2")),
        R: common_assets._imports_9,
        S: common_vendor.o(($event) => orderJump("/pages/memberCenter/myOrder/myOrder?stateid=3")),
        T: common_assets._imports_10,
        U: common_vendor.o(($event) => orderJump("/pages/memberCenter/myOrder/myOrder?stateid=4")),
        V: common_assets._imports_11,
        W: common_vendor.o(($event) => orderJump("/pages/memberCenter/myOrder/myOrder?stateid=5")),
        X: common_assets._imports_0$4,
        Y: common_vendor.p({
          type: "forward",
          size: "20",
          color: "#CACACA"
        }),
        Z: common_vendor.o(($event) => onPageJump("/pages/memberCenter/address/address")),
        aa: common_assets._imports_13,
        ab: common_vendor.p({
          type: "forward",
          size: "20",
          color: "#CACACA"
        }),
        ac: common_vendor.o(($event) => onPageJump("/pages/memberCenter/myCoupon")),
        ad: common_assets._imports_14,
        ae: common_vendor.p({
          type: "forward",
          size: "20",
          color: "#CACACA"
        }),
        af: common_vendor.o(($event) => onPageJump("/pages/memberCenter/commentPage/commentPage")),
        ag: common_assets._imports_15,
        ah: common_vendor.p({
          type: "forward",
          size: "20",
          color: "#CACACA"
        }),
        ai: common_vendor.o(($event) => onPageJump("/pages/memberCenter/returnsExchanges/returnsPage")),
        aj: common_assets._imports_16,
        ak: common_vendor.p({
          type: "forward",
          size: "20",
          color: "#CACACA"
        }),
        al: common_vendor.o(($event) => onPageJump("/pages/memberCenter/invoice/alertInvoice")),
        am: common_assets._imports_17,
        an: common_vendor.p({
          type: "forward",
          size: "20",
          color: "#CACACA"
        }),
        ao: common_vendor.o(($event) => onPageJump("/pages/memberCenter/collectPage")),
        ap: common_assets._imports_18,
        aq: common_vendor.p({
          type: "forward",
          size: "20",
          color: "#CACACA"
        }),
        ar: common_vendor.o(($event) => onPageJump("/pages/memberCenter/personalInformation/personalInformation")),
        as: common_assets._imports_19,
        at: common_vendor.p({
          type: "forward",
          size: "20",
          color: "#CACACA"
        }),
        av: common_vendor.o(($event) => onPageJump("/pages/memberCenter/findMe")),
        aw: common_assets._imports_20,
        ax: common_vendor.p({
          type: "forward",
          size: "20",
          color: "#CACACA"
        }),
        ay: common_vendor.o(($event) => onPageJumps("/pages/memberCenter/resetPassword?phone=")),
        az: common_vendor.unref(flag)
      }, common_vendor.unref(flag) ? {
        aA: common_vendor.o(goOut)
      } : {}, {
        aB: common_vendor.o(dialogConfirm),
        aC: common_vendor.o(dialogClose),
        aD: common_vendor.p({
          type: _ctx.msgType,
          cancelText: "取消",
          confirmText: "确认",
          content: "确认要退出登录吗?"
        }),
        aE: common_vendor.sr(alertDialog, "151a8dd6-12", {
          "k": "alertDialog"
        }),
        aF: common_vendor.p({
          type: "dialog"
        }),
        aG: shareFlag.value
      }, shareFlag.value ? {
        aH: common_vendor.o(($event) => shareFlag.value = false),
        aI: common_vendor.p({
          type: "closeempty",
          size: "30",
          color: "#fff"
        })
      } : {});
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-151a8dd6"]]);
_sfc_main.__runtimeHooks = 2;
wx.createPage(MiniProgramPage);
