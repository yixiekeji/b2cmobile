"use strict";
const common_vendor = require("../../common/vendor.js");
const common_assets = require("../../common/assets.js");
const api_member = require("../../api/member.js");
if (!Array) {
  const _component_HeadNav = common_vendor.resolveComponent("HeadNav");
  const _easycom_uni_popup_dialog2 = common_vendor.resolveComponent("uni-popup-dialog");
  const _easycom_uni_popup2 = common_vendor.resolveComponent("uni-popup");
  (_component_HeadNav + _easycom_uni_popup_dialog2 + _easycom_uni_popup2)();
}
const _easycom_uni_popup_dialog = () => "../../uni_modules/uni-popup/components/uni-popup-dialog/uni-popup-dialog.js";
const _easycom_uni_popup = () => "../../uni_modules/uni-popup/components/uni-popup/uni-popup.js";
if (!Math) {
  (_easycom_uni_popup_dialog + _easycom_uni_popup)();
}
const _sfc_main = {
  __name: "collectPage",
  setup(__props) {
    const page = common_vendor.ref(1);
    let list = common_vendor.ref([]);
    const loading = common_vendor.ref(false);
    const loadingText = common_vendor.ref("加载中...");
    const id = common_vendor.ref("");
    const alertDialog = common_vendor.ref();
    const dataFlag = common_vendor.ref(false);
    function getList() {
      dataFlag.value = false;
      api_member.collectionList({
        page: page.value
      }).then((res) => {
        console.log(res);
        if (res.data.data.memberCollectionProductList.length == 0 && page.value > 1 && list.value.length > 10) {
          loadingText.value = "到底啦~";
        } else {
          list.value = [...list.value, ...res.data.data.memberCollectionProductList];
          loading.value = false;
        }
        dataFlag.value = true;
      });
    }
    function jumpDetail(val) {
      common_vendor.index.navigateTo({
        url: "/pages/homePage/goodsDetail?id=" + val
      });
    }
    function deleteCollect(val) {
      alertDialog.value.open();
      id.value = val;
    }
    function dialogConfirm() {
      api_member.shopCollectionCancel({
        productId: id.value
      }).then((res) => {
        list.value = [];
        page.value = 1;
        getList();
      });
    }
    function dialogClose() {
      alertDialog.value.close();
    }
    common_vendor.onReachBottom(() => {
      if (list.value.length >= 20 * page.value) {
        page.value += 1;
        loading.value = true;
        loadingText.value = "加载中...";
        setTimeout(() => {
          getList();
        }, 800);
      } else {
        loading.value = true;
        loadingText.value = "到底啦~";
      }
    });
    common_vendor.onPullDownRefresh(() => {
      setTimeout(() => {
        list.value = [];
        page.value = 1;
        getList();
        common_vendor.index.stopPullDownRefresh();
      }, 800);
    });
    common_vendor.onLoad(() => {
      getList();
      common_vendor.index.setNavigationBarTitle({
        title: common_vendor.index.getStorageSync("shopName") ? common_vendor.index.getStorageSync("shopName") : "禾鲜谷"
      });
    });
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: common_vendor.p({
          ["nav-background-color"]: "#fff",
          backImageUrl: "1",
          textContent: "收藏",
          fontSize: "15",
          fontWeight: "600"
        }),
        b: common_vendor.unref(list).length != 0
      }, common_vendor.unref(list).length != 0 ? {
        c: common_vendor.f(common_vendor.unref(list), (item, k0, i0) => {
          return {
            a: _ctx.imageUrl + item.productImg,
            b: common_vendor.o(($event) => jumpDetail(item.productId)),
            c: common_vendor.t(item.productName),
            d: common_vendor.t(item.mallPrice),
            e: common_vendor.o(($event) => jumpDetail(item.productId)),
            f: common_vendor.o(($event) => deleteCollect(item.productId))
          };
        }),
        d: common_assets._imports_0$6
      } : {}, {
        e: loading.value
      }, loading.value ? {
        f: common_vendor.t(loadingText.value)
      } : {}, {
        g: common_vendor.unref(list).length == 0 && dataFlag.value
      }, common_vendor.unref(list).length == 0 && dataFlag.value ? {
        h: common_assets._imports_0$2
      } : {}, {
        i: common_vendor.o(dialogConfirm),
        j: common_vendor.o(dialogClose),
        k: common_vendor.p({
          type: _ctx.msgType,
          cancelText: "取消",
          confirmText: "确认",
          content: "确认要取消收藏吗?"
        }),
        l: common_vendor.sr(alertDialog, "5bcb69d9-1", {
          "k": "alertDialog"
        }),
        m: common_vendor.p({
          type: "dialog"
        })
      });
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-5bcb69d9"]]);
wx.createPage(MiniProgramPage);
