"use strict";
const common_vendor = require("../../common/vendor.js");
const common_assets = require("../../common/assets.js");
const api_member = require("../../api/member.js");
if (!Array) {
  const _component_HeadNav = common_vendor.resolveComponent("HeadNav");
  _component_HeadNav();
}
const _sfc_main = {
  __name: "myCoupon",
  setup(__props) {
    const tabs = common_vendor.reactive([
      {
        id: 2,
        name: "未使用"
      },
      {
        id: 1,
        name: "已经使用"
      },
      {
        id: 0,
        name: "已过期"
      }
    ]);
    const currentTab = common_vendor.ref(1);
    const tabCurrent = common_vendor.ref("tabNum1");
    common_vendor.ref(0);
    const top = common_vendor.ref(0);
    const page = common_vendor.ref(1);
    const sort = common_vendor.ref(1);
    let list = common_vendor.ref([]);
    const loading = common_vendor.ref(false);
    const loadingText = common_vendor.ref("加载中...");
    const couponSn = common_vendor.ref("");
    const flag = common_vendor.ref(false);
    const dataFlag = common_vendor.ref(false);
    function swichMenu(id) {
      if (id == sort.value && id == 3) {
        sort.value = 4;
      } else {
        sort.value = id;
      }
      list.value = [];
      page.value = 1;
      getList();
      currentTab.value = id;
      tabCurrent.value = "tabNum" + id;
    }
    function getList() {
      dataFlag.value = false;
      api_member.couponList({
        page: page.value,
        canUse: sort.value
      }).then((res) => {
        console.log(res);
        if (res.data.data.couponUsers.length == 0 && page.value > 1 && list.value.length > 10) {
          loadingText.value = "到底啦~";
        } else {
          list.value = [...list.value, ...res.data.data.couponUsers];
          console.log(list.value.length);
          loading.value = false;
        }
        flag.value = list.value.some((item) => {
          if (currentTab.value == 0) {
            return !item.use && !item.timeout;
          } else if (currentTab.value == 1) {
            return item.use;
          } else if (currentTab.value == 2) {
            return item.timeout;
          }
        });
        dataFlag.value = true;
      });
    }
    function exchange() {
      if (!couponSn.value) {
        common_vendor.index.showToast({
          title: "请填写兑换码",
          duration: 2e3,
          icon: "none"
        });
        return;
      }
      api_member.convertCoupon({
        couponSn: couponSn.value
      }).then((res) => {
        console.log(res);
        if (res.data.success) {
          common_vendor.index.showToast({
            title: "兑换成功",
            duration: 2e3,
            icon: "none"
          });
          couponSn.value = "";
          search();
        }
      });
    }
    function search() {
      list.value = [];
      page.value = 1;
      getList();
    }
    function jumpPage(val, couponName, useEndTime, couponValue, minAmount, linkProduct) {
      common_vendor.index.navigateTo({
        url: "/pages/memberCenter/couponsPage/couponsPage?id=" + val + "&couponName=" + couponName + "&useEndTime=" + useEndTime + "&couponValue=" + couponValue + "&minAmount=" + minAmount + "&linkProduct=" + linkProduct
      });
    }
    common_vendor.onLoad((option) => {
      getList();
    });
    common_vendor.onReachBottom(() => {
      if (list.value.length >= 20 * page.value) {
        page.value += 1;
        loading.value = true;
        loadingText.value = "加载中...";
        setTimeout(() => {
          getList();
        }, 800);
      } else {
        loading.value = true;
        loadingText.value = "到底啦~";
      }
    });
    common_vendor.onPullDownRefresh(() => {
      setTimeout(() => {
        list.value = [];
        page.value = 1;
        getList();
        common_vendor.index.stopPullDownRefresh();
      }, 800);
    });
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: common_vendor.p({
          ["nav-background-color"]: "#FFFFFF",
          backImageUrl: "1",
          textContent: "优惠券",
          fontSize: "16",
          fontWeight: "600"
        }),
        b: common_vendor.f(tabs, (item, k0, i0) => {
          return {
            a: common_vendor.t(item.name),
            b: common_vendor.n(currentTab.value == item.id - 1 ? "menu-topic-act" : "menu-topic"),
            c: "tabNum" + item.id,
            d: item.id - 1,
            e: common_vendor.o(($event) => swichMenu(item.id - 1), item.id - 1)
          };
        }),
        c: top.value,
        d: currentTab.value == 1
      }, currentTab.value == 1 ? {
        e: couponSn.value,
        f: common_vendor.o(($event) => couponSn.value = $event.detail.value),
        g: common_vendor.o(exchange)
      } : {}, {
        h: common_vendor.f(common_vendor.unref(list), (item, k0, i0) => {
          return common_vendor.e({
            a: common_vendor.t(item.couponName),
            b: common_vendor.t(item.useEndTime),
            c: common_vendor.t(item.couponValue),
            d: common_vendor.t(item.minAmount),
            e: common_vendor.t(item.linkProduct == 1 ? "全部商品" : "部分商品"),
            f: item.linkProduct != 1
          }, item.linkProduct != 1 ? {
            g: common_vendor.o(($event) => jumpPage(item.couponId, item.couponName, item.useEndTime, item.couponValue, item.minAmount, item.linkProduct))
          } : {}, {
            h: item.timeout || item.use
          }, item.timeout || item.use ? {} : {}, {
            i: item.timeout
          }, item.timeout ? {
            j: common_assets._imports_0$10
          } : {}, {
            k: item.use
          }, item.use ? {
            l: common_assets._imports_1$5
          } : {});
        }),
        i: loading.value
      }, loading.value ? {
        j: common_vendor.t(loadingText.value)
      } : {}, {
        k: common_vendor.unref(list).length == 0 && dataFlag.value
      }, common_vendor.unref(list).length == 0 && dataFlag.value ? {
        l: common_assets._imports_2$3,
        m: common_vendor.t(currentTab.value == 1 ? "暂无可用的优惠券" : currentTab.value == 0 ? "暂无已经使用的优惠券" : "暂无过期的优惠券")
      } : {});
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-5cd22d3d"]]);
wx.createPage(MiniProgramPage);
