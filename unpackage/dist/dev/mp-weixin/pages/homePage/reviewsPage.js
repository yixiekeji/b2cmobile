"use strict";
const common_vendor = require("../../common/vendor.js");
const common_assets = require("../../common/assets.js");
const api_reviews = require("../../api/reviews.js");
if (!Array) {
  const _component_HeadNav = common_vendor.resolveComponent("HeadNav");
  const _easycom_uni_icons2 = common_vendor.resolveComponent("uni-icons");
  const _easycom_uni_rate2 = common_vendor.resolveComponent("uni-rate");
  const _easycom_uni_popup2 = common_vendor.resolveComponent("uni-popup");
  (_component_HeadNav + _easycom_uni_icons2 + _easycom_uni_rate2 + _easycom_uni_popup2)();
}
const _easycom_uni_icons = () => "../../uni_modules/uni-icons/components/uni-icons/uni-icons.js";
const _easycom_uni_rate = () => "../../uni_modules/uni-rate/components/uni-rate/uni-rate.js";
const _easycom_uni_popup = () => "../../uni_modules/uni-popup/components/uni-popup/uni-popup.js";
if (!Math) {
  (_easycom_uni_icons + _easycom_uni_rate + _easycom_uni_popup)();
}
const _sfc_main = {
  __name: "reviewsPage",
  setup(__props) {
    const id = common_vendor.ref("");
    const page = common_vendor.ref(1);
    const loading = common_vendor.ref(false);
    const loadingText = common_vendor.ref("加载中...");
    let tabs = common_vendor.ref([
      {
        id: 0,
        name: "全部"
      },
      {
        id: 1,
        name: "有图"
      },
      {
        id: 2,
        name: "好评"
      },
      {
        id: 3,
        name: "中差评"
      }
    ]);
    const currentTab = common_vendor.ref("0");
    const top = common_vendor.ref("0");
    const productCommentList = common_vendor.ref([]);
    const dataFlag = common_vendor.ref(false);
    function getList() {
      dataFlag.value = false;
      api_reviews.commentlist({
        productId: id.value,
        page: page.value,
        type: currentTab.value
      }).then((res) => {
        console.log(res);
        if (res.data.data.productCommentList.length == 0 && page.value > 1 && productCommentList.value.length > 10) {
          loadingText.value = "到底啦~";
        } else {
          let data = res.data.data.productCommentList;
          data.forEach((item) => {
            if (item.replyContent.length > 51) {
              item.textOpenFlag = null;
            } else {
              item.textOpenFlag = null;
            }
          });
          productCommentList.value = [...productCommentList.value, ...data];
          loading.value = false;
        }
        dataFlag.value = true;
      });
    }
    function getCount() {
      tabs.value.forEach((item) => {
        api_reviews.commentlist({
          productId: id.value,
          page: page.value,
          type: item.id
        }).then((res) => {
          item.name = item.name + "(" + res.data.data.rowsCount + ")";
          console.log(res);
        });
      });
    }
    function swichMenu(val) {
      page.value = 1;
      productCommentList.value = [];
      currentTab.value = val;
      getList();
    }
    let popupImg = common_vendor.ref();
    let imgUrl = common_vendor.ref("");
    function openPopup(val) {
      imgUrl.value = val;
      popupImg.value.open();
    }
    function closePopup() {
      popupImg.value.close();
    }
    common_vendor.onLoad((options) => {
      id.value = options.id;
      page.value = 1;
      currentTab.value = 0;
      productCommentList.value = [];
      getList();
      getCount();
    });
    common_vendor.onReachBottom(() => {
      if (productCommentList.value.length >= 30 * page.value) {
        page.value += 1;
        loading.value = true;
        loadingText.value = "加载中...";
        setTimeout(() => {
          getList();
        }, 800);
      } else if (productCommentList.value.length != 0) {
        loading.value = true;
        loadingText.value = "到底啦~";
      }
    });
    common_vendor.onPullDownRefresh(() => {
      setTimeout(() => {
        page.value = 1;
        currentTab.value = 0;
        productCommentList.value = [];
        getList();
        common_vendor.index.stopPullDownRefresh();
      }, 800);
    });
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: common_vendor.p({
          ["nav-background-color"]: "#f7f7f5",
          backImageUrl: "1",
          textContent: "商品评价",
          fontSize: "15",
          fontWeight: "600"
        }),
        b: common_vendor.f(common_vendor.unref(tabs), (item, k0, i0) => {
          return {
            a: common_vendor.t(item.name),
            b: common_vendor.n(currentTab.value == item.id ? "menu-topic-act" : "menu-topic"),
            c: "tabNum" + item.id,
            d: item.id,
            e: common_vendor.o(($event) => swichMenu(item.id), item.id)
          };
        }),
        c: top.value,
        d: common_vendor.f(productCommentList.value, (item, k0, i0) => {
          return common_vendor.e({
            a: item.memberHeadImg && item.memberHeadImg.indexOf("http") != -1
          }, item.memberHeadImg && item.memberHeadImg.indexOf("http") != -1 ? {
            b: item.memberHeadImg
          } : item.memberHeadImg ? {
            d: _ctx.imageUrl + item.memberHeadImg
          } : {
            e: "2301b163-1-" + i0,
            f: common_vendor.p({
              type: "contact",
              size: "40",
              color: "#1C9B64"
            })
          }, {
            c: item.memberHeadImg,
            g: common_vendor.t(item.memberName),
            h: "2301b163-2-" + i0,
            i: common_vendor.p({
              size: "18",
              value: item.grade,
              color: "#bbb",
              ["active-color"]: "#1C9B64"
            }),
            j: common_vendor.t(item.content),
            k: common_vendor.f(item.productCommentPictureList, (itemImg, k1, i1) => {
              return {
                a: _ctx.imageUrl + itemImg.imagePath,
                b: common_vendor.o(($event) => openPopup(itemImg.imagePath))
              };
            }),
            l: item.replyContent
          }, item.replyContent ? common_vendor.e({
            m: common_vendor.t(item.replyContent),
            n: item.textOpenFlag ? "58rpx" : "",
            o: item.textOpenFlag ? 1 : "",
            p: item.textOpenFlag !== null
          }, item.textOpenFlag !== null ? {
            q: common_vendor.t(item.textOpenFlag ? "【展开】" : "【收起】"),
            r: common_vendor.o(($event) => item.textOpenFlag = !item.textOpenFlag)
          } : {}) : {});
        }),
        e: loading.value
      }, loading.value ? {
        f: common_vendor.t(loadingText.value)
      } : {}, {
        g: productCommentList.value.length == 0 && dataFlag.value
      }, productCommentList.value.length == 0 && dataFlag.value ? {
        h: common_assets._imports_0$2
      } : {}, {
        i: common_vendor.o(closePopup),
        j: common_vendor.p({
          type: "closeempty",
          color: "#999",
          size: "18"
        }),
        k: _ctx.imageUrl + common_vendor.unref(imgUrl),
        l: common_vendor.sr(popupImg, "2301b163-3", {
          "k": "popupImg"
        }),
        m: common_vendor.p({
          type: "center"
        })
      });
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-2301b163"]]);
wx.createPage(MiniProgramPage);
