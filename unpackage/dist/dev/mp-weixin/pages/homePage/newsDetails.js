"use strict";
const common_vendor = require("../../common/vendor.js");
const api_homePage = require("../../api/homePage.js");
if (!Array) {
  const _component_HeadNav = common_vendor.resolveComponent("HeadNav");
  _component_HeadNav();
}
const _sfc_main = {
  __name: "newsDetails",
  setup(__props) {
    const id = common_vendor.ref("");
    let data = common_vendor.ref();
    function getDetails() {
      api_homePage.newsDetail({ id: id.value }).then((res) => {
        console.log(res);
        data.value = res.data.data;
      });
    }
    function formatRichText(html) {
      let newContent = html.replace(/<img[^>]*>/gi, function(match, capture) {
        match = match.replace(/style="[^"]+"/gi, "").replace(/style='[^']+'/gi, "");
        match = match.replace(/width="[^"]+"/gi, "").replace(/width='[^']+'/gi, "");
        match = match.replace(/height="[^"]+"/gi, "").replace(/height='[^']+'/gi, "");
        return match;
      });
      newContent = newContent.replace(/style="[^"]+"/gi, function(match, capture) {
        match = match.replace(/width:[^;]+;/gi, "width:100%;").replace(/width:[^;]+;/gi, "width:100%;");
        return match;
      });
      newContent = newContent.replace(/<br[^>]*\/>/gi, "");
      newContent = newContent.replace(/\<img/gi, '<img style="max-width:100%;height:auto;display:block;"');
      return newContent;
    }
    common_vendor.onLoad((options) => {
      id.value = options.id;
      getDetails();
    });
    return (_ctx, _cache) => {
      return {
        a: common_vendor.p({
          ["nav-background-color"]: "#F5F5F7",
          backImageUrl: "1",
          textContent: common_vendor.unref(data).title,
          fontWeight: "600",
          fontSize: "15"
        }),
        b: common_vendor.t(common_vendor.unref(data).title),
        c: common_vendor.t(common_vendor.unref(data).updateTime),
        d: formatRichText(common_vendor.unref(data).content)
      };
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-66323152"]]);
_sfc_main.__runtimeHooks = 2;
wx.createPage(MiniProgramPage);
