"use strict";
const common_vendor = require("../../common/vendor.js");
const common_assets = require("../../common/assets.js");
const api_search = require("../../api/search.js");
if (!Array) {
  const _component_HeadNav = common_vendor.resolveComponent("HeadNav");
  const _easycom_uni_popup_dialog2 = common_vendor.resolveComponent("uni-popup-dialog");
  const _easycom_uni_popup2 = common_vendor.resolveComponent("uni-popup");
  (_component_HeadNav + _easycom_uni_popup_dialog2 + _easycom_uni_popup2)();
}
const _easycom_uni_popup_dialog = () => "../../uni_modules/uni-popup/components/uni-popup-dialog/uni-popup-dialog.js";
const _easycom_uni_popup = () => "../../uni_modules/uni-popup/components/uni-popup/uni-popup.js";
if (!Math) {
  (_easycom_uni_popup_dialog + _easycom_uni_popup)();
}
const _sfc_main = {
  __name: "searchPage",
  setup(__props) {
    const alertDialog = common_vendor.ref();
    const searchLists = common_vendor.ref([]);
    function search(val) {
      console.log(val);
      if (common_vendor.index.getStorageSync("token"))
        ;
      else {
        let time = Date.now();
        searchLogsList.value.unshift({
          keyword: val,
          id: time
        });
        searchLists.value.unshift({
          keyword: val,
          id: time
        });
        common_vendor.index.setStorageSync("searchLists", JSON.stringify(searchLists.value));
      }
      common_vendor.index.navigateTo({
        url: "/pages/homePage/searchResult?data=" + val
      });
    }
    let searchLogsList = common_vendor.ref([]);
    let searchKeywordList = common_vendor.ref([]);
    function getSearchKeyword() {
      api_search.searchIndex().then((res) => {
        console.log("!!!", common_vendor.index.getStorageSync("token"));
        if (common_vendor.index.getStorageSync("token")) {
          searchLogsList.value = res.data.data.logList;
          searchKeywordList.value = res.data.data.keywords;
        } else {
          searchLogsList.value = [...searchLists.value];
          searchKeywordList.value = res.data.data.keywords;
        }
      });
    }
    function deletes() {
      alertDialog.value.open();
    }
    function dialogConfirm() {
      if (common_vendor.index.getStorageSync("token")) {
        api_search.delSearch().then((res) => {
          getSearchKeyword();
        });
      } else {
        searchLists.value = [];
        common_vendor.index.removeStorageSync("searchLists", "");
        getSearchKeyword();
      }
    }
    function dialogClose() {
      alertDialog.value.close();
    }
    function jumpSearchList(data, val) {
      if (data == "")
        return;
      if (val && !common_vendor.index.getStorageSync("token")) {
        searchLogsList.value.forEach((item, index) => {
          if (item.id == val) {
            searchLogsList.value.splice(index, 1);
            searchLogsList.value.unshift(item);
          }
        });
        searchLists.value.forEach((item, index) => {
          if (item.id == val) {
            searchLists.value.splice(index, 1);
            searchLists.value.unshift(item);
          }
        });
        common_vendor.index.setStorageSync("searchLists", JSON.stringify(searchLists.value));
      } else if (!common_vendor.index.getStorageSync("token")) {
        let time = Date.now();
        searchLogsList.value.unshift({
          keyword: data,
          id: time
        });
        searchLists.value.unshift({
          keyword: data,
          id: time
        });
        common_vendor.index.setStorageSync("searchLists", JSON.stringify(searchLists.value));
      }
      getSearchKeyword();
      common_vendor.index.navigateTo({
        url: "/pages/homePage/searchResult?data=" + data
      });
    }
    common_vendor.onShow(() => {
      if (common_vendor.index.getStorageSync("searchLists") && common_vendor.index.getStorageSync("token")) {
        const list = JSON.parse(common_vendor.index.getStorageSync("searchLists"));
        for (let i = list.length - 1; i >= 0; i--) {
          api_search.searchList({
            keyword: list[i].keyword,
            page: 1,
            sort: 0
          }).then((res) => {
          });
        }
        common_vendor.index.removeStorageSync("searchLists", "");
        getSearchKeyword();
      } else if (common_vendor.index.getStorageSync("searchLists")) {
        searchLists.value = JSON.parse(common_vendor.index.getStorageSync("searchLists"));
        getSearchKeyword();
      } else {
        getSearchKeyword();
      }
    });
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: common_vendor.o(search),
        b: common_vendor.p({
          ["nav-background-color"]: "",
          backImageUrl: "3"
        }),
        c: common_vendor.unref(searchLogsList).length != 0
      }, common_vendor.unref(searchLogsList).length != 0 ? {
        d: common_assets._imports_0$11,
        e: common_vendor.o(deletes)
      } : {}, {
        f: common_vendor.f(common_vendor.unref(searchLogsList), (item, k0, i0) => {
          return {
            a: common_vendor.t(item.keyword),
            b: common_vendor.o(($event) => jumpSearchList(item.keyword, item.id))
          };
        }),
        g: common_vendor.f(common_vendor.unref(searchKeywordList), (item, k0, i0) => {
          return {
            a: common_vendor.t(item),
            b: common_vendor.o(($event) => jumpSearchList(item))
          };
        }),
        h: common_vendor.o(dialogConfirm),
        i: common_vendor.o(dialogClose),
        j: common_vendor.p({
          type: _ctx.msgType,
          cancelText: "取消",
          confirmText: "确认",
          content: "确认删除历史搜索记录吗？"
        }),
        k: common_vendor.sr(alertDialog, "25ff4859-1", {
          "k": "alertDialog"
        }),
        l: common_vendor.p({
          type: "dialog"
        })
      });
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-25ff4859"]]);
wx.createPage(MiniProgramPage);
