"use strict";
const common_vendor = require("../../common/vendor.js");
const common_assets = require("../../common/assets.js");
const api_homePage = require("../../api/homePage.js");
if (!Array) {
  const _component_HeadNav = common_vendor.resolveComponent("HeadNav");
  const _easycom_uni_icons2 = common_vendor.resolveComponent("uni-icons");
  const _easycom_uni_popup2 = common_vendor.resolveComponent("uni-popup");
  (_component_HeadNav + _easycom_uni_icons2 + _easycom_uni_popup2)();
}
const _easycom_uni_icons = () => "../../uni_modules/uni-icons/components/uni-icons/uni-icons.js";
const _easycom_uni_popup = () => "../../uni_modules/uni-popup/components/uni-popup/uni-popup.js";
if (!Math) {
  (_easycom_uni_icons + _easycom_uni_popup)();
}
const _sfc_main = {
  __name: "newsPage",
  setup(__props) {
    let tabs = common_vendor.ref([{
      id: 0,
      name: "全部"
    }]);
    const currentTab = common_vendor.ref("0");
    common_vendor.ref("0");
    const sort = common_vendor.ref(0);
    const loading = common_vendor.ref(false);
    const loadingText = common_vendor.ref("加载中...");
    const page = common_vendor.ref(1);
    common_vendor.ref(10);
    let newsList = common_vendor.ref([]);
    const popup = common_vendor.ref();
    const dataFlag = common_vendor.ref(false);
    function swichMenu(id) {
      if (id == sort.value) {
        return;
      } else {
        sort.value = id;
        currentTab.value = id;
        newsList.value = [];
        page.value = 1;
        loading.value = false;
        getList();
      }
    }
    function getList() {
      dataFlag.value = false;
      api_homePage.newslist({
        isHome: 0,
        page: page.value,
        typeId: currentTab.value
      }).then((res) => {
        console.log(res);
        if (res.data.data.newslist.length == 0 && page.value > 1 && newsList.value.length > 10) {
          loadingText.value = "到底啦~";
        } else {
          newsList.value = [...newsList.value, ...res.data.data.newslist];
          loading.value = false;
        }
        dataFlag.value = true;
      });
    }
    function getType() {
      api_homePage.newstype().then((res) => {
        tabs.value = [{
          id: 0,
          name: "全部"
        }, ...res.data.data];
      });
    }
    const type = common_vendor.ref("");
    function swichMore(val) {
      console.log(val);
      type.value = val;
      currentTab.value = val;
    }
    function determine() {
      sort.value = type.value;
      currentTab.value = type.value;
      newsList.value = [];
      page.value = 1;
      loading.value = false;
      getList();
      popup.value.close();
    }
    function resize(val) {
      if (val == "close") {
        currentTab.value = sort.value;
        popup.value.close();
      } else {
        sort.value = 0;
        currentTab.value = 0;
        newsList.value = [];
        page.value = 1;
        loading.value = false;
        getList();
        popup.value.close();
      }
    }
    function newsDetails(val) {
      common_vendor.index.navigateTo({
        url: "/pages/homePage/newsDetails?id=" + val
      });
    }
    common_vendor.onLoad(() => {
      getType();
      getList();
    });
    common_vendor.onReachBottom(() => {
      if (newsList.value.length >= 10 * page.value) {
        page.value += 1;
        loading.value = true;
        loadingText.value = "加载中...";
        setTimeout(() => {
          getList();
        }, 800);
      } else {
        loading.value = true;
        loadingText.value = "到底啦~";
      }
    });
    common_vendor.onPullDownRefresh(() => {
      setTimeout(() => {
        sort.value = 0;
        currentTab.value = 0;
        newsList.value = [];
        page.value = 1;
        getList();
        common_vendor.index.stopPullDownRefresh();
      }, 800);
    });
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: common_vendor.o(_ctx.test),
        b: common_vendor.p({
          ["nav-background-color"]: "#FFFFFF",
          backImageUrl: "1",
          textContent: "新闻",
          fontSize: "16",
          fontWeight: "600"
        }),
        c: common_vendor.f(common_vendor.unref(tabs), (item, k0, i0) => {
          return {
            a: common_vendor.t(item.name),
            b: common_vendor.n(currentTab.value == item.id ? "menu-topic-act" : "menu-topic"),
            c: "tabNum" + item.id,
            d: item.id,
            e: common_vendor.o(($event) => swichMenu(item.id), item.id)
          };
        }),
        d: common_vendor.f(common_vendor.unref(newsList), (list, k0, i0) => {
          return {
            a: common_vendor.t(list.title),
            b: common_vendor.t(list.updateTime),
            c: _ctx.imageUrl + list.corverUrl,
            d: common_vendor.o(($event) => newsDetails(list.id))
          };
        }),
        e: loading.value
      }, loading.value ? {
        f: common_vendor.t(loadingText.value)
      } : {}, {
        g: common_vendor.o(($event) => resize("close")),
        h: common_vendor.p({
          type: "closeempty",
          size: "18",
          color: "#1E1E1F"
        }),
        i: common_vendor.f(common_vendor.unref(tabs), (items, k0, i0) => {
          return {
            a: common_vendor.t(items.name),
            b: common_vendor.o(($event) => swichMore(items.id)),
            c: common_vendor.n(currentTab.value == items.id ? "active" : "")
          };
        }),
        j: common_vendor.unref(tabs).length % 3 == 1 || common_vendor.unref(tabs).length % 3 == 2
      }, common_vendor.unref(tabs).length % 3 == 1 || common_vendor.unref(tabs).length % 3 == 2 ? {} : {}, {
        k: common_vendor.unref(tabs).length % 3 == 1
      }, common_vendor.unref(tabs).length % 3 == 1 ? {} : {}, {
        l: common_vendor.o(resize),
        m: common_vendor.o(determine),
        n: common_vendor.sr(popup, "1e2d3b1a-1", {
          "k": "popup"
        }),
        o: common_vendor.o(_ctx.change),
        p: common_vendor.p({
          ["background-color"]: "#fff",
          ["border-radius"]: "48rpx 48rpx 0px 0px",
          type: "bottom"
        }),
        q: common_vendor.unref(newsList).length == 0 && dataFlag.value
      }, common_vendor.unref(newsList).length == 0 && dataFlag.value ? {
        r: common_assets._imports_0$2
      } : {});
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-1e2d3b1a"]]);
_sfc_main.__runtimeHooks = 2;
wx.createPage(MiniProgramPage);
