"use strict";
const common_vendor = require("../../common/vendor.js");
const common_assets = require("../../common/assets.js");
const api_goodsDetail = require("../../api/goodsDetail.js");
const api_member = require("../../api/member.js");
if (!Array) {
  const _component_HeadNav = common_vendor.resolveComponent("HeadNav");
  _component_HeadNav();
}
const _sfc_main = {
  __name: "couponList",
  setup(__props) {
    const tabs = common_vendor.reactive([
      {
        id: 1,
        name: "全部"
      },
      {
        id: 2,
        name: "即将到期"
      },
      {
        id: 3,
        name: "面额最大"
      }
    ]);
    const currentTab = common_vendor.ref(0);
    const tabCurrent = common_vendor.ref("tabNum1");
    common_vendor.ref(0);
    const top = common_vendor.ref(0);
    const page = common_vendor.ref(1);
    const sort = common_vendor.ref(0);
    let list = common_vendor.ref([]);
    const loading = common_vendor.ref(false);
    const loadingText = common_vendor.ref("加载中...");
    common_vendor.ref("");
    common_vendor.ref(false);
    const dataFlag = common_vendor.ref(false);
    function swichMenu(val) {
      if (val != sort.value) {
        sort.value = val;
        currentTab.value = val;
        tabCurrent.value = "tabNum" + val;
      }
      list.value = [];
      page.value = 1;
      getList();
    }
    function getList() {
      dataFlag.value = false;
      api_member.wdlcoupon({
        page: page.value,
        sort: sort.value
      }).then((res) => {
        console.log(res);
        if (res.data.data.couponList.length == 0 && page.value > 1 && list.value.length > 10) {
          loadingText.value = "到底啦~";
        } else {
          list.value = [...list.value, ...res.data.data.couponList];
          loading.value = false;
        }
        dataFlag.value = true;
      });
    }
    function getCoupon(val) {
      api_goodsDetail.reveivecoupon({
        couponId: val
      }).then((res) => {
        if (res.data.success) {
          common_vendor.index.showToast({
            icon: "none",
            duration: 3e3,
            title: `领取成功`
          });
          list.value = [];
          page.value = 1;
          getList();
        }
      });
    }
    function jumpPage(val, couponName, useEndTime, couponValue, minAmount, linkProduct) {
      common_vendor.index.navigateTo({
        url: "/pages/memberCenter/couponsPage/couponsPage?id=" + val + "&couponName=" + couponName + "&useEndTime=" + useEndTime + "&couponValue=" + couponValue + "&minAmount=" + minAmount + "&linkProduct=" + linkProduct
      });
    }
    common_vendor.onLoad((option) => {
      getList();
    });
    common_vendor.onReachBottom(() => {
      if (list.value.length >= 20 * page.value) {
        page.value += 1;
        loading.value = true;
        loadingText.value = "加载中...";
        setTimeout(() => {
          getList();
        }, 800);
      } else {
        loading.value = true;
        loadingText.value = "到底啦~";
      }
    });
    common_vendor.onPullDownRefresh(() => {
      setTimeout(() => {
        list.value = [];
        page.value = 1;
        getList();
        common_vendor.index.stopPullDownRefresh();
      }, 800);
    });
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: common_vendor.p({
          ["nav-background-color"]: "#FFFFFF",
          backImageUrl: "1",
          textContent: "优惠券",
          fontSize: "16",
          fontWeight: "600"
        }),
        b: common_vendor.f(tabs, (item, k0, i0) => {
          return {
            a: common_vendor.t(item.name),
            b: common_vendor.n(currentTab.value == item.id - 1 ? "menu-topic-act" : "menu-topic"),
            c: "tabNum" + item.id,
            d: item.id - 1,
            e: common_vendor.o(($event) => swichMenu(item.id - 1), item.id - 1)
          };
        }),
        c: top.value,
        d: common_vendor.f(common_vendor.unref(list), (item, k0, i0) => {
          return common_vendor.e({
            a: common_vendor.t(item.couponName),
            b: common_vendor.t(item.useEndTime),
            c: common_vendor.t(item.couponValue),
            d: common_vendor.t(item.minAmount),
            e: common_vendor.t(item.linkProduct == 1 ? "全部商品" : "部分商品"),
            f: item.linkProduct != 1
          }, item.linkProduct != 1 ? {
            g: common_vendor.o(($event) => jumpPage(item.id, item.couponName, item.useEndTime, item.couponValue, item.minAmount, item.linkProduct))
          } : {}, {
            h: common_vendor.o(($event) => getCoupon(item.id)),
            i: item.timeout || item.use
          }, item.timeout || item.use ? {} : {}, {
            j: item.timeout
          }, item.timeout ? {
            k: common_assets._imports_0$10
          } : {}, {
            l: item.use
          }, item.use ? {
            m: common_assets._imports_1$5
          } : {});
        }),
        e: loading.value
      }, loading.value ? {
        f: common_vendor.t(loadingText.value)
      } : {}, {
        g: common_vendor.unref(list).length == 0 && dataFlag.value
      }, common_vendor.unref(list).length == 0 && dataFlag.value ? {
        h: common_assets._imports_2$3
      } : {});
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-3df517cc"]]);
wx.createPage(MiniProgramPage);
