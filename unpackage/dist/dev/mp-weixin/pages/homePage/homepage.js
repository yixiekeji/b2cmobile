"use strict";
const common_vendor = require("../../common/vendor.js");
const common_assets = require("../../common/assets.js");
const api_homePage = require("../../api/homePage.js");
const store_address = require("../../store/address.js");
if (!Array) {
  const _component_HeadNav = common_vendor.resolveComponent("HeadNav");
  const _easycom_uni_swiper_dot2 = common_vendor.resolveComponent("uni-swiper-dot");
  const _easycom_uni_grid_item2 = common_vendor.resolveComponent("uni-grid-item");
  const _easycom_uni_grid2 = common_vendor.resolveComponent("uni-grid");
  const _easycom_uni_icons2 = common_vendor.resolveComponent("uni-icons");
  const _easycom_uni_popup_share2 = common_vendor.resolveComponent("uni-popup-share");
  const _easycom_uni_popup2 = common_vendor.resolveComponent("uni-popup");
  (_component_HeadNav + _easycom_uni_swiper_dot2 + _easycom_uni_grid_item2 + _easycom_uni_grid2 + _easycom_uni_icons2 + _easycom_uni_popup_share2 + _easycom_uni_popup2)();
}
const _easycom_uni_swiper_dot = () => "../../uni_modules/uni-swiper-dot/components/uni-swiper-dot/uni-swiper-dot.js";
const _easycom_uni_grid_item = () => "../../uni_modules/uni-grid/components/uni-grid-item/uni-grid-item.js";
const _easycom_uni_grid = () => "../../uni_modules/uni-grid/components/uni-grid/uni-grid.js";
const _easycom_uni_icons = () => "../../uni_modules/uni-icons/components/uni-icons/uni-icons.js";
const _easycom_uni_popup_share = () => "../../uni_modules/uni-popup/components/uni-popup-share/uni-popup-share.js";
const _easycom_uni_popup = () => "../../uni_modules/uni-popup/components/uni-popup/uni-popup.js";
if (!Math) {
  (_easycom_uni_swiper_dot + _easycom_uni_grid_item + _easycom_uni_grid + _easycom_uni_icons + _easycom_uni_popup_share + _easycom_uni_popup)();
}
const _sfc_main = {
  __name: "homepage",
  setup(__props) {
    store_address.address().setAddress();
    function test(e) {
      console.log(e);
    }
    common_vendor.ref("易写科技服务规则");
    common_vendor.ref([]);
    common_vendor.ref([]);
    let newslists = common_vendor.ref([]);
    let floorList = common_vendor.ref([]);
    common_vendor.ref([]);
    common_vendor.ref([]);
    common_vendor.ref([]);
    let bannerList = common_vendor.ref([]);
    const current = common_vendor.ref(0);
    let modes = common_vendor.ref("dot");
    const dotsStyles = common_vendor.reactive({
      backgroundColor: "rgba(0, 0, 0,0.3)",
      border: "0px rgba(83, 200, 249,0.3) solid",
      color: "#fff",
      selectedBackgroundColor: "#1C9B64",
      selectedBorder: "2rpx solid #FFFFFF"
    });
    function change(e) {
      current.value = e.detail.current;
    }
    common_vendor.ref(true);
    common_vendor.ref(true);
    common_vendor.ref("0");
    const shareFlag = common_vendor.ref(false);
    let LikeList = common_vendor.ref([]);
    getList();
    function getList() {
      api_homePage.floor({}).then((res) => {
        floorList.value = res.data.data;
      });
      api_homePage.indexLike({}).then((res) => {
        LikeList.value = res.data.data;
      });
      api_homePage.banner().then((res) => {
        bannerList.value = res.data.data;
      });
      api_homePage.newslist({
        isHome: 1,
        page: 1,
        typeId: 0
      }).then((res) => {
        newslists.value = res.data.data.newslist;
      });
    }
    common_vendor.ref(-1);
    function jumpDetails(val) {
      common_vendor.index.navigateTo({
        url: "/pages/homePage/goodsDetail?id=" + val
      });
    }
    function jump(val) {
      common_vendor.index.navigateTo({
        url: val
      });
    }
    function newsDetails(val) {
      common_vendor.index.navigateTo({
        url: "/pages/homePage/newsDetails?id=" + val
      });
    }
    function pageJumps(val) {
      common_vendor.index.navigateTo({
        url: val
      });
    }
    function pageJump(val) {
      if (common_vendor.index.getStorageSync("token")) {
        common_vendor.index.navigateTo({
          url: val
        });
      } else {
        common_vendor.index.navigateTo({
          url: "/pages/loginInterface/passwordLogin"
        });
      }
    }
    function newsJump(val) {
      common_vendor.index.navigateTo({
        url: val
      });
    }
    function onTabbarJump(val) {
      common_vendor.index.switchTab({
        url: val
      });
    }
    const share = common_vendor.ref();
    function shareToggle() {
    }
    function selected(item, index) {
      if (item.index == 0) {
        common_vendor.index.share({
          provider: "weixin",
          scene: "WXSceneSession",
          type: 5,
          imageUrl: "http://img.yixiekeji.com:8080/b2cimage/images/mobile/mobile/cad931d2-2bca-4195-8a1c-08ac29ee653b.png",
          title: "易写科技商城",
          miniProgram: {
            id: "gh_1da0d5922daf",
            //微信小程序原始id
            path: "pages/homePage/homepage",
            //点击链接进入的页面
            type: 2,
            //微信小程序版本类型，可取值： 0-正式版； 1-测试版； 2-体验版。 默认值为0。
            webUrl: "http://uniapp.dcloud.io"
            //兼容低版本的网页链接
          },
          success: function(res) {
            console.log("success:" + JSON.stringify(res));
          },
          fail: function(err) {
            console.log("fail:" + JSON.stringify(err));
          }
        });
      }
    }
    common_vendor.onShareAppMessage((res) => {
      console.log(res);
      return {
        title: "易写科技小程序",
        path: `/pages/homePage/homepage`
        // imageUrl: '/static/imgs/mylogo.png'
      };
    });
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: common_vendor.o(test),
        b: common_vendor.p({
          ["nav-background-color"]: "#f5f5f7",
          backImageUrl: "8",
          textAlign: "left"
        }),
        c: common_vendor.unref(bannerList).length != 0
      }, common_vendor.unref(bannerList).length != 0 ? {
        d: common_vendor.f(common_vendor.unref(bannerList), (item, index, i0) => {
          return {
            a: _ctx.imageUrl + item.image,
            b: index,
            c: common_vendor.o(($event) => pageJumps(item.linkUrl), index)
          };
        }),
        e: common_vendor.o(change),
        f: common_vendor.p({
          info: common_vendor.unref(bannerList),
          current: current.value,
          field: "content",
          mode: common_vendor.unref(modes),
          ["dots-styles"]: dotsStyles
        })
      } : {}, {
        g: common_assets._imports_0,
        h: common_vendor.o(($event) => onTabbarJump("/pages/classification/classification")),
        i: common_assets._imports_1,
        j: common_vendor.o(($event) => onTabbarJump("/pages/memberCenter/memberCenter")),
        k: common_assets._imports_2,
        l: common_vendor.o(($event) => pageJump("/pages/memberCenter/myOrder/myOrder")),
        m: common_assets._imports_3,
        n: common_vendor.o(shareToggle),
        o: common_assets._imports_4,
        p: common_vendor.o(($event) => pageJumps("/pages/memberCenter/integralPage/integralPage")),
        q: common_assets._imports_5,
        r: common_vendor.o(($event) => pageJumps("/pages/homePage/couponList")),
        s: common_assets._imports_6,
        t: common_vendor.o(($event) => onTabbarJump("/pages/shoppingCart/ShoppingCart")),
        v: common_assets._imports_7,
        w: common_vendor.o(($event) => pageJump("/pages/memberCenter/collectPage")),
        x: common_vendor.o(change),
        y: common_vendor.p({
          column: 4,
          highlight: false,
          showBorder: false
        }),
        z: common_vendor.f(common_vendor.unref(floorList), (item, k0, i0) => {
          return {
            a: common_vendor.t(item.name),
            b: _ctx.imageUrl + item.advImage,
            c: common_vendor.o(($event) => jump(item.advLinkUrl)),
            d: common_vendor.f(item.floorDataList, (itemGoods, k1, i1) => {
              return common_vendor.e({
                a: _ctx.imageUrl + itemGoods.product.masterImg,
                b: common_vendor.t(itemGoods.product.mallPrice),
                c: common_vendor.t(itemGoods.product.name),
                d: common_vendor.t(itemGoods.product.keyword),
                e: common_vendor.t(itemGoods.product.sellerName),
                f: itemGoods.product.level === 1
              }, itemGoods.product.level === 1 ? {} : itemGoods.product.level === 2 ? {} : itemGoods.product.level === 3 ? {} : itemGoods.product.level === 4 ? {} : itemGoods.product.level === 5 ? {} : itemGoods.product.level === 6 ? {} : {}, {
                g: itemGoods.product.level === 2,
                h: itemGoods.product.level === 3,
                i: itemGoods.product.level === 4,
                j: itemGoods.product.level === 5,
                k: itemGoods.product.level === 6,
                l: common_vendor.o(($event) => jumpDetails(itemGoods.product.id))
              });
            })
          };
        }),
        A: common_vendor.f(common_vendor.unref(LikeList), (itemGoods, k0, i0) => {
          return common_vendor.e({
            a: _ctx.imageUrl + itemGoods.masterImg,
            b: common_vendor.t(itemGoods.mallPrice),
            c: common_vendor.t(itemGoods.name),
            d: common_vendor.t(itemGoods.keyword),
            e: common_vendor.t(itemGoods.sellerName),
            f: itemGoods.level === 1
          }, itemGoods.level === 1 ? {} : itemGoods.level === 2 ? {} : itemGoods.level === 3 ? {} : itemGoods.level === 4 ? {} : itemGoods.level === 5 ? {} : itemGoods.level === 6 ? {} : {}, {
            g: itemGoods.level === 2,
            h: itemGoods.level === 3,
            i: itemGoods.level === 4,
            j: itemGoods.level === 5,
            k: itemGoods.level === 6,
            l: common_vendor.o(($event) => jumpDetails(itemGoods.id))
          });
        }),
        B: common_vendor.unref(newslists).length > 0
      }, common_vendor.unref(newslists).length > 0 ? {
        C: common_vendor.o(($event) => jump()),
        D: common_vendor.p({
          type: "right",
          size: "17",
          color: "#999999"
        }),
        E: common_vendor.o(($event) => newsJump("/pages/homePage/newsPage")),
        F: common_vendor.f(common_vendor.unref(newslists), (item, k0, i0) => {
          return {
            a: common_vendor.t(item.abstracts),
            b: common_vendor.t(item.createTime),
            c: _ctx.imageUrl + item.corverUrl,
            d: common_vendor.o(($event) => newsDetails(item.id))
          };
        })
      } : {}, {
        G: common_vendor.o(selected),
        H: common_vendor.sr(share, "b4adc7da-12", {
          "k": "share"
        }),
        I: common_vendor.p({
          type: "share",
          safeArea: true,
          backgroundColor: "#fff"
        }),
        J: shareFlag.value
      }, shareFlag.value ? {
        K: common_vendor.o(($event) => shareFlag.value = false),
        L: common_vendor.p({
          type: "closeempty",
          size: "30",
          color: "#fff"
        })
      } : {});
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-b4adc7da"]]);
_sfc_main.__runtimeHooks = 2;
wx.createPage(MiniProgramPage);
