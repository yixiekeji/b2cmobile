"use strict";
const common_vendor = require("../../common/vendor.js");
const common_assets = require("../../common/assets.js");
const api_search = require("../../api/search.js");
if (!Array) {
  const _component_HeadNav = common_vendor.resolveComponent("HeadNav");
  _component_HeadNav();
}
const _sfc_main = {
  __name: "searchResult",
  setup(__props) {
    const tabs = common_vendor.reactive([
      {
        id: 1,
        name: "综合"
      },
      {
        id: 2,
        name: "销量"
      },
      {
        id: 3,
        name: "上新"
      },
      {
        id: 4,
        name: "价格"
      }
    ]);
    const currentTab = common_vendor.ref(0);
    const tabCurrent = common_vendor.ref("tabNum1");
    common_vendor.ref(0);
    const top = common_vendor.ref(0);
    common_vendor.ref(1);
    const searchValue = common_vendor.ref("");
    const pageValue = common_vendor.ref(1);
    const sort = common_vendor.ref(0);
    let list = common_vendor.ref([]);
    common_vendor.ref("");
    const loading = common_vendor.ref(false);
    const loadingText = common_vendor.ref("加载中...");
    const searchValues = common_vendor.ref("");
    const dataFlag = common_vendor.ref(false);
    function swichMenu(id) {
      if (id == sort.value && id == 3) {
        sort.value = 4;
      } else {
        sort.value = id;
      }
      list.value = [];
      pageValue.value = 1;
      getList();
      currentTab.value = id;
      tabCurrent.value = "tabNum" + id;
    }
    function getList() {
      dataFlag.value = false;
      api_search.searchList({
        keyword: searchValue.value,
        page: pageValue.value,
        sort: sort.value
      }).then((res) => {
        console.log(res.data.data.producList);
        if (res.data.data.producList.length == 0 && pageValue.value > 1 && list.value.length > 10) {
          loadingText.value = "到底啦~";
        } else {
          list.value = [...list.value, ...res.data.data.producList];
          console.log(list.value.length);
          loading.value = false;
        }
        dataFlag.value = true;
      });
    }
    function search(val) {
      list.value = [];
      pageValue.value = 1;
      searchValue.value = val;
      if (common_vendor.index.getStorageSync("token"))
        ;
      else {
        let time = Date.now();
        let searchLists = JSON.parse(common_vendor.index.getStorageSync("searchLists"));
        searchLists.unshift({
          keyword: val,
          id: time
        });
        common_vendor.index.setStorageSync("searchLists", JSON.stringify(searchLists));
      }
      getList();
    }
    common_vendor.onLoad((option) => {
      console.log(option);
      searchValue.value = option.data;
      searchValues.value = option.data;
      getList();
    });
    function jumpDetail(id) {
      common_vendor.index.navigateTo({
        url: "/pages/homePage/goodsDetail?id=" + id
      });
    }
    common_vendor.onReachBottom(() => {
      if (list.value.length >= 20 * pageValue.value) {
        pageValue.value += 1;
        loading.value = true;
        loadingText.value = "加载中...";
        setTimeout(() => {
          getList();
        }, 800);
      } else {
        loading.value = true;
        loadingText.value = "到底啦~";
      }
    });
    common_vendor.onPullDownRefresh(() => {
      setTimeout(() => {
        list.value = [];
        pageValue.value = 1;
        getList();
        common_vendor.index.stopPullDownRefresh();
      }, 800);
    });
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: common_vendor.o(search),
        b: common_vendor.p({
          ["nav-background-color"]: "#f5f5f7",
          backImageUrl: "3",
          searchValues: searchValues.value
        }),
        c: common_vendor.f(tabs, (item, k0, i0) => {
          return common_vendor.e({
            a: common_vendor.t(item.name),
            b: item.name == "价格"
          }, item.name == "价格" ? {
            c: common_vendor.n(sort.value == 3 ? "topActive" : "top"),
            d: common_vendor.n(sort.value == 4 ? "bottomActive" : "bottom")
          } : {}, {
            e: common_vendor.n(currentTab.value == item.id - 1 ? "menu-topic-act" : "menu-topic"),
            f: "tabNum" + item.id,
            g: item.id - 1,
            h: common_vendor.o(($event) => swichMenu(item.id - 1), item.id - 1)
          });
        }),
        d: top.value,
        e: common_vendor.unref(list).length != 0
      }, common_vendor.unref(list).length != 0 ? {
        f: common_vendor.f(common_vendor.unref(list), (item, k0, i0) => {
          return {
            a: _ctx.imageUrl + item.masterImg,
            b: common_vendor.t(item.name),
            c: common_vendor.t(item.mallPrice),
            d: common_vendor.o(($event) => jumpDetail(item.id))
          };
        })
      } : {}, {
        g: loading.value
      }, loading.value ? {
        h: common_vendor.t(loadingText.value)
      } : {}, {
        i: common_vendor.unref(list).length == 0 && dataFlag.value
      }, common_vendor.unref(list).length == 0 && dataFlag.value ? {
        j: common_assets._imports_0$2
      } : {});
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-5b9ecdcd"]]);
wx.createPage(MiniProgramPage);
