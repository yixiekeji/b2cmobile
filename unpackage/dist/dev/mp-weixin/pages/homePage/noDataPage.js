"use strict";
const common_vendor = require("../../common/vendor.js");
const common_assets = require("../../common/assets.js");
const _sfc_main = {
  data() {
    return {};
  },
  methods: {
    // 跳转到首页
    goHome() {
      common_vendor.index.switchTab({
        url: "/pages/homePage/homepage"
      });
    }
  },
  onLoad() {
    common_vendor.index.setNavigationBarTitle({
      title: common_vendor.index.getStorageSync("shopName") ? common_vendor.index.getStorageSync("shopName") : "禾鲜谷"
    });
  }
};
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: common_assets._imports_0$1,
    b: common_vendor.o((...args) => $options.goHome && $options.goHome(...args))
  };
}
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__scopeId", "data-v-cdbfdc85"]]);
wx.createPage(MiniProgramPage);
