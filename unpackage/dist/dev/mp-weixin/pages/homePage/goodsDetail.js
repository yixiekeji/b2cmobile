"use strict";
const common_vendor = require("../../common/vendor.js");
const common_assets = require("../../common/assets.js");
const api_goodsDetail = require("../../api/goodsDetail.js");
require("../../store/login.js");
const api_member = require("../../api/member.js");
const api_reviews = require("../../api/reviews.js");
const store_order = require("../../store/order.js");
if (!Array) {
  const _component_HeadNav = common_vendor.resolveComponent("HeadNav");
  const _easycom_uni_swiper_dot2 = common_vendor.resolveComponent("uni-swiper-dot");
  const _easycom_uni_icons2 = common_vendor.resolveComponent("uni-icons");
  const _easycom_uni_list_item2 = common_vendor.resolveComponent("uni-list-item");
  const _easycom_pick_regions2 = common_vendor.resolveComponent("pick-regions");
  const _easycom_uni_rate2 = common_vendor.resolveComponent("uni-rate");
  const _easycom_uni_goods_nav2 = common_vendor.resolveComponent("uni-goods-nav");
  const _easycom_uni_data_checkbox2 = common_vendor.resolveComponent("uni-data-checkbox");
  const _easycom_uni_number_box2 = common_vendor.resolveComponent("uni-number-box");
  const _easycom_uni_popup2 = common_vendor.resolveComponent("uni-popup");
  const _easycom_uni_popup_share2 = common_vendor.resolveComponent("uni-popup-share");
  (_component_HeadNav + _easycom_uni_swiper_dot2 + _easycom_uni_icons2 + _easycom_uni_list_item2 + _easycom_pick_regions2 + _easycom_uni_rate2 + _easycom_uni_goods_nav2 + _easycom_uni_data_checkbox2 + _easycom_uni_number_box2 + _easycom_uni_popup2 + _easycom_uni_popup_share2)();
}
const _easycom_uni_swiper_dot = () => "../../uni_modules/uni-swiper-dot/components/uni-swiper-dot/uni-swiper-dot.js";
const _easycom_uni_icons = () => "../../uni_modules/uni-icons/components/uni-icons/uni-icons.js";
const _easycom_uni_list_item = () => "../../uni_modules/uni-list/components/uni-list-item/uni-list-item.js";
const _easycom_pick_regions = () => "../../components/pick-regions/pick-regions.js";
const _easycom_uni_rate = () => "../../uni_modules/uni-rate/components/uni-rate/uni-rate.js";
const _easycom_uni_goods_nav = () => "../../uni_modules/uni-goods-nav/components/uni-goods-nav/uni-goods-nav.js";
const _easycom_uni_data_checkbox = () => "../../uni_modules/uni-data-checkbox/components/uni-data-checkbox/uni-data-checkbox.js";
const _easycom_uni_number_box = () => "../../uni_modules/uni-number-box/components/uni-number-box/uni-number-box.js";
const _easycom_uni_popup = () => "../../uni_modules/uni-popup/components/uni-popup/uni-popup.js";
const _easycom_uni_popup_share = () => "../../uni_modules/uni-popup/components/uni-popup-share/uni-popup-share.js";
if (!Math) {
  (_easycom_uni_swiper_dot + _easycom_uni_icons + _easycom_uni_list_item + _easycom_pick_regions + _easycom_uni_rate + _easycom_uni_goods_nav + _easycom_uni_data_checkbox + _easycom_uni_number_box + _easycom_uni_popup + _easycom_uni_popup_share)();
}
const _sfc_main = {
  __name: "goodsDetail",
  setup(__props) {
    const shareFlag = common_vendor.ref(false);
    let optiones = common_vendor.ref([
      {
        icon: "home",
        text: "首页"
      },
      {
        icon: "heart",
        text: "收藏"
      },
      {
        icon: "cart",
        text: "购物车",
        // info: '1',
        infoBackgroundColor: "#1C9B64"
      }
    ]);
    let customButtonGroup = common_vendor.ref([
      {
        text: "加入购物车",
        backgroundColor: "#ff0000",
        color: "#fff"
      },
      {
        text: "立即购买",
        backgroundColor: "#ffa200",
        color: "#fff"
      }
    ]);
    let dotsStyles = common_vendor.ref({
      backgroundColor: "rgba(0, 0, 0,0.3)",
      border: "0px rgba(83, 200, 249,0.3) solid",
      color: "#fff",
      selectedBackgroundColor: "#1C9B64",
      selectedBorder: "2rpx solid #FFFFFF"
    });
    const mode = common_vendor.ref("default");
    common_vendor.ref([]);
    common_vendor.ref("请选择省市区");
    common_vendor.ref({
      siteAddress: ""
    });
    const current = common_vendor.ref(0);
    common_vendor.ref([]);
    let regionName = common_vendor.ref("");
    common_vendor.ref(["广东省", "广州市", "番禺区"]);
    const defaultRegionCode = common_vendor.ref("440113");
    let details = common_vendor.ref({});
    let norm1s = common_vendor.ref([]);
    const norm1 = common_vendor.ref("");
    const norm1Value = common_vendor.ref("");
    let norm2s = common_vendor.ref([]);
    const norm2 = common_vendor.ref("");
    const norm2Value = common_vendor.ref("");
    common_vendor.ref("");
    let masterImg = common_vendor.ref([]);
    const masterImgs = common_vendor.ref("");
    const mallPrice = common_vendor.ref("");
    const name = common_vendor.ref("");
    const description = common_vendor.ref("");
    const stock = common_vendor.ref("");
    const isCollenction = common_vendor.ref(false);
    const productId = common_vendor.ref("");
    const max = common_vendor.ref(1);
    const min = common_vendor.ref(0);
    const numValue = common_vendor.ref(1);
    const productGoodId = common_vendor.ref("");
    const id = common_vendor.ref("");
    const chooseValue = common_vendor.ref("");
    const isAddCart = common_vendor.ref("");
    const isSelf = common_vendor.ref("");
    const report = common_vendor.ref("");
    const sales = common_vendor.ref("0");
    const popup = common_vendor.ref();
    let EffectiveCoupon = common_vendor.ref([]);
    common_vendor.ref(false);
    common_vendor.ref("");
    common_vendor.ref("");
    common_vendor.ref(true);
    common_vendor.ref(1);
    common_vendor.ref(common_vendor.index.getStorageSync("phone"));
    common_vendor.ref(common_vendor.index.getStorageSync("name"));
    common_vendor.ref(common_vendor.index.getStorageSync("shopName"));
    common_vendor.ref("Hello");
    common_vendor.ref(false);
    common_vendor.ref("rotate1");
    common_vendor.ref("white");
    common_vendor.ref(true);
    const productCommentList = common_vendor.ref([]);
    const state = common_vendor.ref("");
    let productAttrList = common_vendor.ref([]);
    function change(e) {
      console.log(e);
      current.value = e.detail.current;
    }
    let free = common_vendor.ref("");
    function handleGetRegion(region) {
      region.value = region;
      regionName.value = region.value.map((item) => item.regionName).join(" ");
      console.log(region.value, regionName.value);
      api_goodsDetail.getTransportPrice({
        cityId: region.value[region.value.length - 1].id,
        num: 1,
        productId: id.value,
        productGoodsId: productGoodId.value
      }).then((res) => {
        console.log(res);
        free.value = res.data.data;
      });
    }
    function norm2Change(val) {
      getinfor();
      choose();
    }
    function norm1Change(val) {
      getinfor();
      choose();
    }
    function getinfor() {
      if (norm1.value == 99 && norm2.value == 99) {
        return;
      } else if (norm2.value == 99) {
        setTimeout(() => {
          api_goodsDetail.detailgoods({
            normName: chooseValue.value,
            productId: productId.value
          }).then((res) => {
            mallPrice.value = res.data.data.mallPrice;
            if (res.data.data.stock == 0) {
              min.value = 0;
              max.value = 0;
              numValue.value = 0;
            } else {
              min.value = 1;
              max.value = res.data.data.stock;
              numValue.value = 1;
            }
            productGoodId.value = res.data.data.id;
            stock.value = res.data.data.stock == 0 ? "暂无库存" : "剩余" + res.data.data.stock + "件";
          });
        }, 100);
      } else if (norm1.value != 99 && norm2.value != 99) {
        if (norm1Value.value != "" && norm2Value.value != "") {
          setTimeout(() => {
            api_goodsDetail.detailgoods({
              normName: chooseValue.value,
              productId: productId.value
            }).then((res) => {
              console.log(res);
              mallPrice.value = res.data.data.mallPrice;
              if (res.data.data.stock == 0) {
                min.value = 0;
                max.value = 0;
                numValue.value = 0;
              } else {
                min.value = 1;
                max.value = res.data.data.stock;
                numValue.value = 1;
              }
              productGoodId.value = res.data.data.id;
              stock.value = res.data.data.stock == 0 ? "暂无库存" : "剩余" + res.data.data.stock + "件";
            });
          }, 100);
        }
      }
    }
    const favorableRate = common_vendor.ref("");
    function addCart() {
      productId.value = id.value;
      norm1s.value = [];
      norm2s.value = [];
      api_goodsDetail.productDetail({
        productId: id.value
      }).then((res) => {
        if (res.data.data.product.isNorm == 0) {
          norm1.value = 99;
          norm2.value = 99;
          productGoodId.value = res.data.data.product.productGoodsList[0].id;
        } else if (res.data.data.product.isNorm == 1) {
          norm1.value = res.data.data.norm1;
          res.data.data.norm1s.forEach((item) => {
            norm1s.value.push({
              text: item,
              value: item
            });
            norm1Value.value = norm1s.value[0].value;
          });
          if (res.data.data.norm2) {
            norm2.value = res.data.data.norm2;
            res.data.data.norm2s.forEach((item) => {
              norm2s.value.push({
                text: item,
                value: item
              });
              norm2Value.value = norm2s.value[0].value;
            });
          } else {
            norm2.value = 99;
          }
          choose();
          getinfor();
        }
        favorableRate.value = res.data.data.favorableRate * 100 + "%";
        stock.value = res.data.data.product.productGoodsList[0].stock == 0 ? "暂无库存" : "剩余" + res.data.data.product.productGoodsList[0].stock + "件";
        name.value = res.data.data.product.name;
        isCollenction.value = res.data.data.isCollenction;
        if (!isCollenction.value) {
          optiones.value[1].icon = "heart";
          optiones.value[1].color = "";
          optiones.value[1].text = "收藏";
        } else {
          optiones.value[1].icon = "heart-filled";
          optiones.value[1].color = "#1C9B64";
          optiones.value[1].text = "已收藏";
        }
        productAttrList.value = res.data.data.product.productAttrList;
        description.value = formatRichText(res.data.data.product.description);
        masterImg.value = res.data.data.product.productPictureList;
        masterImgs.value = res.data.data.product.masterImg;
        mallPrice.value = res.data.data.product.mallPrice;
        state.value = res.data.data.product.state;
        sales.value = res.data.data.product.productGoodsList[0].sales;
        details.value = res.data.data.product;
        isSelf.value = res.data.data.product.isSelf;
        report.value = res.data.data.report;
        if (res.data.data.product.productGoodsList[0].stock == 0) {
          min.value = 0;
          max.value = 0;
          numValue.value = 0;
        } else {
          min.value = 1;
          max.value = res.data.data.product.productGoodsList[0].stock;
          numValue.value = 1;
        }
      });
      api_goodsDetail.getEffectiveCoupon({
        productId: id.value
      }).then((res) => {
        EffectiveCoupon.value = res.data.data;
      });
    }
    function getDailog() {
      isAddCart.value = 2;
      popup.value.open();
    }
    function buttonClick(e) {
      if (state.value != 2) {
        common_vendor.index.showToast({
          icon: "none",
          duration: 3e3,
          title: `该商品已下架`
        });
        return;
      }
      if (e.index == 0) {
        isAddCart.value = 2;
      } else {
        isAddCart.value = 1;
      }
      popup.value.open();
    }
    function close() {
      popup.value.close();
    }
    function isCollenctions() {
      if (!common_vendor.index.getStorageSync("token")) {
        common_vendor.index.navigateTo({
          url: "/pages/loginInterface/passwordLogin"
        });
        return;
      }
      if (isCollenction.value) {
        api_member.shopCollectionCancel({
          productId: id.value
        }).then((res) => {
          optiones.value[1].icon = "heart";
          optiones.value[1].color = "";
          optiones.value[1].text = "收藏";
          isCollenction.value = !isCollenction.value;
        });
      } else {
        api_member.shopCollectionCreate({
          productId: id.value
        }).then((res) => {
          optiones.value[1].icon = "heart-filled";
          optiones.value[1].color = "#1C9B64";
          optiones.value[1].text = "已收藏";
          isCollenction.value = !isCollenction.value;
        });
      }
    }
    function confirmAddCart() {
      if (isAddCart.value == 1) {
        store_order.order().resize();
        store_order.order().setProductId(id.value);
        store_order.order().setProductGoodId(productGoodId.value);
        store_order.order().setNumber(numValue.value);
        console.log(store_order.order().order);
        popup.value.close();
        common_vendor.index.navigateTo({
          url: "/pages/shoppingCart/confirmOrder?integral=1"
        });
      } else if (isAddCart.value == 2) {
        api_goodsDetail.cartSave({
          productId: productId.value,
          number: numValue.value,
          productGoodId: productGoodId.value
        }).then((res) => {
          if (res.data.success) {
            common_vendor.index.showToast({
              icon: "none",
              duration: 3e3,
              title: `加购成功`
            });
          }
        });
      } else if (isAddCart.value == 3) {
        if (store.state.confims.productId && store.state.confims.productId != void 0 && store.state.confims.productId != "") {
          alert(JSON.stringify(store.state.confims) + "++++");
          let local = encodeURIComponent((window.location.href.split("#")[0].indexOf("?") == -1 ? window.location.href.split("#")[0] : window.location.href.split("#")[0].split("?")[0]) + "#/pages/shoppingCart/confirmOrder");
          let appid = "wx1874315b459ddf26";
          window.location.href = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=" + appid + "&redirect_uri=" + local + "&response_type=code&scope=snsapi_base&state=1#wechat_redirect";
        } else {
          store.commit("setConfim", {
            productId: productId.value,
            productGoodId: productGoodId.value,
            number: numValue.value
          });
          common_vendor.index.setStorageSync("confims", {
            productId: productId.value,
            productGoodId: productGoodId.value,
            number: numValue.value
          });
          let local = encodeURIComponent((window.location.href.split("#")[0].indexOf("?") == -1 ? window.location.href.split("#")[0] : window.location.href.split("#")[0].split("?")[0]) + "#/pages/shoppingCart/confirmOrder");
          let appid = "wx1874315b459ddf26";
          window.location.href = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=" + appid + "&redirect_uri=" + local + "&response_type=code&scope=snsapi_base&state=1#wechat_redirect";
        }
      }
      popup.value.close();
    }
    function jump() {
      common_vendor.index.navigateTo({
        url: "/pages/homePage/reviewsPage?id=" + id.value
      });
    }
    function choose() {
      let aaa = "";
      if (norm1.value != 99 && norm1Value.value != "") {
        aaa = norm1.value + ",";
      } else {
        aaa = "";
      }
      let bbb = "";
      if (norm1Value.value != "" && norm2Value.value != "") {
        bbb = ";";
      } else {
        bbb = "";
      }
      let ccc = "";
      if (norm2.value != 99 && norm2Value.value != "") {
        ccc = norm2.value + ",";
      } else {
        ccc = "";
      }
      chooseValue.value = aaa + norm1Value.value + bbb + ccc + norm2Value.value;
    }
    function onClick(e) {
      if (e.index == 1) {
        isCollenctions();
      } else if (e.index == 2) {
        common_vendor.index.switchTab({
          url: "/pages/shoppingCart/ShoppingCart"
        });
      } else if (e.index == 0) {
        common_vendor.index.switchTab({
          url: "/pages/homePage/homepage"
        });
      }
    }
    function getComment() {
      api_reviews.comment({
        productId: id.value
      }).then((res) => {
        console.log(res);
        let data = res.data.data;
        data.forEach((item) => {
          if (item.replyContent.length > 51) {
            item.textOpenFlag = null;
          } else {
            item.textOpenFlag = null;
          }
        });
        productCommentList.value = [...data];
      });
    }
    function getCoupon(val) {
      api_goodsDetail.reveivecoupon({
        couponId: val
      }).then((res) => {
        if (res.data.success) {
          common_vendor.index.showToast({
            icon: "none",
            duration: 3e3,
            title: `领取成功`
          });
          addCart();
        }
      });
    }
    const parameterPopup = common_vendor.ref();
    function parameter() {
      parameterPopup.value.open();
    }
    function closeParameter(val) {
      parameterPopup.value.close();
    }
    let popupImg = common_vendor.ref();
    let imgUrl = common_vendor.ref("");
    function openPopup(val) {
      imgUrl.value = val;
      popupImg.value.open();
    }
    function closePopup() {
      popupImg.value.close();
    }
    function formatRichText(html) {
      let newContent = html.replace(/<img[^>]*>/gi, function(match, capture) {
        match = match.replace(/style="[^"]+"/gi, "").replace(/style='[^']+'/gi, "");
        match = match.replace(/width="[^"]+"/gi, "").replace(/width='[^']+'/gi, "");
        match = match.replace(/height="[^"]+"/gi, "").replace(/height='[^']+'/gi, "");
        return match;
      });
      newContent = newContent.replace(/style="[^"]+"/gi, function(match, capture) {
        match = match.replace(/width:[^;]+;/gi, "width:100%;").replace(/width:[^;]+;/gi, "width:100%;");
        return match;
      });
      newContent = newContent.replace(/<br[^>]*\/>/gi, "");
      newContent = newContent.replace(/\<img/gi, '<img style="width:100%;height:auto;display:block;"');
      return newContent;
    }
    const share = common_vendor.ref();
    function shareToggle() {
    }
    function selected(item, index) {
      if (item.index == 0) {
        common_vendor.index.share({
          provider: "weixin",
          scene: "WXSceneSession",
          type: 5,
          imageUrl: "http://img.yixiekeji.com:8080/b2cimage" + masterImgs.value,
          title: name.value,
          miniProgram: {
            id: "gh_1da0d5922daf",
            //微信小程序原始id
            path: "pages/homePage/goodsDetail?id=" + id.value,
            //点击链接进入的页面
            type: 2,
            //微信小程序版本类型，可取值： 0-正式版； 1-测试版； 2-体验版。 默认值为0。
            webUrl: "http://uniapp.dcloud.io"
            //兼容低版本的网页链接
          },
          success: function(res) {
            console.log("success:" + JSON.stringify(res));
          },
          fail: function(err) {
            console.log("fail:" + JSON.stringify(err));
          }
        });
      }
    }
    common_vendor.onLoad((options) => {
      id.value = options.id;
      addCart();
      getComment();
    });
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: common_vendor.o(_ctx.test),
        b: common_vendor.p({
          ["nav-background-color"]: "#f7f7f5",
          backImageUrl: "1",
          textContent: "商品详情",
          fontSize: "15",
          fontWeight: "600"
        }),
        c: common_vendor.f(common_vendor.unref(masterImg), (item, index, i0) => {
          return {
            a: _ctx.imageUrl + item.imagePath,
            b: index
          };
        }),
        d: common_vendor.o(change),
        e: common_vendor.p({
          info: common_vendor.unref(masterImg),
          current: current.value,
          field: "content",
          mode: mode.value,
          ["dots-styles"]: common_vendor.unref(dotsStyles)
        }),
        f: common_vendor.t(mallPrice.value),
        g: common_vendor.t(sales.value),
        h: common_assets._imports_0$7,
        i: common_vendor.o(shareToggle),
        j: common_vendor.o(isCollenctions),
        k: common_vendor.p({
          type: isCollenction.value ? "heart-filled" : "heart",
          size: "44rpx",
          color: "#1C9B64"
        }),
        l: common_vendor.t(isCollenction.value ? "已收藏" : "收藏"),
        m: common_vendor.t(name.value),
        n: common_vendor.unref(EffectiveCoupon).length > 0
      }, common_vendor.unref(EffectiveCoupon).length > 0 ? {
        o: common_vendor.t(common_vendor.unref(EffectiveCoupon).length),
        p: common_vendor.f(common_vendor.unref(EffectiveCoupon), (item, k0, i0) => {
          return {
            a: common_vendor.t(item.couponValue),
            b: common_vendor.o(($event) => getCoupon(item.id)),
            c: common_vendor.t(item.minAmount)
          };
        })
      } : {}, {
        q: !(norm1.value == 99 && norm2.value == 99)
      }, !(norm1.value == 99 && norm2.value == 99) ? common_vendor.e({
        r: norm1.value == 99 && norm2.value == 99
      }, norm1.value == 99 && norm2.value == 99 ? {
        s: common_vendor.p({
          border: false,
          showArrow: true,
          title: common_vendor.unref(details).productGoodsList[0].normName
        })
      } : {
        t: common_vendor.p({
          border: false,
          showArrow: true,
          title: chooseValue.value == "" ? "未选择" : chooseValue.value
        })
      }, {
        v: common_vendor.o(($event) => getDailog())
      }) : {}, {
        w: common_vendor.p({
          showArrow: true,
          title: common_vendor.unref(regionName) == "" ? "请选择" : common_vendor.unref(regionName),
          note: common_vendor.unref(regionName) == "" ? "付款成功后，预计24小时内发货" : common_vendor.unref(free) + "元起"
        }),
        x: common_vendor.o(handleGetRegion),
        y: common_vendor.p({
          defaultRegion: defaultRegionCode.value
        }),
        z: common_vendor.o((...args) => _ctx.chooseAddress && _ctx.chooseAddress(...args)),
        A: report.value
      }, report.value ? {} : {}, {
        B: common_vendor.f(common_vendor.unref(productAttrList), (item, index, i0) => {
          return {
            a: common_vendor.t(item.value),
            b: common_vendor.t(item.name),
            c: common_vendor.unref(productAttrList).length > 4 ? index < 3 : index < common_vendor.unref(productAttrList).length - 2,
            d: index > 3 ? "none" : ""
          };
        }),
        C: common_vendor.p({
          type: "right",
          size: "16",
          color: "#999"
        }),
        D: common_vendor.o(parameter),
        E: productCommentList.value.length != 0
      }, productCommentList.value.length != 0 ? {
        F: common_vendor.t(favorableRate.value),
        G: common_vendor.p({
          type: "right",
          size: "17",
          color: "#999999"
        }),
        H: common_vendor.o(($event) => jump()),
        I: common_vendor.f(productCommentList.value, (item, k0, i0) => {
          return common_vendor.e({
            a: item.memberHeadImg && item.memberHeadImg.indexOf("http") != -1
          }, item.memberHeadImg && item.memberHeadImg.indexOf("http") != -1 ? {
            b: item.memberHeadImg
          } : item.memberHeadImg ? {
            d: _ctx.imageUrl + item.memberHeadImg
          } : {
            e: "6802ea33-9-" + i0,
            f: common_vendor.p({
              type: "contact",
              size: "40",
              color: "#1C9B64"
            })
          }, {
            c: item.memberHeadImg,
            g: common_vendor.t(item.memberName),
            h: "6802ea33-10-" + i0,
            i: common_vendor.p({
              size: "18",
              value: item.grade,
              color: "#bbb",
              ["active-color"]: "#1C9B64"
            }),
            j: common_vendor.t(item.content),
            k: common_vendor.f(item.productCommentPictureList, (itemImg, k1, i1) => {
              return {
                a: _ctx.imageUrl + itemImg.imagePath,
                b: common_vendor.o(($event) => openPopup(itemImg.imagePath))
              };
            }),
            l: item.replyContent
          }, item.replyContent ? common_vendor.e({
            m: common_vendor.t(item.replyContent),
            n: item.textOpenFlag ? "58rpx" : "",
            o: item.textOpenFlag ? 1 : "",
            p: item.textOpenFlag !== null
          }, item.textOpenFlag !== null ? {
            q: common_vendor.t(item.textOpenFlag ? "【展开】" : "【收起】"),
            r: common_vendor.o(($event) => item.textOpenFlag = !item.textOpenFlag)
          } : {}) : {});
        })
      } : {}, {
        J: description.value,
        K: common_vendor.o(onClick),
        L: common_vendor.o(buttonClick),
        M: common_vendor.p({
          options: common_vendor.unref(optiones),
          fill: true,
          ["button-group"]: common_vendor.unref(customButtonGroup)
        }),
        N: _ctx.imageUrl + masterImgs.value,
        O: common_vendor.t(name.value),
        P: common_vendor.t(mallPrice.value),
        Q: common_vendor.o(close),
        R: common_vendor.p({
          type: "closeempty",
          color: "#999",
          size: "18"
        }),
        S: norm1Value.value != "" || norm2Value.value != ""
      }, norm1Value.value != "" || norm2Value.value != "" ? {
        T: common_vendor.t(chooseValue.value)
      } : {}, {
        U: norm1.value == 99 && norm2.value == 99 && common_vendor.unref(details).productGoodsList[0].normName
      }, norm1.value == 99 && norm2.value == 99 && common_vendor.unref(details).productGoodsList[0].normName ? {
        V: common_vendor.t(common_vendor.unref(details).productGoodsList[0].normName)
      } : {}, {
        W: norm2.value != 99
      }, norm2.value != 99 ? {
        X: common_vendor.t(norm2.value),
        Y: common_vendor.o(norm2Change),
        Z: common_vendor.o(($event) => norm2Value.value = $event),
        aa: common_vendor.p({
          selectedColor: "rgba(28, 155, 100, 0.4)",
          selectedTextColor: "#1C9B64",
          mode: "tag",
          localdata: common_vendor.unref(norm2s),
          modelValue: norm2Value.value
        })
      } : {}, {
        ab: norm1.value != 99
      }, norm1.value != 99 ? {
        ac: common_vendor.t(norm1.value),
        ad: common_vendor.o(norm1Change),
        ae: common_vendor.o(($event) => norm1Value.value = $event),
        af: common_vendor.p({
          selectedColor: "rgba(28, 155, 100, 0.4)",
          selectedTextColor: "#1C9B64",
          mode: "tag",
          localdata: common_vendor.unref(norm1s),
          modelValue: norm1Value.value
        })
      } : {}, {
        ag: common_vendor.t(stock.value),
        ah: common_vendor.o(($event) => numValue.value = $event),
        ai: common_vendor.p({
          max: max.value,
          min: min.value,
          modelValue: numValue.value
        }),
        aj: common_vendor.t(stock.value == "暂无库存" ? stock.value : "确定"),
        ak: common_vendor.o(($event) => confirmAddCart()),
        al: stock.value == "暂无库存",
        am: common_vendor.sr(popup, "6802ea33-12", {
          "k": "popup"
        }),
        an: common_vendor.p({
          type: "bottom"
        }),
        ao: common_vendor.o(($event) => closeParameter()),
        ap: common_vendor.p({
          type: "closeempty",
          size: "18",
          color: "#1E1E1F"
        }),
        aq: common_vendor.f(common_vendor.unref(productAttrList), (item, k0, i0) => {
          return {
            a: common_vendor.t(item.name),
            b: common_vendor.t(item.value)
          };
        }),
        ar: common_vendor.o(($event) => closeParameter()),
        as: common_vendor.sr(parameterPopup, "6802ea33-17", {
          "k": "parameterPopup"
        }),
        at: common_vendor.p({
          ["background-color"]: "#fff",
          ["border-radius"]: "48rpx 48rpx 0px 0px",
          type: "bottom"
        }),
        av: common_vendor.o(closePopup),
        aw: common_vendor.p({
          type: "closeempty",
          color: "#999",
          size: "18"
        }),
        ax: _ctx.imageUrl + common_vendor.unref(imgUrl),
        ay: common_vendor.sr(popupImg, "6802ea33-19", {
          "k": "popupImg"
        }),
        az: common_vendor.p({
          type: "center"
        }),
        aA: common_vendor.o(selected),
        aB: common_vendor.sr(share, "6802ea33-21", {
          "k": "share"
        }),
        aC: common_vendor.p({
          type: "share",
          safeArea: true,
          backgroundColor: "#fff"
        }),
        aD: shareFlag.value
      }, shareFlag.value ? {
        aE: common_vendor.o(($event) => shareFlag.value = false),
        aF: common_vendor.p({
          type: "closeempty",
          size: "30",
          color: "#fff"
        })
      } : {});
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-6802ea33"]]);
wx.createPage(MiniProgramPage);
